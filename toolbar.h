#ifndef TOOLBAR_H
#define TOOLBAR_H
#include <stdio.h>
#include <QAction>
#include <QSignalmapper>
#include <QMenu>

#include "customnode.h"
#include "scene.h"
#include "sceneio.h"
#include "nodebuilder.h"

/// <summary>
/// Tool bar object which is displayed at the top of the
/// main window
/// </summary>
class ToolBar : public QWidget
{
	Q_OBJECT

public:
	explicit ToolBar(QMenuBar* menu, QWidget* parent = 0) : QWidget(parent), windowMenu_(menu) { scene_ = NULL; }
	virtual ~ToolBar(void);

	void initaliseToolBar();

	/// <summary>
	/// Sets the scene.
	/// </summary>
	/// <param name="activeScene">The active scene.</param>
	void setScene(Scene* activeScene) { scene_ = activeScene; }

signals:
	void openNodeBuilder();
	void saveSceneOut(QString location, Scene* scene);
	void loadSceneIn(QString location, Scene* scene);
	void buildNewScene();

private slots:

	//File Menu Slots
	void newScene();
	void openScene();
	void saveScene();
	void saveAsScene();

	
	//create Menu Slots
	void constructNode();

	//Help Menu Slots
	void help();
	void about();
private:

	void initaliseFileMenu();
	void initaliseCreateMenu();
	void initaliseHelpMenu();


private:

	
	/// <summary>
	/// The menu bar which will hold all the menus
	/// </summary>
	QMenuBar* windowMenu_;

	/// <summary>
	/// The file menu
	/// </summary>
	QMenu* fileMenu_;

	/// <summary>
	/// The create menu
	/// </summary>
	QMenu* createMenu_;

	/// <summary>
	/// The help menu
	/// </summary>
	QMenu* helpMenu_;

	/// <summary>
	/// The new scene action
	/// </summary>
	QAction* newSceneAction_;

	/// <summary>
	/// The open scene action
	/// </summary>
	QAction* openSceneAction_;

	/// <summary>
	/// The save scene action
	/// </summary>
	QAction* saveSceneAction_;

	/// <summary>
	/// The save scene as action
	/// </summary>
	QAction* saveAsSceneAction_;

	/// <summary>
	/// The exit action
	/// </summary>
	QAction* exitAction_;

	/// <summary>
	/// The custom node action
	/// loads the custom node builder
	/// </summary>
	QAction* customNodeAction_;

	/// <summary>
	/// The help action
	/// </summary>
	QAction* helpAction_;

	/// <summary>
	/// The about action
	/// </summary>
	QAction* aboutAction_;

	/// <summary>
	/// File dialog which will help with in input and output of 
	/// scene information
	/// </summary>
	QFileDialog* file_;

	/// <summary>
	/// The scene object
	/// </summary>
	Scene* scene_;
	
	/// <summary>
	/// The scene Input and Output
	/// </summary>
	SceneIO* sceneIO_;
	
};

#endif // TOOLBAR_H
