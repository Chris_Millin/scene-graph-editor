#ifndef SCENE_H
#define SCENE_H

#include <QWidget>

#include "rendercontext.h"
#include "customnode.h"
#include "nodebuilder.h"

using namespace irr;
using namespace scene;

/// <summary>
/// The Scene object which provides the ability to build nodes and
/// stores the base node of the scene which is used to build the scene tree
/// </summary>
class Scene : public QWidget
{
	Q_OBJECT

public:
    Scene(void);
	virtual ~Scene(void);

	void initaliseScene();

	bool isNodeBase(Node* checkNode);

	Node* getBaseNode();

signals:
	void updateScene(Scene* scene);
	void addedNode(Node* newNode);


public slots:
	void addCustomNode(CustomNode* node, std::vector<CompleteProperty> propertyList);
	void selectedNode(Node* node) { currentSelectedNode_ = node; }

private:

	CompleteProperty findName(std::vector<CompleteProperty> properties, std::string name);
	void addProperties(CustomNode* node, std::vector<CompleteProperty> properties);

	void addMeshNode(CustomNode* node, std::vector<CompleteProperty> properties);
	void addAniMeshNode(CustomNode* node, std::vector<CompleteProperty> properties);
	void addTerrainNode(CustomNode* node, std::vector<CompleteProperty> properties);

private:

	/// <summary>
	/// The base node_ for the scene.
	/// Using this node it should be possible to obtain any node
	/// in the scene.
	/// </summary>
	Node* baseNode_;
	/// <summary>
	/// The node list_ containing all the nodes
	/// added in the scene as a list structure.
	/// This provides a slightly quickest way of finding a node
	/// than traversing the hierarchical tree if the parent node
	/// is not known
	/// </summary>
	std::vector<Node*> NodeList_;
	/// <summary>
	/// The current selected node_ in the scene
	/// this is set via the scenelist object
	/// </summary>
	Node* currentSelectedNode_;
};

#endif // SCENE_H
