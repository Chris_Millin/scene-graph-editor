#include "mainwindow.h"
#include <Windows.h>
#include <io.h>
#include <fcntl.h> 

#include <QApplication>

#define MAX_CONSOLE_LINES 500
#define MIN_WINDOW_X 1000
#define MIN_WINDOW_Y 600


//standard code for launching a windows console
//may be windows exclusive 
/// <summary>
/// Launch the console
/// </summary>
void console()
{
	int hConHandle;

	long lStdHandle;

	CONSOLE_SCREEN_BUFFER_INFO coninfo;

	FILE *fp;

	// allocate a console for this app

	AllocConsole();

	// set the screen buffer to be big enough to let us scroll text

	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),&coninfo);

	coninfo.dwSize.Y = MAX_CONSOLE_LINES;

	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),11);

	// redirect unbuffered STDOUT to the console

	lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen( hConHandle, "w" );

	*stdout = *fp;

	setvbuf( stdout, NULL, _IONBF, 0 );

	// redirect unbuffered STDIN to the console

	lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);

	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen( hConHandle, "r" );

	*stdin = *fp;

	setvbuf( stdin, NULL, _IONBF, 0 );

	// redirect unbuffered STDERR to the console

	lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);

	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen( hConHandle, "w" );

	*stderr = *fp;

	setvbuf( stderr, NULL, _IONBF, 0 );

	// make cout, wcout, cin, wcin, wcerr, cerr, wclog and clog

	// point to console as well
	std::ios_base::sync_with_stdio();
}

int main(int argc, char *argv[])
{
	//Launch a console for output information
	console();

	//initalise Qt
    QApplication a(argc, argv);

	//Create an instance of the main window
    MainWindow w;
	//initalise the window instance which will load all the widgets
	w.initaliseWindow();
	//set the min window size
	w.setMinimumSize(MIN_WINDOW_X,MIN_WINDOW_Y);
	//display the window
    w.show();

	//run the Qt loop
    return a.exec();
}
