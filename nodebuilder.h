#ifndef NODEBUILDER_H
#define NODEBUILDER_H

#include <QWidget>
#include <QtGui>
#include <QVBoxLayout>
#include <qvariant.h>
#include <map>
#include <algorithm>

#include <irrlicht.h>
#include "PropertyWidgets.h"
#include "PropertyCallBacks.h"
#include "customnode.h"


#include "irrlichtNodeBuilder.h"

using namespace irr;
using namespace core;
using namespace video;

/// <summary>
/// A complete property is a structure
/// to store information about a property
/// before it has been fully build
/// so it can be built and added to a custom node
/// </summary>
struct CompleteProperty
{
	IrrNodeType irrtype;
	std::string irrName;
	CallBackInfo propCallBackInfo;
	PropertyID* prop;
};


/// <summary>
/// The node builder class is a Qt dialog window which aids in the designing
/// of custom nodes objects with irrlicht nodes. It contains the functionality
/// to add a irrlicht node, select a callback function and then set the property
/// value for the callback function. Once done this will add a node to the scene
/// (based on the scene containing the functionality to convert the information
/// built from this class to a custom node).
/// </summary>
class NodeBuilder : public QDialog
{
    Q_OBJECT
public:
    explicit NodeBuilder(QWidget *parent = 0);
	virtual ~NodeBuilder(void);

	void initaliseNodeBuilder();

signals:
	void addCustomNode(CustomNode* node, std::vector<CompleteProperty>);

public slots:
	
	void updateNodeList();

	void changedNode(int index);

	void changedNodeName();
	void changedNodeFunction(int index);

	void addProperty();
	void removeProperty();

	void addNode();
	void exitBuilder();

private:
	void buildNameElement(QVBoxLayout* container);
	void buildIrrNodeDropDownElement(QVBoxLayout* container);
	void buildFunctionDropDownElement(QVBoxLayout* container);
	void buildPropertyWidgetElement(QVBoxLayout* container, propertyType type);
	void buildPropertyTableElement(QVBoxLayout* container);

	void UpdateTable();
	void updateFunctionCombo(int nodeTypeID);

	CallBackInfo getCurrentCallBackInfo();

private:

	/// <summary>
	/// The for all the widgets
	/// </summary>
	QVBoxLayout* container_;

	/// <summary>
	/// The name label
	/// </summary>
	QLabel* nameLabel_;
	/// <summary>
	/// The name input text box for the Node
	/// </summary>
	QLineEdit* nameLine_;

	/// <summary>
	/// The irrlicht node builder which will
	/// create irrlicht node types
	/// </summary>
	irrlichtNodeBuilder* nodeWidget_;
	
	/// <summary>
	/// label for the irrlicht node selector
	/// </summary>
	QLabel* irrNodeSelectorLabel_;

	/// <summary>
	/// Drop down which will display all the irrlicht node types
	/// created in the irrlichtNodeBuilder
	/// </summary>
	QComboBox* irrNodeSelectorDropDown_;

	/// <summary>
	/// The label for the function drop down
	/// </summary>
	QLabel* functionLabel_;

	/// <summary>
	/// The function drop down which will display all the available functions
	/// for a selected irrlicht node type
	/// </summary>
	QComboBox* functionDropDown_;

	/// <summary>
	/// label for the property
	/// </summary>
	QLabel* propertyValueLabel_;

	/// <summary>
	/// The property widget which is created when a function callback is
	/// selected form the drop down list to ensure the correct property is
	/// created.
	/// </summary>
	QWidget* propertyWidget_;

	/// <summary>
	/// Button for adding a property
	/// </summary>
	QPushButton* addProperty_;

	/// <summary>
	/// Button for removing a property
	/// </summary>
	QPushButton* removeProperty_;

	/// <summary>
	/// The label for the property table
	/// </summary>
	QLabel* propertyTableLable_;

	/// <summary>
	/// The table which will contain all the properties
	/// for the node so the use knows which ones they have added
	/// </summary>
	QTableWidget* propertyTable_;

	/// <summary>
	/// The dialog buttons for ok and cancel
	/// Cancel button is !NOT IMPLEMENTED!
	/// </summary>
	QDialogButtonBox* dialogButtons_;
	

	/// <summary>
	/// The current node that is being built
	/// </summary>
	CustomNode* currentNode_;

	/// <summary>
	/// The current property that is being displayed
	/// </summary>
	PropertyID* currentProperty_;

	/// <summary>
	/// The completed property list which is compiled at the end
	/// </summary>
	std::vector<CompleteProperty> completedProperties_;

};

#endif // NODEBUILDER_H
