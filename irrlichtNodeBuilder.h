#include <QtGui>
#include "PropertyCallBacks.h"

#pragma once
/// <summary>
/// This is a custom widget to add with the adding of
/// irrlicht nodes to a custom node. It does not explicitly
/// add the irrlicht nodes to the custom node but it can
/// build a list or irrlicht node types which can be obtained
/// and then translated into irrlicht nodes and added to the
/// custom node. This widget it used in the node builder object
/// to help it build the custom node
/// </summary>
class irrlichtNodeBuilder : public QWidget
{
	Q_OBJECT

public:
	irrlichtNodeBuilder(QWidget* parent = 0);
	~irrlichtNodeBuilder(void);

	void initalise();

	/// <summary>
	/// Gets the node list of added nodes.
	/// </summary>
	/// <returns>map of the added irrlicht nodes</returns>
	std::unordered_map<std::string, IrrNodeType> getNodeList() { return nodeList_; }

	/// <summary>
	/// Gets the name of the node type by its type id.
	/// </summary>
	/// <param name="type">The type.</param>
	/// <returns>The name of the irrlicht node</returns>
	QString getNodeTypeName(IrrNodeType type) { return nodeTypes_[type]; }

public slots:

	void addNodeToTable();
	void removeNodeFromTable();

signals:
	void addedNode();
	void removedNode();

private:

	void buildNodeTableElement();
	void buildNodeCreatorElement();

	void updateTable();

private:

	/// <summary>
	/// The top level layout which will contain both
	/// Vbox right and Vbox left
	/// </summary>
	QHBoxLayout* HLayout_;

	/// <summary>
	/// The VBox which will contain the right hand side
	/// widgets
	/// </summary>
	QVBoxLayout* VLayoutLeft_;

	/// <summary>
	/// The VBox which will contain the Left hand side
	/// widgets
	/// </summary>
	QVBoxLayout* VLayoutRight_;

	/// <summary>
	/// The irrlicht node name label
	/// </summary>
	QLabel*	nameLabel_;

	/// <summary>
	/// The irrlicht node name input text box
	/// </summary>
	QLineEdit* nameTextBox_;

	/// <summary>
	/// The irrlicht node type label
	/// </summary>
	QLabel* typeLabel_;

	/// <summary>
	/// Combo box holding the list for all the irrlicht node types
	/// </summary>
	QComboBox* typeCombo_;

	/// <summary>
	/// The button to add the irrlicht node
	/// </summary>
	QPushButton* addNode_;

	/// <summary>
	/// The button to remove the irrlicht node
	/// </summary>
	QPushButton* removeNode_;

	/// <summary>
	/// The table for displaying all the irrlicht nodes
	/// </summary>
	QTableWidget* irrNodeTable_;

	/// <summary>
	/// The stored list of all the nodes
	/// </summary>
	std::unordered_map<std::string, IrrNodeType> nodeList_;
	
	/// <summary>
	/// This is used to add the nodes to the combo box
	/// containing the irrNodeType id and the name for the node
	/// </summary>
	std::unordered_map<int, QString> nodeTypes_;
};

