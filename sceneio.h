#ifndef SCENEIO_H
#define SCENEIO_H

#include "rendercontext.h"
#include <QtGui>
#include "nodebuilder.h"
#include "node.h"
#include "customnode.h"
#include "Property.h"
#include "scene.h"

/// <summary>
/// Structure to store the XML info of a irrlicht node
/// before the properties are created. 
/// </summary>
struct IrrNodeInfo
{
	std::string name;
	int typeID;
};

/// <summary>
/// The Input and Output parsar for the scene structure
/// this will convert the scene into an xml data structure
/// or will import a structure xml data structure to convert
/// into a scene
/// </summary>
class SceneIO : public QWidget
{
	Q_OBJECT
public:
    SceneIO(void);
	virtual ~SceneIO(void);

public slots:
	void saveScene(QString location, Scene* scene);
	void loadScene(QString location, Scene* scene);
signals:
	void addCustomNode(CustomNode* node, std::vector<CompleteProperty> propertyList);

private:

	void saveSceneInformation(irr::io::IXMLWriter* xwriter, Scene* scene);
	void saveNodeInformation(irr::io::IXMLWriter* xwriter, Scene* scene, Node* node);
	void savePropertyInformation(irr::io::IXMLWriter* xwriter, Node* node, PropertyID* property);

	void loadSceneInformation(irr::io::IXMLReader* xml, Scene* scene);
	void loadNodeInformation(irr::io::IXMLReader* xml, Scene* scene, Node* node);
	void loadPropertyInformation(irr::io::IXMLReader* xml, Node* node, CompleteProperty& property);


	vector2df readVec2(irr::io::IXMLReader* xml);
	vector3df readVec3(irr::io::IXMLReader* xml);
	SColor readColour(irr::io::IXMLReader* xml);

	IrrNodeInfo findIrrlichtNode(std::string name, std::vector<IrrNodeInfo> irrnodelist);
};

#endif // SCENEIO_H
