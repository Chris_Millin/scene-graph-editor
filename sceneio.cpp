#include "sceneio.h"

SceneIO::SceneIO(void)
{
}

SceneIO::~SceneIO(void)
{

}

/// <summary>
/// Saves the scene to the XML data structure.
/// </summary>
/// <param name="location">The location of the output.</param>
/// <param name="scene">The scene to output.</param>
void SceneIO::saveScene(QString location, Scene* scene)
{
	//irrlicht contains a basic xml writer and reader which provide enough support to
	//output the scene structure
	irr::io::IXMLWriter* xwriter = RenderContext::getRenderDevice()->getFileSystem()->createXMLWriter(location.toStdString().c_str());
	//write the standard XML header
	xwriter->writeXMLHeader();
	//Save the information about the scene
	saveSceneInformation(xwriter, scene);
	//close the file
	xwriter->drop();
}

/// <summary>
/// Loads the scene as an XML data structure.
/// </summary>
/// <param name="location">The location of the file to read.</param>
/// <param name="scene">The scene to add the scene information to.</param>
void SceneIO::loadScene(QString location, Scene* scene)
{
	//open the xml file with the irrlicht xml reader
	irr::io::IXMLReader* xml = RenderContext::getRenderDevice()->getFileSystem()->createXMLReader(location.toStdString().c_str());
	//if the file was found then load the scene information
	if (xml)
		loadSceneInformation(xml, scene);
	//close the file
	xml->drop();
}

/// <summary>
/// Saves the scene information.
/// At the moment there is no direct scene data to save
/// however this is the starting point to the next stage of
/// saving the nodes.
/// </summary>
/// <param name="xwriter">The xwriter.</param>
/// <param name="scene">The scene.</param>
void SceneIO::saveSceneInformation(irr::io::IXMLWriter* xwriter, Scene* scene)
{
	//Create the top most element with the tag Scene
	xwriter->writeElement(L"Scene");
	xwriter->writeLineBreak();
	//Create the Nodes element to know that nodes are going to
	//be defined inbetween the tags
	xwriter->writeElement(L"Nodes");
	xwriter->writeLineBreak();
	//Save the node information which requires the scene and to start with
	//the base node.
	saveNodeInformation(xwriter,  scene, scene->getBaseNode());
	xwriter->writeLineBreak();
	//close the Nodes tag
	xwriter->writeClosingTag(L"Nodes");
	xwriter->writeLineBreak();
	//close the Scene tag
	xwriter->writeClosingTag(L"Scene");
}

/// <summary>
/// Saves the node information.
/// </summary>
/// <param name="xwriter">The xwriter.</param>
/// <param name="scene">The scene.</param>
/// <param name="node">The node to be written.</param>
void SceneIO::saveNodeInformation(irr::io::IXMLWriter* xwriter, Scene* scene, Node* node)
{
	//Obtain the id of node as a string (This is not going to be used
	//when the data is loaded back in but may be useful for external applications
	//which dont have Uuid capabilities
	stringw id = node->getUuID().toString().toStdString().c_str();
	//Store the name of the node
	stringw name = node->getName().c_str();
	//Write the Node element with the ID attribute and the name attribute
	xwriter->writeElement(L"Node", false, L"id", id.c_str(), L"name", name.c_str());
	xwriter->writeLineBreak();
	//store the child count to know if to create children or not
	stringw children = QString::number(node->getChildCount()).toStdString().c_str();
	//Since a node will contain children (the base will always have children if a node is
	//added to the scene) they will be added in the child tag of the node
	xwriter->writeElement(L"Children", false, L"count", children.c_str());
	xwriter->writeLineBreak();

	//Loop through all the children and perform this same function again to build the node information
	for (int i = 0; i < node->getChildCount(); i++)
	{
		saveNodeInformation(xwriter, scene, node->getChildren()[i]);
	}
	xwriter->writeLineBreak();
	//close the children tag once that is complete
	xwriter->writeClosingTag(L"Children");
	//check to see if the node is not the base node (since the base node has
	//no properties or irrlicht nodes)
	if (node->getUuID() != scene->getBaseNode()->getUuID())
	{
		//Get the irrlicht node list reference
		NodeReference nodes = ((CustomNode*)node)->getNodes();
		//iterate through the irrlicht nodes
		NodeReference::iterator it = nodes.begin();
		for (it; it != nodes.end(); it++)
		{
			//convert the name to a string
			stringw namen = it->first.c_str();
			//convert its type id to a string (this is not really needed for anything 
			//outside of the application but it could be used if needed)
			stringw typen = QString::number(it->second.irrType).toStdString().c_str();
			//Add the irrlicht node element which is a close element with the name and type attributes
			xwriter->writeLineBreak();
			xwriter->writeElement(L"IrrNode", true, L"name", namen.c_str(), L"type", typen.c_str());
			xwriter->writeLineBreak();
		}

		//Obtain the properties form the node
		NodePropertyMap props = ((CustomNode*)node)->getProperties();
		//iterate through the properties
		NodePropertyMap::iterator itp = props.begin();
		for (itp; itp != props.end(); itp++)
		{
			//save the property information
			savePropertyInformation(xwriter, node, itp->second.property);
		}
	}

	xwriter->writeLineBreak();
	//Close the node tag
	xwriter->writeClosingTag(L"Node");
}

/// <summary>
/// Saves the property information.
/// </summary>
/// <param name="xwriter">The xwriter.</param>
/// <param name="node">The node which contains the property.</param>
/// <param name="property">The property.</param>
void SceneIO::savePropertyInformation(irr::io::IXMLWriter* xwriter, Node* node, PropertyID* property)
{
	//convert the id to a string
	stringw id = property->getUuid().toString().toStdString().c_str();
	//convert the name to the correct string
	stringw name = property->getName().c_str();
	//get the name of the irrlicht node that the property uses (NULL if not found)
	stringw nodename = ((CustomNode*)node)->getNodeNameFromProperty(property).c_str();
	//convert the property type id to a string
	stringw typeID = QString::number(property->getTypeId()).toStdString().c_str();
	//write the property tag with the set attributes
	xwriter->writeLineBreak();
	xwriter->writeElement(L"Property", false, L"id", id.c_str(), L"name", name.c_str(), L"node", nodename.c_str(), L"type", typeID.c_str());
	
	
	//The next stage is to write the actual property value
	stringw value;
	if(property->getTypeId() == BOOL_PROP)
	{
		//Bool tag is converted to True or False
		Property<bool>* propertyDatab = dynamic_cast<Property<bool>*>(property);
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Bool");
		xwriter->writeText(propertyDatab->getPropertyasType() == true ? L"True" : L"False");
		xwriter->writeClosingTag(L"Bool");
	}
	if(property->getTypeId() == INT_PROP)
	{
		//convert int to a string and place in the Int tags
		Property<int>* propertyDatai = dynamic_cast<Property<int>*>(property);
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Int");
		value = QString::number(propertyDatai->getPropertyasType()).toStdString().c_str();
		xwriter->writeText(value.c_str());
		xwriter->writeClosingTag(L"Int");
	}
	if(property->getTypeId() == FLOAT_PROP)
	{
		//same as int but with Float tags
		Property<float>* propertyDataf = dynamic_cast<Property<float>*>(property);
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Float");
		value = QString::number(propertyDataf->getPropertyasType()).toStdString().c_str();
		xwriter->writeText(value.c_str());
		xwriter->writeClosingTag(L"Float");
	}
	if(property->getTypeId() == STRING_PROP)
	{
		//string property
		Property<std::string>* propertyDatas = dynamic_cast<Property<std::string>*>(property);
		xwriter->writeLineBreak();
		xwriter->writeElement(L"String");
		value = propertyDatas->getPropertyasType().c_str();
		xwriter->writeText(value.c_str());
		xwriter->writeClosingTag(L"String");
	}
	if(property->getTypeId() == VEC2_PROP)
	{
		//Vec2 is placed inbetween the Vec2 tabs and then
		//broken down again into the X and Y component as tags
		Property<vector2df>* propertyDatav2 = dynamic_cast<Property<vector2df>*>(property);
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Vec2");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"X");
		value = QString::number(propertyDatav2->getPropertyasType().X).toStdString().c_str();
		xwriter->writeText(value.c_str());
		xwriter->writeClosingTag(L"X");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Y");
		value = QString::number(propertyDatav2->getPropertyasType().Y).toStdString().c_str();
		xwriter->writeText(value.c_str());
		xwriter->writeClosingTag(L"Y");
		xwriter->writeLineBreak();
		xwriter->writeClosingTag(L"Vec2");
	}
	if(property->getTypeId() == VEC3_PROP)
	{
		//Same as Vec2 but with the added Z component
		Property<vector3df>* propertyDatav3 = dynamic_cast<Property<vector3df>*>(property);
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Vec3");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"X");
		value = QString::number(propertyDatav3->getPropertyasType().X).toStdString().c_str();
		xwriter->writeText(value.c_str());
		xwriter->writeClosingTag(L"X");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Y");
		value = QString::number(propertyDatav3->getPropertyasType().Y).toStdString().c_str();
		xwriter->writeText(value.c_str());
		xwriter->writeClosingTag(L"Y");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Z");
		value = QString::number(propertyDatav3->getPropertyasType().Y).toStdString().c_str();
		xwriter->writeText(value.c_str());
		xwriter->writeClosingTag(L"Z");
		xwriter->writeLineBreak();
		xwriter->writeClosingTag(L"Vec3");
	}
	if(property->getTypeId() == COLOUR_PROP)
	{
		//Similar to Vec2 and Vec3 but works with RGBA
		Property<SColor>* propertyDatac = dynamic_cast<Property<SColor>*>(property);
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Colour");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"R");
		value = QString::number(propertyDatac->getPropertyasType().getRed()).toStdString().c_str();
		xwriter->writeText(value.c_str());
		xwriter->writeClosingTag(L"R");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"G");
		value = QString::number(propertyDatac->getPropertyasType().getGreen()).toStdString().c_str();
		xwriter->writeText(value.c_str());
		xwriter->writeClosingTag(L"G");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"B");
		value = QString::number(propertyDatac->getPropertyasType().getBlue()).toStdString().c_str();
		xwriter->writeText(value.c_str());
		xwriter->writeClosingTag(L"B");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"A");
		value = QString::number(propertyDatac->getPropertyasType().getAlpha()).toStdString().c_str();
		xwriter->writeText(value.c_str());
		xwriter->writeClosingTag(L"A");
		xwriter->writeLineBreak();
		xwriter->writeClosingTag(L"Colour");
	}

	xwriter->writeLineBreak();
	//Close the property tag
	xwriter->writeClosingTag(L"Property");
}

/// <summary>
/// Loads the scene information into the specified scene object.
/// </summary>
/// <param name="xml">The XML.</param>
/// <param name="scene">The scene.</param>
void SceneIO::loadSceneInformation(irr::io::IXMLReader* xml, Scene* scene)
{
	stringc name;
	//This is not the best way to load in the XML data as it assumes all the
	//data is stored in the correct tree structure and format and doesnt give much room to expand the tree
	
	//read the header
	xml->read();
	//Read the first line <Scene>
	xml->read();
	//Since <Scene> is not used for anything read the next line to get <Nodes>
	xml->read();


	//!NOTE BUG HERE WHICH WILL NEED FIXING!
	//Because the base node is at this level when another node wants to add to the base
	//it gets a little problematic and results in the hierarchy being screwed
	//Also emit addCustomNode may also be the problem 

	
	//get the node name to ensure it is the <Nodes> element
	name = xml->getNodeName();
	printf("\nNode Name = %s\n", name.c_str());
	if (name == L"Nodes")
	{
		//Read the next line which should be <Node>
		xml->read();
		//again check the name
		name = xml->getNodeName();
		printf("Node Name = %s\n", name.c_str());
		if(name == L"Node")
		{	
			//Since the first node will always be the base node
			//load that in
			Node* basenode = scene->getBaseNode();
			//load the node information
			loadNodeInformation(xml, scene, basenode);
		}
	}
}

/// <summary>
/// Loads the node information.
/// </summary>
/// <param name="xml">The XML.</param>
/// <param name="scene">The scene.</param>
/// <param name="node">The node to load informaition into.</param>
void SceneIO::loadNodeInformation(irr::io::IXMLReader* xml, Scene* scene, Node* node)
{
	//list to store the irrlicht nodes for the current node object
	std::vector<IrrNodeInfo> irrlichtNodes;
	//list of completed properties
	std::vector<CompleteProperty> properties;
	stringc name;
	
	//read the first node which should be <Children>
	xml->read();
	//Check the node
	name = xml->getNodeName();
	if(name == L"Children")
	{
		printf("Node Name = %s\n", name.c_str());
		
		//read the next node which should be <Node> since that is the only thing that can be stored in the
		//<Children> tag if not that tag will be </Children> which will result in the current node
		//properties being built
		xml->read();
		//Check the node
		name = xml->getNodeName();
		if(name == L"Node")
		{
			//get the name attribute for the <Node> tag
			stringc nodeName = xml->getAttributeValueSafe(L"name");
			printf("Node Name = %s attname = %s\n", name.c_str(), nodeName.c_str());
			//create a new CustomNode object
			CustomNode* childNode = new CustomNode(nodeName.c_str(), node);
			//add the new node as a child of the previous node
			node->addChild(childNode);
			//load the node information
			loadNodeInformation(xml, scene, childNode);
		}
	}

	//read the next tag after </Children> which should be <IrrNode>
	xml->read();
	//check the node in a loop as there can be multiple IrrNode tags
	name = xml->getNodeName();
	while(name == L"IrrNode")
	{
		//create a new IrrlichtNodeInfo
		IrrNodeInfo info;
		//Get the name attribute of the IrrNode
		stringc irrNodeName = xml->getAttributeValueSafe(L"name");
		//Set the name attribute
		info.name = irrNodeName.c_str();
		//Get and Set the type attribute which is the irrlicht node type ID
		info.typeID = xml->getAttributeValueAsInt(L"type");
		//add the irrlicht node to the list
		irrlichtNodes.push_back(info);
		
		printf("Node Name = %s attname = %s, atttype = %d\n", name.c_str(), irrNodeName.c_str(), info.typeID);
		
		//Read the next tag which will be <IrrNode> or <Property>
		//if it is <IrrNode> the next irrlicht node info will be loaded if not
		//it will move onto properties
		xml->read();
		name = xml->getNodeName();
	}

	//Check to see if the current tag is <Property> which is the tag that comes after <IrrNode>
	while(name == L"Property")
	{
		//obtain the name and linked irrlicht node name attribute
		stringc propName = xml->getAttributeValueSafe(L"name");
		stringc nodeName = xml->getAttributeValueSafe(L"node");

		printf("Node Name = %s attname = %s, attnode = %s\n", name.c_str(), propName.c_str(), nodeName.c_str());
		
		//create a new completed property which will be used to build the property in the Scene Class
		CompleteProperty prop;
		//check to see if the irrlicht node name is a valid irrlicht node contained in the node
		IrrNodeInfo irrNode = findIrrlichtNode(nodeName.c_str(), irrlichtNodes);
		if(irrNode.typeID != -1)
		{
			//if it was set the irrlicht node name
			prop.irrName = nodeName.c_str();
			//set the irrlicht node type
			prop.irrtype = (IrrNodeType)irrNode.typeID;
			//load the rest of the property information
			loadPropertyInformation(xml, node, prop);
			
			properties.push_back(prop);
		}
		//read the tag which should be <Property> or </Node> (wont be </Property> as the loadPropertyInformation reads it.
		xml->read();
		name = xml->getNodeName();
	}

	if(node->getUuID() != scene->getBaseNode()->getUuID())
	{
		//The irrlicht nodes are constructed for the node and the properties added
		emit addCustomNode((CustomNode*)node, properties);
	}

	//if the last tag was </Node> then another read is needed to check the tag is <Node> in
	//which case a new node will need to be created
	xml->read();
	//check the name and also check to make sure that it is a new <Node> tag
	name = xml->getNodeName();
	if (name == "Node" && xml->getNodeType() == irr::io::EXN_ELEMENT)
	{
		//perform all the same actions as with Node in Children

		//get the name attribute for the <Node> tag
		stringc nodeName = xml->getAttributeValueSafe(L"name");
		printf("Node Name = %s attname = %s\n", name.c_str(), nodeName.c_str());
		//create a new CustomNode object
		CustomNode* childNode = new CustomNode(nodeName.c_str(), node->getParent());
		//add the new node as a child of the previous node
		node->getParent()->addChild(childNode);
		//load the node information
		loadNodeInformation(xml, scene, childNode);
	}
	
}


/// <summary>
/// Loads the property information.
/// </summary>
/// <param name="xml">The XML.</param>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void SceneIO::loadPropertyInformation(irr::io::IXMLReader* xml, Node* node, CompleteProperty& property)
{
	PropertyID* prop;

	//Since the current node should be <Property> obtain the property type ID
	int propType = xml->getAttributeValueAsInt(L"type");
	stringc propName = xml->getAttributeValueSafe(L"name");
	stringc name;
	//read the next node which will be one of the property Type Tags
	xml->read();
	//check the names to find which type of property
	name = xml->getNodeName();
	if(name ==  "Bool")
	{
		//read the next information which should be the Bool Data
		xml->read();
		//get the bool data
		stringc val = xml->getNodeData();
		//convert the bool data to a property
		Property<bool>* newProp = new Property<bool>(node, propType, val == "True" ? true : false);
		prop = newProp;
		printf("Added Bool\n");
	}
	else if(name ==  "Int")
	{
		//same as bool but with int value atoi is used to conver the string
		xml->read();
		stringc val = xml->getNodeData();
		Property<int>* newProp = new Property<int>(node, propType, atoi(val.c_str()));
		prop = newProp;
		printf("Added Int\n");
	}
	else if(name ==  "Float")
	{
		xml->read();
		stringc val = xml->getNodeData();
		Property<float>* newProp = new Property<float>(node, propType, atof(val.c_str()));
		prop = newProp;
		printf("Added Float\n");
	}
	else if(name ==  "String")
	{
		xml->read();
		stringc val = xml->getNodeData();
		Property<std::string>* newProp = new Property<std::string>(node, propType, val.c_str());
		prop = newProp;
		printf("Added String\n");
	}
	else if(name ==  "Vec2")
	{
		//unlike the single value properties the vector properties also include <X><Y> tags
		//with data for each. a readVec2 function was created to ease this process
		Property<vector2df>* newProp = new Property<vector2df>(node, propType, readVec2(xml));
		prop = newProp;
		printf("Added Vec2\n");
	}
	else if(name ==  "Vec3")
	{
		Property<vector3df>* newProp = new Property<vector3df>(node, propType, readVec3(xml));
		prop = newProp;
		printf("Added Vec3\n");
	}
	else if(name ==  "Colour")
	{
		Property<SColor>* newProp = new Property<SColor>(node, propType, readColour(xml));
		prop = newProp;
		printf("Added Colour\n");
	}

	prop->setName(propName.c_str());
	//add the property to the completed property structure
	property.prop = prop;

	//Find the CallBackIInfo for the property
	std::unordered_map<std::string, CallBackInfo> callbacks;
	//get all the callbacks for the selected irrlicht node type
	callbacks = NodeCallBacks::getCallbackList((IrrNodeType)property.irrtype);
	//return the callback info
	property.propCallBackInfo = callbacks[property.prop->getName()];

	//read the end tag of the property type
	xml->read();
	//read the </Property> tag
	xml->read();
}

/// <summary>
/// Reads a vec2 from the scene structure.
/// </summary>
/// <param name="xml">The XML.</param>
/// <returns>The read vector2df</returns>
vector2df SceneIO::readVec2(irr::io::IXMLReader* xml)
{
	vector2df vec;
	stringc name;
	xml->read();
	name = xml->getNodeName();
	if (name == "X")
	{
		xml->read();
		stringc val = xml->getNodeData();
		vec.X = atof(val.c_str());
		xml->read();
	}
	xml->read();
	name = xml->getNodeName();
	if (name == "Y")
	{
		xml->read();
		stringc val = xml->getNodeData();
		vec.Y = atof(val.c_str());
		xml->read();
	}

	return vec;
}

/// <summary>
/// Reads a vec3 from the scene structure.
/// </summary>
/// <param name="xml">The XML.</param>
/// <returns>The read vector3df</returns>
vector3df SceneIO::readVec3(irr::io::IXMLReader* xml)
{
	vector3df vec;
	stringc name;
	xml->read();
	name = xml->getNodeName();
	if (name == "X")
	{
		xml->read();
		stringc val = xml->getNodeData();
		vec.X = atof(val.c_str());
		xml->read();
	}
	xml->read();
	name = xml->getNodeName();	
	if (name == "Y")
	{
		xml->read();
		stringc val = xml->getNodeData();
		vec.Y = atof(val.c_str());
		xml->read();
	}
	xml->read();
	name = xml->getNodeName();
	if (name == "Z")
	{
		xml->read();
		stringc val = xml->getNodeData();
		vec.Z = atof(val.c_str());
		xml->read();
	}

	return vec;
}

/// <summary>
/// Reads a colour from the scene structure.
/// </summary>
/// <param name="xml">The XML.</param>
/// <returns>The read colour</returns>
SColor SceneIO::readColour(irr::io::IXMLReader* xml)
{
	SColor col;
	stringc name;
	xml->read();
	name = xml->getNodeName();
	if (name == "R")
	{
		xml->read();
		stringc val = xml->getNodeData();
		col.setRed(atoi(val.c_str()));
		xml->read();
	}
	xml->read();
	name = xml->getNodeName();
	if (name == "G")
	{
		xml->read();
		stringc val = xml->getNodeData();
		col.setGreen(atoi(val.c_str()));
		xml->read();
	}
	xml->read();
	name = xml->getNodeName();
	if (name == "B")
	{
		xml->read();
		stringc val = xml->getNodeData();
		col.setBlue(atoi(val.c_str()));
		xml->read();
	}
	xml->read();
	name = xml->getNodeName();
	if (name == "A")
	{
		xml->read();
		stringc val = xml->getNodeData();
		col.setAlpha(atoi(val.c_str()));
		xml->read();
	}

	return col;
}

/// <summary>
/// helper function to find a read in irrlicht node
/// </summary>
/// <param name="name">The name.</param>
/// <param name="irrnodelist">The irrnodelist.</param>
/// <returns>The irrlicht node info. type id is -1 if not found</returns>
IrrNodeInfo SceneIO::findIrrlichtNode(std::string name, std::vector<IrrNodeInfo> irrnodelist)
{
	for (int i = 0; i < irrnodelist.size(); i++)
	{
		if (irrnodelist[i].name == name)
		{
			return irrnodelist[i];
		}
	}
	IrrNodeInfo r;
	r.typeID = -1;
	return r;
}
