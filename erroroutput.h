#ifndef ERROROUTPUT_H
#define ERROROUTPUT_H

#include <stdio.h>
#include <string>

/// <summary>
/// The default log file location
/// </summary>
#define LOG_NAME "ErrorLog.txt"

/// <summary>
/// This class is to provide basic error output which is stored in a log
/// file which can be checked at any time to view the errors
/// the log file would also be encorperate into the error message
/// </summary>
static class ErrorOutput
{
public:
    ErrorOutput();

	static void initaliseErrorLog(bool newLog = false);
	static void clearLog();

	static void WriteError(std::string message);

private:

	static bool checkFileExists();
	static void createLog();
	static void loadLog();

	static const char* getLogName() { return LOG_NAME; }

private:

	static FILE* logFile_;
};

#endif // ERROROUTPUT_H
