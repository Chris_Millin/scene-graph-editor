#include "nodebuilder.h"

NodeBuilder::NodeBuilder(QWidget *parent) : QDialog(parent)
{
	functionDropDown_ = NULL;
	currentProperty_ = NULL;
	propertyWidget_ = NULL;
}

NodeBuilder::~NodeBuilder(void)
{

}

/// <summary>
/// Initalises the node builder.
/// </summary>
void NodeBuilder::initaliseNodeBuilder()
{
	//set the fixed size to 500,800
	this->setFixedSize(500,800);

	//Create the custom node 
	currentNode_ = new CustomNode("CustomNode", NULL);

	//Create the container for the widgets in the dialog
	container_ = new QVBoxLayout(this);
	
	//Build the name element for the node builder
	buildNameElement(container_);

	//Create the irrlicht node builder
	nodeWidget_ = new irrlichtNodeBuilder(this);
	//initalise it
	nodeWidget_->initalise();
	//add it to the container at the third position
	container_->insertWidget(3, nodeWidget_);
	//connect the signals for when a node is added or removed from the irrlicht
	//node builder
	connect(nodeWidget_, SIGNAL(addedNode()), this, SLOT(updateNodeList()));
	connect(nodeWidget_, SIGNAL(removedNode()), this, SLOT(updateNodeList()));


	//build the irrlicht node drop down elements
	buildIrrNodeDropDownElement(container_);
	//build the function drop down elements
	buildFunctionDropDownElement(container_);

	//create the add node property button
	addProperty_ = new QPushButton(tr("Add Property"), this);
	//disable space or enter from interacting with the button
	addProperty_->setDefault(false);
	addProperty_->setAutoDefault(false);
	//connect the clicked signal to the addproperty slot
	connect(addProperty_, SIGNAL(clicked()), this, SLOT(addProperty()));
	//add te button to the layout container
	container_->addWidget(addProperty_);
	//add a spacer to space the add button and property table 
	container_->addSpacerItem(new QSpacerItem(0, 25));

	//build the table elemets
	buildPropertyTableElement(container_);

	//Create the remove property button with the similar parameters to the
	//add property
	removeProperty_ = new QPushButton(tr("Remove Property"), this);
	removeProperty_->setDefault(false);
	removeProperty_->setAutoDefault(false);
	//connect the click event to removeproperty slot
	connect(removeProperty_, SIGNAL(clicked()), this, SLOT(removeProperty()));
	//add the button to the container layout
	container_->addWidget(removeProperty_);
	
	//add the dialog buttons
	dialogButtons_ = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	//link their signals
	connect(dialogButtons_, SIGNAL(accepted()), this, SLOT(addNode()));
	connect(dialogButtons_, SIGNAL(rejected()), this, SLOT(exitBuilder()));
	//add the buttons to the layout
	container_->addWidget(dialogButtons_);

	//set the layout for the dialog
	this->setLayout(container_);
}

/// <summary>
/// Changeds the name of the node.
/// </summary>
void NodeBuilder::changedNodeName()
{
	currentNode_->rename(nameLine_->text().toStdString());
}

/// <summary>
/// Change the node type in the combo box.
/// </summary>
/// <param name="index">The index in the combo box.</param>
void NodeBuilder::changedNode(int index)
{
	updateFunctionCombo((IrrNodeType)irrNodeSelectorDropDown_->itemData(index).toInt());
}

/// <summary>
/// Change the node function in the combo box.
/// construct the property for the function
/// </summary>
/// <param name="index">The index in the combo box.</param>
void NodeBuilder::changedNodeFunction(int index)
{
	//check to ensure it wasnt set to no call back
	if (functionDropDown_->currentText() != tr("No CallBack"))
	{
		//get the all the current node callback info
		std::unordered_map<std::string, CallBackInfo> callbacks = NodeCallBacks::getCallbackList((IrrNodeType)functionDropDown_->itemData(index).toInt());
		//find the function
		CallBackInfo info = callbacks[functionDropDown_->currentText().toStdString()];
		//build the property widget for it
		buildPropertyWidgetElement(container_, (propertyType)info.PropertyType);
	}
	
}


/// <summary>
/// Adds the property to the table and completed list.
/// </summary>
void NodeBuilder::addProperty()
{
	//check that the node type is valid
	if(irrNodeSelectorDropDown_->itemData(irrNodeSelectorDropDown_->currentIndex()).toInt() != IRR_NO_NODE)
	{
		//create the completed property
		CompleteProperty newProperty;
		newProperty.irrtype	= (IrrNodeType)irrNodeSelectorDropDown_->itemData(irrNodeSelectorDropDown_->currentIndex()).toInt();
		newProperty.irrName	= irrNodeSelectorDropDown_->itemText(irrNodeSelectorDropDown_->currentIndex()).toStdString();
		newProperty.propCallBackInfo = getCurrentCallBackInfo();
		newProperty.prop	= currentProperty_;
		
		//if(std::find(completedProperties_.begin(), completedProperties_.end(), newProperty) == completedProperties_.end())
		//add the property
		completedProperties_.push_back(newProperty);
		
		//set the current property to null since the property has now been added
		currentProperty_ = NULL;
		//inset a new row into the property table
		propertyTable_->insertRow(propertyTable_->rowCount());
	}
	else
	{
		//This should allow for properites to be added to a generic no purpouse irrlicht node
		//but has not been implement fully
		currentProperty_ = NULL;
		//insert a new row into the table
		propertyTable_->insertRow(propertyTable_->rowCount());
	}

	//update the table
	UpdateTable();

}


/// <summary>
/// Removes the property for the table and list.
/// </summary>
void NodeBuilder::removeProperty()
{
	//get all the selected items in the table
	QList<QTableWidgetItem*> selected_itms = propertyTable_->selectedItems();
	while( !selected_itms.isEmpty() )
	{
		//get the first item
		QTableWidgetItem* item = selected_itms.at(0);
		int index = -1;
		//loop through all the completed properties and check the unique id is the same
		for(int i = 0; i < completedProperties_.size(); i++)
		{
			if (completedProperties_[i].prop->getUuid().toString() == item->text())
			{
				//if it is store the index location
				index = i;
				break;
			}
		}
		//if the index is valid
		if(index != -1)
		{
			//remove the property
			completedProperties_.erase(completedProperties_.begin()+index);
			//remove the row from the table
			propertyTable_->removeRow(item->row());
		}
		else
			break;
		//get the new selected index count
		selected_itms = propertyTable_->selectedItems();
	}
	//update the table with the new property items
	UpdateTable();
}


/// <summary>
/// used to emit the signal to add a new node to the scene.
/// </summary>
void NodeBuilder::addNode()
{
	if(currentNode_)
		emit addCustomNode(currentNode_, completedProperties_);
	exitBuilder();
}

/// <summary>
/// Exits the node builder.
/// </summary>
void NodeBuilder::exitBuilder()
{
	//delete this;
}

/// <summary>
/// Builds the name elements.
/// </summary>
/// <param name="container">The layout container.</param>
void NodeBuilder::buildNameElement(QVBoxLayout* container)
{
	//create the name label
	nameLabel_ = new QLabel(tr("Node Name"));
	//create the input text box for the name
	nameLine_ = new QLineEdit(QString::fromStdString(currentNode_->getName()));
	
	//add the two widgets to the container
	container_->insertWidget(0, nameLabel_);
	container_->insertWidget(1, nameLine_);
	//add a spacer
	container_->addSpacerItem(new QSpacerItem(0, 25));

	//connect the name text box editing finished signal 
	connect(nameLine_, SIGNAL(editingFinished()), this, SLOT(changedNodeName()));
}


/// <summary>
/// Builds the irrlicht node type drop down elements.
/// </summary>
/// <param name="container">The layout container.</param>
void NodeBuilder::buildIrrNodeDropDownElement(QVBoxLayout* container)
{
	//create the label and combo box
	irrNodeSelectorLabel_ = new QLabel(tr("Available Nodes"));
	irrNodeSelectorDropDown_ = new QComboBox();

	//add the two widgets (the combo box contains nothing at the moment)
	container->insertWidget(4, irrNodeSelectorLabel_);
	container->insertWidget(5, irrNodeSelectorDropDown_);
	
	//connect the combo box index change to the change node slot
	connect(irrNodeSelectorDropDown_,	SIGNAL(currentIndexChanged(int)),	this, SLOT(changedNode(int)));
}

/// <summary>
/// Builds the function drop down elements.
/// </summary>
/// <param name="container">The layout container.</param>
void NodeBuilder::buildFunctionDropDownElement(QVBoxLayout* container)
{
	//Create the label and combo box
	functionLabel_ = new QLabel(tr("Available Functions"));
	functionDropDown_ = new QComboBox();

	//add the widgets to the layout (similar to the node drop down
	//the function drop down will not contain any elements yet)
	container->insertWidget(6, functionLabel_);
	container->insertWidget(7, functionDropDown_);
	
	//connect the index change signal to the change node function
	connect(functionDropDown_, SIGNAL(currentIndexChanged(int)), this, SLOT(changedNodeFunction(int)));
}


/// <summary>
/// Builds the property widget elements.
/// </summary>
/// <param name="container">The layout container.</param>
/// <param name="type">The property type.</param>
void NodeBuilder::buildPropertyWidgetElement(QVBoxLayout* container, propertyType type)
{
	//check to see if the current property is valid
	if(currentProperty_ != NULL)
	{
		//if so remove it to allow for the new node
		//to be create as the previous property was not added
		//otherwise it would have been null
		delete currentProperty_;
		currentProperty_ = NULL;
	}

	//if there was a property widget remove it and delete it
	//ready for a new one
	if(propertyWidget_ != NULL)
	{
		container->removeWidget(propertyWidget_);
		delete propertyWidget_;
		propertyWidget_ = NULL;
	}
	
	//check the type of property
	switch(type)
	{
	case BOOL_PROP:
		//create the property with default value
		currentProperty_ = new Property<bool>((Node*)currentNode_, BOOL_PROP, false);
		//set the name of the property to that of the function
		currentProperty_->setName(functionDropDown_->itemText(functionDropDown_->currentIndex()).toStdString());
		//create the widget to display the property
		propertyWidget_ = new PropertyBoolWidget(currentProperty_);
		break;
	case INT_PROP:
		currentProperty_ = new Property<int>((Node*)currentNode_, INT_PROP, 0);
		currentProperty_->setName(functionDropDown_->itemText(functionDropDown_->currentIndex()).toStdString());
		propertyWidget_ = new PropertyNumericWidget(currentProperty_);
		break;
	case FLOAT_PROP:
		currentProperty_ = new Property<float>((Node*)currentNode_, FLOAT_PROP, 0.0f);
		currentProperty_->setName(functionDropDown_->itemText(functionDropDown_->currentIndex()).toStdString());
		propertyWidget_ = new PropertyNumericWidget(currentProperty_);
		break;
	case STRING_PROP:
		currentProperty_ = new Property<std::string>((Node*)currentNode_, STRING_PROP, "");
		currentProperty_->setName(functionDropDown_->itemText(functionDropDown_->currentIndex()).toStdString());
		propertyWidget_ = new PropertyStringWidget(currentProperty_);
		break;
	case VEC2_PROP:
		currentProperty_ = new Property<vector2df>((Node*)currentNode_, VEC2_PROP, vector2df(0.0f));
		currentProperty_->setName(functionDropDown_->itemText(functionDropDown_->currentIndex()).toStdString());
		propertyWidget_ = new PropertyVec2Widget(currentProperty_);
		break;
	case VEC3_PROP:
		currentProperty_ = new Property<vector3df>((Node*)currentNode_, VEC3_PROP, vector3df(0.0f));
		currentProperty_->setName(functionDropDown_->itemText(functionDropDown_->currentIndex()).toStdString());
		propertyWidget_ = new PropertyVec3Widget(currentProperty_);
		break;
	case COLOUR_PROP:
		currentProperty_ = new Property<SColor>((Node*)currentNode_, COLOUR_PROP, SColor(0,0,0,0));
		currentProperty_->setName(functionDropDown_->itemText(functionDropDown_->currentIndex()).toStdString());
		propertyWidget_ = new PropertyColourWidget(currentProperty_);
		break;
	}
	
	//if the property widget is valid add it to the layout
	if(propertyWidget_ != NULL)
		container->insertWidget(8, propertyWidget_);
}

/// <summary>
/// Builds the property table element.
/// </summary>
/// <param name="container">The layout container.</param>
void NodeBuilder::buildPropertyTableElement(QVBoxLayout* container)
{
	//create the property table label and the table
	propertyTableLable_ = new QLabel(tr("Node Properties"));
	propertyTable_ = new QTableWidget(0,4); //this will need to be changed

	//set the table labels
	QStringList labels;
	labels << tr("Uuid") << tr("Name") << tr("IrrlichtNode Name") << tr("IrrlichtNode Type");
	propertyTable_->setHorizontalHeaderLabels(labels);
	propertyTable_->resizeColumnToContents(0);
	propertyTable_->horizontalHeader()->setStretchLastSection(true);

	//add the two widgets to the layout
	container->addWidget(propertyTableLable_);
	container->addWidget(propertyTable_);
}


/// <summary>
/// Updates the table properties.
/// </summary>
void NodeBuilder::UpdateTable()
{
	//clear all the elements in the table
	propertyTable_->clearContents();

	std::vector<CompleteProperty>::iterator it;
	int row = 0;
	//loop through all the completed properties
	for (it = completedProperties_.begin(); it != completedProperties_.end(); it++)
	{
		//convert the information into a string and add it as a table widget item
		QTableWidgetItem* UuidItem = new QTableWidgetItem(it->prop->getUuid().toString());
		propertyTable_->setItem(row, 0, UuidItem);
		QTableWidgetItem* nameItem = new QTableWidgetItem(QString::fromStdString(it->prop->getName()));
		propertyTable_->setItem(row, 1, nameItem);
		QTableWidgetItem* irrnameItem = new QTableWidgetItem(QString::fromStdString(it->irrName));
		propertyTable_->setItem(row, 2, irrnameItem);
		QTableWidgetItem* typeItem = new QTableWidgetItem(nodeWidget_->getNodeTypeName(it->irrtype));
		propertyTable_->setItem(row, 3, typeItem);
		//increase the row
		row++;
	}
}

/// <summary>
/// Updates the node list which in turn updates the irrlicht node selector
/// drop down.
/// </summary>
void NodeBuilder::updateNodeList()
{
	//check if the selector is valid
	if(irrNodeSelectorDropDown_)
	{
		//if so clear it and the function drop down as
		//that will also change then the new node is added
		irrNodeSelectorDropDown_->clear();
		functionDropDown_->clear();
	}

	//get all the added irrlicht node types from the irrlicht node builder
	std::unordered_map<std::string, IrrNodeType> avaliableNodes = nodeWidget_->getNodeList();
	std::unordered_map<std::string, IrrNodeType>::iterator it;
	for (it = avaliableNodes.begin(); it != avaliableNodes.end(); it++)
	{
		//add the item to the selector drop down
		irrNodeSelectorDropDown_->addItem(QString::fromStdString(it->first), (int)it->second);
	}
}

/// <summary>
/// Updates the function combo box.
/// </summary>
/// <param name="nodeTypeID">The node type identifier.</param>
void NodeBuilder::updateFunctionCombo(int nodeTypeID)
{
	//check if its valid and if so clear it
	if(functionDropDown_)
		functionDropDown_->clear();

	//check to see if the node type is valid
	if (nodeTypeID != IRR_NO_NODE)
	{	
		//get the irrlicht node type function callback info list
		std::unordered_map<std::string, CallBackInfo> callbacks = NodeCallBacks::getCallbackList((IrrNodeType)nodeTypeID);
		std::unordered_map<std::string, CallBackInfo>::iterator it;
		for (it = callbacks.begin(); it != callbacks.end(); it++)
		{
			//add all the names of the function and the node type that they relate to
			functionDropDown_->addItem(QString::fromStdString(it->first), nodeTypeID);
		}
	}
	else if(nodeTypeID == IRR_NO_NODE)
	{
		//!NOT IMPLEMENTED!
		//This should change the function drop down to a property type drop down and add a name property text box
	}

	//add the no callback item which doesnt do anything at the moment 
	//but should allow for custom properties to be added
	functionDropDown_->addItem(tr("No CallBack"));
}

/// <summary>
/// Gets the current call back information.
/// </summary>
/// <returns>The callback info of the current selected function</returns>
CallBackInfo NodeBuilder::getCurrentCallBackInfo()
{
	//check to see if the function is not no callback
	if (functionDropDown_->currentText() != tr("No CallBack"))
	{
		std::unordered_map<std::string, CallBackInfo> callbacks;
		//get all the callbacks for the selected irrlicht node type
		callbacks = NodeCallBacks::getCallbackList((IrrNodeType)functionDropDown_->itemData(functionDropDown_->currentIndex()).toInt());
		//return the callback info
		return callbacks[functionDropDown_->currentText().toStdString()];
	}

	//if not found return a empty callback
	return CallBackInfo();
}