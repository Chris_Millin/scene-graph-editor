#include "customnode.h"

CustomNode::~CustomNode(void)
{

}

/// <summary>
/// Adds a irrlicht node into the node reference map
/// </summary>
/// <param name="name">The name.</param>
/// <param name="node">The node.</param>
/// <param name="typeID">The irrlicht node type identifier.</param>
void CustomNode::addNode(std::string name, ISceneNode* node, int typeID)
{
	//Create a new scene node structure
	SceneNode nodedata;
	//set the node
	nodedata.node = node;
	//set the type
	nodedata.irrType = typeID;
	//add it to the map via its name (which is unique as duplicate names cannot
	//be added in the node builder)
	nodeReference_[name] = nodedata;
}

/// <summary>
/// Add a property to the property map without a node reference.
/// </summary>
/// <param name="property">The property.</param>
void CustomNode::addProperty(PropertyID* property)
{
	//create a new property node info
	propertyNode prop;
	//Set the irrlicht node to null
	prop.node = NULL;
	//Set the property
	prop.property = property;

	//add the property to the property map using its Uuid as the key in string form
	nodeProperties_[property->getUuid().toString().toStdString()] = prop;
}

/// <summary>
/// Add a property to the property map with a node reference.
/// </summary>
/// <param name="property">The property.</param>
/// <param name="NodeName">Name of the irrlicht node.</param>
void CustomNode::addProperty(PropertyID* property, std::string& NodeName)
{
	//find the irrlicht node in the node reference map
	NodeReference::iterator it = nodeReference_.find(NodeName);

	//create the new property info
	propertyNode prop;

	//if the iterator did not reach the end in the find then add the
	//node it did fine otherwise set it to null
	if (it != nodeReference_.end())
		prop.node = it->second.node;
	else
		prop.node = NULL;

	//Set the property
	prop.property = property;

	//add the property to the property map using its Uuid as the key in string form
	nodeProperties_[property->getUuid().toString().toStdString()] = prop;
}

/// <summary>
/// Removes the property.
/// !NOT IMPLEMENTED!
/// </summary>
/// <param name="property">The property.</param>
/// <param name="NodeName">Name of the node.</param>
void CustomNode::removeProperty(PropertyID* property, std::string& NodeName)
{

}

/// <summary>
/// Gets the irrlicht node from property.
/// </summary>
/// <param name="prop">The property.</param>
/// <returns>irrlicht scene node</returns>
ISceneNode* CustomNode::getNodeFromProperty(PropertyID* prop)
{
	//create an iterator for the node property map (This could be done
	//with a find using the Uuid!)
	NodePropertyMap::iterator it = nodeProperties_.begin();
	for (it; it != nodeProperties_.end(); it++)
	{
		//if the property Uuid is the same then the property has been found
		if(it->second.property->getUuid() == prop->getUuid())
		{
			//return the irrlicht node
			return it->second.node;
		}
	}

	//if not found return null
	return NULL;
}

/// <summary>
/// Gets the node type from node.
/// </summary>
/// <param name="node">The node.</param>
/// <returns>The irrlicht node type</returns>
int CustomNode::getNodeTypeFromNode(ISceneNode* node)
{
	//Iterate through the node reference
	NodeReference::iterator it = nodeReference_.begin();
	for (it; it != nodeReference_.end(); it++)
	{
		//if the node is the same then return the type
		if(it->second.node == node)
		{
			return it->second.irrType;
		}
	}

	//if not found return minus 1 which should not be a node type
	return -1;
}

/// <summary>
/// Gets the node name from property.
/// </summary>
/// <param name="property">The property.</param>
/// <returns>The name of the node</returns>
std::string CustomNode::getNodeNameFromProperty(PropertyID* property)
{
	//get the node from the property
	ISceneNode* node = getNodeFromProperty(property);
	//iterate through the node reference to find the same node
	NodeReference::iterator it = nodeReference_.begin();
	for (it; it != nodeReference_.end(); it++)
	{
		//if found return the name of the node
		if(it->second.node == node)
		{
			return it->first;
		}
	}

	//otherwise return a string with NULL
	return std::string("NULL");
}

/// <summary>
/// change the position of the node if it contains a position property.
/// the position will be changed for all properties with the position
/// name.
/// </summary>
/// <param name="change">change value.</param>
void CustomNode::setPositionChange(vector3df change)
{
	//iterate through all the properties
	NodePropertyMap::iterator it = nodeProperties_.begin();
	for (it; it != nodeProperties_.end(); it++)
	{
		//if the name is "Position" then obtain the property and is a vector 3
		if(it->second.property->getName() == "Position" && it->second.property->getTypeId() == VEC3_PROP)
		{
			//get the property value and updates it
			Property<vector3df>* propertyData = dynamic_cast<Property<vector3df>*>(it->second.property);
			propertyData->setProperty(propertyData->getPropertyasType() + change);
		}
	}
}

/// <summary>
/// changes the rotation of the node if it contains the rotation property.
/// The rotation property will be changed for all properties with the rotation name
/// </summary>
/// <param name="change">change value.</param>
void CustomNode::setRotationChange(vector3df change)
{
	NodePropertyMap::iterator it = nodeProperties_.begin();
	for (it; it != nodeProperties_.end(); it++)
	{
		if(it->second.property->getName() == "Rotation" && it->second.property->getTypeId() == VEC3_PROP)
		{
			Property<vector3df>* propertyData = dynamic_cast<Property<vector3df>*>(it->second.property);
			propertyData->setProperty(propertyData->getPropertyasType() + change);
		}
	}
}

/// <summary>
/// changes the scale of the node if it contains the scale property.
/// The scale property will be changed for all properties with the scale name
/// </summary>
/// <param name="change">change value.</param>
void CustomNode::setScaleChange(vector3df change)
{
	NodePropertyMap::iterator it = nodeProperties_.begin();
	for (it; it != nodeProperties_.end(); it++)
	{
		if(it->second.property->getName() == "Scale" && it->second.property->getTypeId() == VEC3_PROP)
		{
			Property<vector3df>* propertyData = dynamic_cast<Property<vector3df>*>(it->second.property);
			propertyData->setProperty(propertyData->getPropertyasType() + change);
		}
	}
}

/// <summary>
/// change the position for all children nodes.
/// This should be used when modifying the position property of the
/// a node
/// </summary>
/// <param name="change">change value.</param>
void CustomNode::setChildrenPosition(vector3df change)
{
	for(int i = 0; i < getChildCount(); i++)
	{
		((CustomNode*)getChildren()[i])->setPositionChange(change);
	}
}

/// <summary>
/// change the rotation for all children nodes.
/// This should be used when modifying the rotation property of the
/// a node
/// </summary>
/// <param name="change">change value.</param>
void CustomNode::setChildrenRotation(vector3df change)
{
	for(int i = 0; i < getChildCount(); i++)
	{
		((CustomNode*)getChildren()[i])->setRotationChange(change);
	}
}

/// <summary>
/// change the scale for all children nodes.
/// This should be used when modifying the scale property of the
/// a node
/// </summary>
/// <param name="change">change value.</param>
void CustomNode::setChildrenScale(vector3df change)
{
	for(int i = 0; i < getChildCount(); i++)
	{
		((CustomNode*)getChildren()[i])->setScaleChange(change);
	}
}