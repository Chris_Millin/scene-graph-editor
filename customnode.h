#ifndef CUSTOMNODE_H
#define CUSTOMNODE_H

#include <irrlicht.h>
#include "node.h"
#include "Property.h"
#include <unordered_map>
using namespace irr;
using namespace scene;
using namespace core;

/// <summary>
/// Property Node which stores the property
/// and the linked irrlicht node
/// </summary>
struct propertyNode
{
	PropertyID* property;
	ISceneNode* node;
};

/// <summary>
/// Scene node which stores the irrlicht node and its type
/// </summary>
struct SceneNode
{
	ISceneNode* node;
	int irrType;
};

typedef std::unordered_map<std::string, propertyNode> NodePropertyMap;
typedef std::unordered_map<std::string, SceneNode> NodeReference;

/// <summary>
/// The Custom node class is an extension from the node class
/// the original plan was to have multiple node types and this was
/// to be one of them but the other ones have been removed due to time constraints
/// and re-reviewing the design it may be best to have a single custom node
/// which can contain anything.
/// To add more node types it would be best to inherit from custom node and
/// build functions to build the node directly.
/// </summary>
class CustomNode : public Node
{
public:
    CustomNode(std::string name, Node* parentNode) : Node(name, parentNode) { nodeType = CUSTOM_NODE; }
	virtual ~CustomNode(void);

	/// <summary>
	/// function to return if the node contains any properties
	/// </summary>
	/// <returns>If the node has properties</returns>
	virtual bool hasProperties() { return nodeProperties_.size() > 0 ? true : false; }

	virtual void addNode(std::string name, ISceneNode* node, int typeID);
	virtual void addProperty(PropertyID* property);
	virtual void addProperty(PropertyID* property, std::string& NodeName);
	virtual void removeProperty(PropertyID* property, std::string& NodeName);
	virtual ISceneNode* getNodeFromProperty(PropertyID* property);
	virtual int getNodeTypeFromNode(ISceneNode* node);
	virtual std::string getNodeNameFromProperty(PropertyID* property);

	virtual void setPositionChange(vector3df change);
	virtual void setRotationChange(vector3df change);
	virtual void setScaleChange(vector3df change);

	virtual void setChildrenPosition(vector3df change);
	virtual void setChildrenScale(vector3df change);
	virtual void setChildrenRotation(vector3df change);

	/// <summary>
	/// Gets the properties.
	/// </summary>
	/// <returns>returns the property map</returns>
	virtual NodePropertyMap getProperties() { return nodeProperties_; }
	/// <summary>
	/// Gets the nodes reference map.
	/// </summary>
	/// <returns>returns the node reference map</returns>
	virtual NodeReference getNodes() { return nodeReference_; }

private:

private:
	
	/// <summary>
	///This is the node property map which contains all the properties added to the
	///node. It also contains a link to the irrlicht node stored in the node reference map
	///so the property can be associated with a irrlicht node.
	/// </summary>
	NodePropertyMap nodeProperties_;
	
	/// <summary>
	///The map containing all the irrlicht nodes added to the node. This is used by the
	///property map to reference a node
	/// </summary>
	NodeReference nodeReference_;
};

#endif // CUSTOMNODE_H
