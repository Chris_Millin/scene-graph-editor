#include "editorwindow.h"

/// <summary>
/// The irrlicht device used for all things irrlicht
/// </summary>
IrrlichtDevice* RenderContext::renderDevice_;

EditorWindow::EditorWindow(QWidget *parent) : QWidget(parent)
{
	// Wait for the init() call
	renderDevice_ = 0;

	// Default to Open GL
	driverType_ = irr::video::EDT_OPENGL;
}

EditorWindow::~EditorWindow(void)
{
}

/// <summary>
/// Automatics the update scene.
/// This function is triggered from a Qt signal
/// </summary>
void EditorWindow::autoUpdateScene()
{
	//update the timer
	renderDevice_->getTimer()->tick();

	//Set the back buffer colour
	irr::video::SColor color(0, 126, 126, 126);

	//Start the scene rendering
	renderDevice_->getVideoDriver()->beginScene( true, true, color);

	//Draw any objects in the scene manager
	renderDevice_->getSceneManager()->drawAll();
	//Draw any objects in the GUI environment
	renderDevice_->getGUIEnvironment()->drawAll();

	//End the current scene rendering
	renderDevice_->getVideoDriver()->endScene();

	//Debug camera information
	//irr::scene::ICameraSceneNode *cam = renderDevice_->getSceneManager()->getActiveCamera();
	//printf("cam x: %f, y: %f, z: %f\n", cam->getPosition().X, cam->getPosition().Y, cam->getPosition().Z);
}



/// <summary>
/// If Required this can be linked into the scene list to
/// obtain a node that is selected from the scene list
/// !NOT IMPLEMENTED!
/// </summary>
/// <param name="node">The node.</param>
void EditorWindow::SceneListSelectedNode(Node* node)
{

}

/// <summary>
/// Key press event for QWidget.
/// </summary>
/// <param name="event">The event.</param>
void EditorWindow::keyPressEvent(QKeyEvent* event)
{
	//ensure the render device is active
	if ( renderDevice_ != 0 )
	{
		//send the event to irrlicht
		sendKeyEventToIrrlicht( event, true );
	}
	//Qt should not handle the key press any more
	event->ignore();
}

/// <summary>
/// Key release event for QWidget.
/// </summary>
/// <param name="event">The event.</param>
void EditorWindow::keyReleaseEvent(QKeyEvent* event)
{
	//Same as key press event but is triggered
	//when a key is released
	if ( renderDevice_ != 0 )
	{
		sendKeyEventToIrrlicht( event, false );
	}
	event->ignore();
}

/// <summary>
/// Mouse press event for QWidget.
/// </summary>
/// <param name="event">The event.</param>
void EditorWindow::mousePressEvent(QMouseEvent* event)
{
	//Check the render device is valid
	if ( renderDevice_ != 0 )
	{
		//If the right mouse button is pressed
		//The camera should get control and the
		//Keyboard should also be obtained by the widget
		//to listen for key input
		if(event->button() == Qt::RightButton)
		{
			//Give the camera control
			camera_->setInputReceiverEnabled(true);
			//Obtain the keyboard
			this->grabKeyboard();
		}

		//Send the mouse event to irrlicht
		sendMouseEventToIrrlicht( event, true );
	}
	event->ignore();
}

/// <summary>
/// Mouse release event for QWidget.
/// </summary>
/// <param name="event">The event.</param>
void EditorWindow::mouseReleaseEvent(QMouseEvent* event)
{
	//Check the device is valid
	if ( renderDevice_ != 0 )
	{
		//if the mouse button is released then 
		//the camera needs to remove its control
		//and the keyboard needs to be given back
		if(event->button() == Qt::RightButton)
		{
			//Disable input
			camera_->setInputReceiverEnabled(false);
			//release the keyboard back to which
			//ever Qt widget requires it
			this->releaseKeyboard();
		}

		//Send the mouse event to irrlicht
		sendMouseEventToIrrlicht( event, false );
	}
	event->ignore();
}

/// <summary>
/// Mouse move event for QWidget.
/// </summary>
/// <param name="event">The event.</param>
void EditorWindow::mouseMoveEvent( QMouseEvent* event )
{
	//Create a irrlicht event
	irr::SEvent irrEvent;

	//Set the event type to mouse
	irrEvent.EventType = irr::EET_MOUSE_INPUT_EVENT;

	//check for valid render device
	if ( renderDevice_ != 0 )
	{
		//Set the type of event 
		irrEvent.MouseInput.Event = irr::EMIE_MOUSE_MOVED;
		//Set the mouse movement change
		irrEvent.MouseInput.X = event->x();
		irrEvent.MouseInput.Y = event->y();
		//set the mouse wheel value to 0
		irrEvent.MouseInput.Wheel = 0.0f; // Zero is better than undefined
		//send the event to irrlicht
		if(renderDevice_->postEventFromUser( irrEvent ))
			event->accept();
	}
}

/// <summary>
/// Mouse Wheel event for QWidget.
/// </summary>
/// <param name="event">The event.</param>
void EditorWindow::wheelEvent(QWheelEvent* event)
{
	if ( renderDevice_ != 0 && event->orientation() == Qt::Vertical )
	{
		irr::SEvent irrEvent;

		irrEvent.EventType = irr::EET_MOUSE_INPUT_EVENT;

		irrEvent.MouseInput.Event = irr::EMIE_MOUSE_WHEEL;
		irrEvent.MouseInput.X = 0; // We don't know these,
		irrEvent.MouseInput.Y = 0; // but better zero them instead of letting them be undefined
		irrEvent.MouseInput.Wheel = event->delta() / 120.0f;

		renderDevice_->postEventFromUser( irrEvent );
	}
	event->ignore();	
}

/// <summary>
/// Resize event for QWidget.
/// </summary>
/// <param name="event">The event.</param>
void EditorWindow::resizeEvent(QResizeEvent* event)
{
	//Check the render device is valid
	if ( renderDevice_ != 0 )
	{
		//set the new size value
		irr::core::dimension2d<irr::u32> size;
		size.Width = event->size().width();
		size.Height = event->size().height();
		//Set the size to irrlicht
		renderDevice_->getVideoDriver()->OnResize( size );

		//There was an issue with this where the aspect ratio would become
		//invalid if not around 4:3 and cause the objects in the scene not to be displayed
		irr::scene::ICameraSceneNode *cam = renderDevice_->getSceneManager()->getActiveCamera();
		if ( cam != 0 )
		{
			//cam->setAspectRatio( size.Height / size.Width );
		}
	}
	//call the base class resize function
	QWidget::resizeEvent(event);
}

/// <summary>
/// Paint event for QWidget.
/// </summary>
/// <param name="event">The event.</param>
void EditorWindow::paintEvent(QPaintEvent* event)
{
	//check the device is valid
	if ( renderDevice_ != 0 )
		emit updateScene(); //if valid emit the update scene which will render the scene with irrlicht
}

/// <summary>
/// Timer event for QWidget.
/// </summary>
/// <param name="event">The event.</param>
void EditorWindow::timerEvent(QTimerEvent* event)
{
	//perform the same task as paint to ensure the scene is updated
	if ( renderDevice_ != 0 )
		emit updateScene();
	event->accept();
}

/// <summary>
/// Initalises the context.
/// </summary>
void EditorWindow::initaliseContext()
{
	// Don't initialize more than once!
	if (renderDevice_ != 0 ) renderDevice_->drop();

	//Set the device paramaters
	irr::SIrrlichtCreationParameters params;

	params.DriverType = driverType_;
	params.WindowId = winId(); //This is obtained from QWidget which is the current window HWND
	params.WindowSize.Width = width();
	params.WindowSize.Height = height();

	//Create the render device
	renderDevice_ = createDeviceEx(params);

	// Irrlicht will clear the canvas for us, so tell Qt not to do it
	setAttribute( Qt::WA_OpaquePaintEvent );

	//Connect up the update scene signal with the auto update slot to render the scene
	connect(this, SIGNAL(updateScene()), this, SLOT(autoUpdateScene()));

	//Start the timer for the QWidget
	startTimer(0);

	//Get the scene manager to add the camera to the scene which will be used to
	//move around the scene
	scene::ISceneManager* manager = renderDevice_->getSceneManager();
	//Since the FPS camera provides all the controls that are needed to move around the scene 
	//it can be used as the main camera
	camera_ = manager->addCameraSceneNodeFPS(0,50.0f,0.5f);

	//Set camera informtion and disable the input
	camera_->setPosition(core::vector3df(0,0,0));
	camera_->setTarget(core::vector3df(0,0,0));
	camera_->setFarValue(42000.0f);
	camera_->setInputReceiverEnabled(false);
	//Set the camera as the active camera
	manager->setActiveCamera(camera_);

	//Add the event filter to the QWidget which will allow from knowledge of when the mouse
	//
	this->installEventFilter(this);
}


/// <summary>
/// Cast a ray.
/// This function should implement a ray cast into the scene to
/// obtain an irrlicht node which can then be translated into
/// a Node and selected similar to the way the scene list works
/// !NOT IMPLEMENTED!
/// </summary>
void EditorWindow::castRay()
{

}

/// <summary>
/// Finds a node from irrlicht node.
/// This should be implemented with the castRay function
/// to obtain a Node from the scene
/// </summary>
/// <param name="node">The node.</param>
/// <returns>A node object</returns>
Node* EditorWindow::findNodeFromIrrlichtNode(ISceneNode* node)
{
	return NULL;
}

/// <summary>
/// Send key event to irrlicht.
/// </summary>
/// <param name="event">The event.</param>
/// <param name="pressedDown">pressed down.</param>
void EditorWindow::sendKeyEventToIrrlicht(QKeyEvent* event, bool pressedDown)
{
	//Create the irrlicht event
	irr::SEvent irrEvent;

	//Set the event type
	irrEvent.EventType = irr::EET_KEY_INPUT_EVENT;

	//Convert the key
	SIrrlichtKey irrKey = convertKey( event->key() );

	if ( irrKey.code == 0 ) return; // Could not find a match for this key

	//Set the key event
	irrEvent.KeyInput.Key = irrKey.code;
	irrEvent.KeyInput.Control = ((event->modifiers() & Qt::ControlModifier) != 0);
	irrEvent.KeyInput.Shift = ((event->modifiers() & Qt::ShiftModifier) != 0);
	irrEvent.KeyInput.Char = irrKey.ch;
	irrEvent.KeyInput.PressedDown = pressedDown;

	//Send the event to irrlicht
	renderDevice_->postEventFromUser( irrEvent );
}

/// <summary>
/// Send mouse event to irrlicht.
/// </summary>
/// <param name="event">The event.</param>
/// <param name="pressedDown">pressed down.</param>
void EditorWindow::sendMouseEventToIrrlicht(QMouseEvent* event, bool pressedDown)
{
	//Create the irrlicht event
	irr::SEvent irrEvent;

	//Set the event type
	irrEvent.EventType = irr::EET_MOUSE_INPUT_EVENT;

	//Check the buttons
	switch ( event->button() )
	{
	case Qt::LeftButton:
		irrEvent.MouseInput.Event = pressedDown? irr::EMIE_LMOUSE_PRESSED_DOWN:irr::EMIE_LMOUSE_LEFT_UP;
		break;

	case Qt::MidButton:
		irrEvent.MouseInput.Event = pressedDown? irr::EMIE_MMOUSE_PRESSED_DOWN:irr::EMIE_MMOUSE_LEFT_UP;
		break;

	case Qt::RightButton:
		irrEvent.MouseInput.Event = pressedDown? irr::EMIE_RMOUSE_PRESSED_DOWN:irr::EMIE_RMOUSE_LEFT_UP;
		break;

	default:
		return; // Cannot handle this mouse event
	}

	//Set the x and y values
	irrEvent.MouseInput.X = event->x();
	irrEvent.MouseInput.Y = event->y();
	
	//Set the mouse wheel
	irrEvent.MouseInput.Wheel = 0.0f; // Zero is better than undefined

	//Post the event to irrlicht
	renderDevice_->postEventFromUser( irrEvent );
}

/// <summary>
/// Converts the key from Qt to irrlicht.
/// </summary>
/// <param name="key">The key.</param>
/// <returns></returns>
SIrrlichtKey EditorWindow::convertKey(int key)
{
	SIrrlichtKey irrKey;
	irrKey.code = (irr::EKEY_CODE)(0);
	irrKey.ch = (wchar_t)(0);

	// Letters A..Z and numbers 0..9 are mapped directly
	if ( (key >= Qt::Key_A && key <= Qt::Key_Z) || (key >= Qt::Key_0 && key <= Qt::Key_9) )
	{
		irrKey.code = (irr::EKEY_CODE)( key );
		irrKey.ch = (wchar_t)( key );
	}
	else

		// Dang, map keys individually
		switch( key )
	{
		case Qt::Key_Up:
			irrKey.code = irr::KEY_UP;
			break;

		case Qt::Key_Down:
			irrKey.code = irr::KEY_DOWN;
			break;

		case Qt::Key_Left:
			irrKey.code = irr::KEY_LEFT;
			break;

		case Qt::Key_Right:
			irrKey.code = irr::KEY_RIGHT;
			break;
	}
	return irrKey;
}

/// <summary>
/// Events the filter.
/// This was used to check if the mouse entered or left the widget to trigger the
/// camera on and off. This how now been removed as it caused issues with
/// the mouse getting out of the scene. The right click method works
/// better. This function is still avaliable if any filter events
/// are required
/// </summary>
/// <param name="watched">The watched.</param>
/// <param name="event">The event.</param>
/// <returns>event filter done</returns>
bool EditorWindow::eventFilter(QObject *watched, QEvent *event)
{
	//bool enabled = false;
	//if (event->type() == QEvent::Enter)
	//{
	//	camera_->setInputReceiverEnabled(true);
	//}
	//if (event->type() == QEvent::Leave )
	//{
	//	camera_->setInputReceiverEnabled(false);
	//}
	return QWidget::eventFilter(watched, event);
}

