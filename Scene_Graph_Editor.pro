#-------------------------------------------------
#
# Project created by QtCreator 2014-02-17T19:20:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Scene_Graph_Editor
TEMPLATE = app


SOURCES += \
    actionhandler.cpp \
    animeshnode.cpp \
    cameranode.cpp \
    customnode.cpp \
    editorwindow.cpp \
    erroroutput.cpp \
    handle.cpp \
    main.cpp \
    mainwindow.cpp \
    meshnode.cpp \
    node.cpp \
    nodebuilder.cpp \
    portalnode.cpp \
    positionhandle.cpp \
    PropertyCallBacks.cpp \
    PropertyWidgets.cpp \
    propertywindow.cpp \
    rendercontext.cpp \
    rotationhandle.cpp \
    scalehandle.cpp \
    scene.cpp \
    sceneio.cpp \
    scenelist.cpp \
    scenewindow.cpp \
    soundnode.cpp \
    terrainnode.cpp \
    toolbar.cpp

HEADERS  += \
    actionhandler.h \
    animeshnode.h \
    cameranode.h \
    customnode.h \
    editorwindow.h \
    erroroutput.h \
    handle.h \
    mainwindow.h \
    meshnode.h \
    node.h \
    nodebuilder.h \
    portalnode.h \
    positionhandle.h \
    Property.h \
    PropertyCallBacks.h \
    PropertyWidgets.h \
    propertywindow.h \
    rendercontext.h \
    roomnode.h \
    rotationhandle.h \
    scalehandle.h \
    scene.h \
    sceneio.h \
    scenelist.h \
    scenewindow.h \
    soundnode.h \
    terrainnode.h \
    toolbar.h
