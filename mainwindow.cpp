#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
	windowToolBar_ = NULL;

	editorWidgetWindow_		= NULL;
	propertyWidgetWindow_	= NULL;
	sceneWidgetWindow_		= NULL;

	builder_ = NULL;

	activeScene_ = NULL;
	
}

MainWindow::~MainWindow()
{
	if (windowToolBar_)
	{
		delete windowToolBar_;
		windowToolBar_ = NULL;
	}
	if (editorWidgetWindow_)
	{
		delete editorWidgetWindow_;
		editorWidgetWindow_ = NULL;
	}
	if (propertyWidgetWindow_)
	{
		delete propertyWidgetWindow_;
		propertyWidgetWindow_ = NULL;
	}
	if (sceneWidgetWindow_)
	{
		delete sceneWidgetWindow_;
		sceneWidgetWindow_ = NULL;
	}
	if(activeScene_)
	{
		delete activeScene_;
		activeScene_ = NULL;
	}
	if(builder_)
	{
		delete builder_;
		builder_ = NULL;
	}
}

/// <summary>
/// initalises the window.
/// </summary> 
void MainWindow::initaliseWindow()
{
	//construct a new scene instance
	newScene();

	//initalise the toolbar widget
	initaliseToolBar();
	//initalise all the other window widgets
	initaliseWidgetWindows();
}

/// <summary>
/// Initalises the tool bar.
/// </summary>
void MainWindow::initaliseToolBar()
{
	//If the tool bar is null then a new instance can be created
	if(!windowToolBar_)
		windowToolBar_ = new ToolBar(this->menuBar()); //The menu bar from the main window is used to construct the tool bar

	//initalise the tool bar
	windowToolBar_->initaliseToolBar();
	//let the tool bar know what scene is currently being used
	windowToolBar_->setScene(activeScene_);
	//link up the signal and slots for creating a new scene and opening the node builder
	//which are controlled from the main window rather than from the tool bar
	connect(windowToolBar_, SIGNAL(buildNewScene()), this, SLOT(newScene()));
	connect(windowToolBar_, SIGNAL(openNodeBuilder()), this, SLOT(openNodeBuilder()));
}

/// <summary>
/// Initalises the widget windows.
/// </summary>
void MainWindow::initaliseWidgetWindows()
{
	//If there is no scene widget construct one
	if(!sceneWidgetWindow_)
	{
		sceneWidgetWindow_ = new SceneWindow(this);
		//initalise the tree with the current scene. The scene widget will also save a pointer to the scene
		//so it can use it again so this must be called whenever a new scene is constructed
		sceneWidgetWindow_->initaliseSceneTree(activeScene_);
		//since the scene widget it a dockable widget which can be docked on the left of right
		//this is specified with QT.
		sceneWidgetWindow_->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
		//add the scene widget to the left dock
		this->addDockWidget(Qt::LeftDockWidgetArea, sceneWidgetWindow_);
	}

	//If there is no property widget construct one
	if(!propertyWidgetWindow_)
	{
		propertyWidgetWindow_ = new PropertyWindow(this);
		//The property widget will not need initalising as it only needs to run a setup when a node
		//is selected to display the information of that node
		//similar to the scene widget the property widget is also dockable to the left or right
		propertyWidgetWindow_->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
		//Add the dock widget to the current window but this time to the right
		this->addDockWidget(Qt::RightDockWidgetArea, propertyWidgetWindow_);
		//link the selected node signal from the scene widget with the property window so it knows when a node has been
		//selected
		connect(sceneWidgetWindow_, SIGNAL(selectedNode(Node*)), propertyWidgetWindow_, SLOT(sceneListSelectedNode(Node*)));
	}
	
	//if there is no editor widget construct a new one
	if(!editorWidgetWindow_)
	{
		editorWidgetWindow_ = new EditorWindow(this);
		//initalise the widget which will create a irrlicht render target in Qt
		editorWidgetWindow_->initaliseContext();
		//Since the editor will be core to viewing the scene it is positioned as the central widget
		this->setCentralWidget(editorWidgetWindow_);
	}
}

/// <summary>
/// Create a new scene instance
/// </summary>
void MainWindow::newScene()
{
	printf("creating new scene\n");
	//destroy the old scene
	delete activeScene_;
	//set the scene pointer back to null
	activeScene_ = NULL;
	//construct a new scene
	activeScene_ = new Scene();
	if(activeScene_ != NULL)
	{
		//initlaise the scene
		activeScene_->initaliseScene();
		
		//reset all the widgets with the new scene
		if (windowToolBar_)
			windowToolBar_->setScene(activeScene_);
		if(sceneWidgetWindow_)
			sceneWidgetWindow_->initaliseSceneTree(activeScene_);
		if (editorWidgetWindow_)
			editorWidgetWindow_->initaliseContext();
		if (propertyWidgetWindow_)
			propertyWidgetWindow_->clearProperties();
	}
	else
		close();
}

/// <summary>
/// Opens the node builder dialog.
/// </summary>
void MainWindow::openNodeBuilder()
{
	//construct a new node builder dialog
	builder_ = new NodeBuilder(this);
	//initalise the dialog window
	builder_->initaliseNodeBuilder();
	//show the window
	builder_->show();
	//connect the adding of the custom node to the active scene allowing the custom node to be consturcted;
	connect(builder_, SIGNAL(addCustomNode(CustomNode*, std::vector<CompleteProperty>)), activeScene_, SLOT(addCustomNode(CustomNode*, std::vector<CompleteProperty>)));
}

/// <summary>
/// Closes the node builder.
/// </summary>
void MainWindow::closeNodeBuilder()
{
	delete builder_;
}