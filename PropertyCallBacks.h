
#include "rendercontext.h"
#include "Property.h"
#include "customnode.h"
#include <irrlicht.h>
#include <unordered_map>
#include <string>
#pragma once

using namespace irr;
using namespace core;
using namespace video;
using namespace scene;

/// <summary>
/// Enumeration of the types of irrlicht nodes
/// </summary>
enum IrrNodeType
{
	IRR_MESH_NODE = 0,
	IRR_ANI_MESH_NODE = 1,
	IRR_CAMERA_NODE = 2,
	IRR_TERRAIN_NODE = 3,
	IRR_SOUND_NODE = 4,
	IRR_BILLBOARD_NODE = 5,
	IRR_BILLBOARD_TEXT_NODE = 6,
	IRR_LIGHT_NODE = 7,
	IRR_PARTICLE_EMITTER = 8,
	IRR_PARTICLE_ANI_MESH_NODE = 9,
	IRR_TEXT_NODE = 10,
	IRR_VOLUME_LIGHT_NODE = 11,
	IRR_NO_NODE
};

/// <summary>
/// Provides information about a callback function
/// which contains the associated node type,
/// callback pointer and the property type allowed
/// </summary>
struct CallBackInfo
{
	IrrNodeType			LinkedNode;
	propertyCallBack	Callback;
	int					PropertyType;
};

/// <summary>
/// This static class provides helper functionality to obtaining callback arrays
/// building a callback information (using with the static callback classes) and
/// help with attaching a callback to a property
/// </summary>
class NodeCallBacks
{
public:
	static std::unordered_map<std::string, CallBackInfo> getCallbackList(IrrNodeType type);
	static CallBackInfo buildCallBack(IrrNodeType node, propertyCallBack callback, int propertytype);

	static void attachCallback(PropertyID* prop, CallBackInfo callback);
};

/// <summary>
/// Contains the base callbacks for irrlicht scene nodes
/// </summary>
class SceneNodeCallbacks
{
public:
	static void nameCallBack(Node* node,PropertyID* property);
	static void positionCallBack(Node* node,PropertyID* property);
	static void rotationCallBack(Node* node,PropertyID* property);
	static void scaleCallBack(Node* node,PropertyID* property);
	static void visibleCallBack(Node* node,PropertyID* property);
};

/// <summary>
/// Contains the callbacks for a irrlicht mesh node
/// </summary>
class MeshNodeCallBacks : public SceneNodeCallbacks
{
public:
	static void materialTextureCallBack(Node* node, PropertyID* property);
	static void meshCallBack(Node* node, PropertyID* property);

	static std::unordered_map<std::string, CallBackInfo> getCallBackArray();
protected:
	static std::unordered_map<std::string, CallBackInfo> callbackArray_;
};

/// <summary>
/// Contains the callbacks for a irrlicht Animation mesh node
/// </summary>
class AniMeshNodeCallBacks : public  SceneNodeCallbacks
{
public:
	static void materialTextureCallBack(Node* node, PropertyID* property);
	static void meshCallBack(Node* node, PropertyID* property);
	static void animationSpeedCallBack(Node* node, PropertyID* property);
	static void currentFrameCallBack(Node* node, PropertyID* property);
	static void loopAnimationCallBack(Node* node, PropertyID* property);
	static void md2AnimationCallBack(Node* node, PropertyID* property);

	//should be read only
	static void startFrameCallBack(Node* node, PropertyID* property);
	static void endFrameCallBack(Node* node, PropertyID* property);
	static void framesCallBack(Node* node, PropertyID* property);

	static std::unordered_map<std::string, CallBackInfo> getCallBackArray();
private:
	static std::unordered_map<std::string, CallBackInfo> callbackArray_;
};

/// <summary>
/// Contains the callbacks for a irrlicht Camera node
/// !NOT IMPLEMENTED!
/// </summary>
class CameraNodeCallBacks : public  SceneNodeCallbacks
{
public:
	static void aspectRatioCallBack(Node* node, PropertyID* property);
	static void farViewCallBack(Node* node, PropertyID* property);
	static void fovCallBack(Node* node, PropertyID* property);
	static void nearViewCallBack(Node* node, PropertyID* property);
	static void projectionCallBack(Node* node, PropertyID* property);
	static void targetCallBack(Node* node, PropertyID* property);
	static void upVecCallBack(Node* node, PropertyID* property);

	static std::unordered_map<std::string, CallBackInfo> getCallBackArray();
private:
	static std::unordered_map<std::string, CallBackInfo> callbackArray_;
};

/// <summary>
/// Contains the callbacks for a irrlicht Terrain node
/// </summary>
class TerrainNodeCallBacks : public  SceneNodeCallbacks
{
public:
	static void hightMapCallBack(Node* node, PropertyID* property);
	static void textureScaleCallBack(Node* node, PropertyID* property);
	static void materialTextureCallBack(Node* node, PropertyID* property);

	static std::unordered_map<std::string, CallBackInfo> getCallBackArray();
private:
	static std::unordered_map<std::string, CallBackInfo> callbackArray_;
};

/// <summary>
/// Contains the callbacks for a custom irrlicht/irrklang sound node
/// !NOT IMPLEMENTED!
/// </summary>
class SoundNodeCallBacks : public  SceneNodeCallbacks
{
public:
	static void volumeCallBack(Node* node, PropertyID* property);
	static void upVecCallBack(Node* node, PropertyID* property);
	static void loopCallBack(Node* node, PropertyID* property);
	static void minDistanceCallBack(Node* node, PropertyID* property);
	static void maxDistanceCallBack(Node* node, PropertyID* property);
	static void speedCallBack(Node* node, PropertyID* property);

	//read only
	static void lengthCallBack(Node* node, PropertyID* property);

	static std::unordered_map<std::string, CallBackInfo> getCallBackArray();
private:
	static std::unordered_map<std::string, CallBackInfo> callbackArray_;

};

/// <summary>
/// Contains the callbacks for a custom irrlicht portal node
/// !NOT IMPLEMENTED!
/// </summary>
class PortalNodeCallBacks : public  SceneNodeCallbacks
{

};

/// <summary>
/// Contains the callbacks for a billboard node
/// !NOT IMPLEMENTED!
/// </summary>
class BillBoardNodeCallBacks : public  SceneNodeCallbacks
{
public:
	static void colourCallBack(Node* node, PropertyID* property);
	static void sizeCallBack(Node* node, PropertyID* property);
	static void materialTextureCallBack(Node* node, PropertyID* property);

	static std::unordered_map<std::string, CallBackInfo> getCallBackArray();
private:
	static std::unordered_map<std::string, CallBackInfo> callbackArray_;

};

/// <summary>
/// Contains the callbacks for a billboard text node
/// !NOT IMPLEMENTED!
/// </summary>
class BillBoardTextNodeCallBacks : public  BillBoardNodeCallBacks
{
public:
	static void textCallBack(Node* node, PropertyID* property);
	static void textColourCallBack(Node* node, PropertyID* property);

	static std::unordered_map<std::string, CallBackInfo> getCallBackArray();
private:
	static std::unordered_map<std::string, CallBackInfo> callbackArray_;
};

/// <summary>
/// Contains the callbacks for a Light node
/// !NOT IMPLEMENTED!
/// </summary>
class LightNodeCallBacks : public  SceneNodeCallbacks
{
public: 
	static void castShadowCallBack(Node* node, PropertyID* property);
	static void lightTypeCallBack(Node* node, PropertyID* property);
	static void radiusCallBack(Node* node, PropertyID* property);

	static std::unordered_map<std::string, CallBackInfo> getCallBackArray();
private:
	static std::unordered_map<std::string, CallBackInfo> callbackArray_;
};

/// <summary>
/// Contains the callbacks for a Particle Emitter
/// !NOT IMPLEMENTED!
/// </summary>
class ParticleEmitterCallBack
{
public: 
	static void directionCallBack(Node* node, PropertyID* property);
	static void maxAngleCallBack(Node* node, PropertyID* property);
	static void maxLifeCallBack(Node* node, PropertyID* property);
	static void maxPerSecCallBack(Node* node, PropertyID* property);
	static void maxColourCallBack(Node* node, PropertyID* property);
	static void maxStartSizeCallBack(Node* node, PropertyID* property);
	static void minLifeCallBack(Node* node, PropertyID* property);
	static void minPerSecCallBack(Node* node, PropertyID* property);
	static void minColourCallBack(Node* node, PropertyID* property);
	static void minStartSizeCallBack(Node* node, PropertyID* property);

	static std::unordered_map<std::string, CallBackInfo> getCallBackArray();
private:
	static std::unordered_map<std::string, CallBackInfo> callbackArray_;
};

/// <summary>
/// Contains the callbacks for a Particle Ani Mesh Node
/// !NOT IMPLEMENTED!
/// </summary>
class ParticleAniMeshNodeCallBacks : public ParticleEmitterCallBack, public  SceneNodeCallbacks
{
public:
	static void meshCallBack(Node* node, PropertyID* property);
	static void normalDirectionCallBack(Node* node, PropertyID* property);
	static void useNormalDirectionCallBack(Node* node, PropertyID* property);

	static std::unordered_map<std::string, CallBackInfo> getCallBackArray();
private:
	static std::unordered_map<std::string, CallBackInfo> callbackArray_;
};

/// <summary>
/// Contains the callbacks for a Text Node
/// !NOT IMPLEMENTED!
/// </summary>
class TextNodeCallBacks : public  SceneNodeCallbacks
{
public:
	static void textCallBack(Node* node, PropertyID* property);
	static void textColourCallBack(Node* node, PropertyID* property);

	static std::unordered_map<std::string, CallBackInfo> getCallBackArray();
private:
	static std::unordered_map<std::string, CallBackInfo> callbackArray_;
};

/// <summary>
/// Contains the callbacks for a Volume Light Node
/// !NOT IMPLEMENTED!
/// </summary>
class VolumeLightNodeCallBacks : public  SceneNodeCallbacks
{
public:
	static void footColourCallBack(Node* node, PropertyID* property);
	static void subDivUCallBack(Node* node, PropertyID* property);
	static void subDivVCallBack(Node* node, PropertyID* property);
	static void tailColourCallBack(Node* node, PropertyID* property);

	static std::unordered_map<std::string, CallBackInfo> getCallBackArray();
private:
	static std::unordered_map<std::string, CallBackInfo> callbackArray_;
};



