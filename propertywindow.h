#ifndef PROPERTYWINDOW_H
#define PROPERTYWINDOW_H

#include <QtGui>
#include <QWidget>
#include <QSizePolicy>
#include "node.h"
#include "customnode.h"
#include "PropertyWidgets.h"

/// <summary>
/// The property window is a dock widget which displays the
/// properties of a selected node object
/// </summary>
class PropertyWindow : public QDockWidget
{
    Q_OBJECT
public:
    explicit PropertyWindow(QWidget *parent = 0, Qt::WindowFlags flags = 0 );
	virtual ~PropertyWindow(void);
	void clearProperties() { clearLayout(propertyHolder_); }
signals:

public slots:
	void sceneListSelectedNode(Node* object);

private:
	void clearLayout(QLayout* layout, bool deleteWidgets = true);

private:

	/// <summary>
	/// The widget which will hold the layout
	/// </summary>
	QWidget* layoutHolder_;

	/// <summary>
	/// The layout for the widgets
	/// </summary>
	QVBoxLayout* propertyHolder_;
	
	/// <summary>
	/// A scroll area which is used to allow the layout
	/// to expand past the size of the dock widget and
	/// provide a scroll bar to move around the layout
	/// </summary>
	QScrollArea* scrollArea_;

	/// <summary>
	/// A map storing all the property widgets
	/// </summary>
	QMap<QString, QWidget*> widgets_;
};

#endif // PROPERTYWINDOW_H
