#ifndef RENDERCONTEXT_H
#define RENDERCONTEXT_H

#include <QtGui>
#include <irrlicht.h>
#include "node.h"
#include "customnode.h"

using namespace irr;
using namespace scene;

struct SIrrlichtKey
{
	irr::EKEY_CODE code;
	wchar_t ch;
};

/// <summary>
/// Inherit class only which provides all the override
/// functions to enable irrlicht. It also holds the static
/// irrlicht device which is used for rendering
/// </summary>
class RenderContext
{
public:
	RenderContext() {}
	virtual ~RenderContext() {}

	virtual void initaliseContext() {}

	virtual void addIrrlichtNode(Node* node) {}

	virtual void updateScene() {}

	virtual void moveViewCamera() {}

	/// <summary>
	/// Gets the render device.
	/// </summary>
	/// <returns>The static irrlicht device</returns>
	static IrrlichtDevice* getRenderDevice() { return renderDevice_; }

protected:
	
	virtual void castRay() {}
	virtual Node* findNodeFromIrrlichtNode(ISceneNode* node) { return NULL; }

	virtual void sendKeyEventToIrrlicht(QKeyEvent* event, bool pressedDown) {}
	virtual void sendMouseEventToIrrlicht(QMouseEvent* event, bool pressedDown) {}

	virtual void renderScene() {}
	
protected:
	
	static IrrlichtDevice* renderDevice_;
	
	/// <summary>
	/// The display driver type
	/// </summary>
	irr::video::E_DRIVER_TYPE driverType_;
	
	/// <summary>
	/// The camera used for the display
	/// </summary>
	scene::ICameraSceneNode* camera_;
};

#endif // RENDERCONTEXT_H
