#include "propertywindow.h"

PropertyWindow::PropertyWindow(QWidget *parent, Qt::WindowFlags flags) : QDockWidget(parent, flags)
{
	//Set the dock widgets name
	this->setWindowTitle(tr("PropertyWindow"));
	
	//Create the scroll area
	scrollArea_ = new QScrollArea(this);
	//prevent it from resizeing the widgets (does not seem to effect it though)
	scrollArea_->setWidgetResizable(false);
	//Create the layout holder widget which will be a child of the scroll area
	layoutHolder_ = new QWidget(scrollArea_);
	//Create the layout which is a child of the layout holder widget
	propertyHolder_ = new QVBoxLayout(layoutHolder_);
	//Set the layout properties
	propertyHolder_->setSpacing(0);
	propertyHolder_->setMargin(0);
	propertyHolder_->setContentsMargins(0,0,0,0);
	//Doesnt seem to want to work
	propertyHolder_->setSizeConstraint(QLayout::SetFixedSize);
	//Set the layout of the layout holder
	layoutHolder_->setLayout(propertyHolder_);
	//Set the widget of the scroll area to the layout holder
	scrollArea_->setWidget(layoutHolder_);
	//Add the scroll area to this widget
	this->setWidget(scrollArea_);
	//set the min size for this widget to 210
	this->setMinimumWidth(210);
}

PropertyWindow::~PropertyWindow(void)
{
	if (propertyHolder_)
	{
		delete propertyHolder_;
		propertyHolder_ = NULL;
	}
	if (layoutHolder_)
	{
		delete layoutHolder_;
		layoutHolder_ = NULL;
	}
	if (scrollArea_)
	{
		delete scrollArea_;
		scrollArea_ = NULL;
	}
}

/// <summary>
/// Scene selected node.
/// triggers when a node in the scene list is selected
/// </summary>
/// <param name="object">The object.</param>
void PropertyWindow::sceneListSelectedNode(Node* object)
{
	//Clear the layout of all widgets
	if (propertyHolder_)
	{
		clearLayout(propertyHolder_);
	}
	
	//The property widget
	QWidget* propertyWidget; 

	if(object->getName() != "BaseNode")
	{
		//The current node as a custom node (which it should be all the time unless base node
		//in which case it has no properties
		CustomNode* node = (CustomNode*)object;
	
		//Check if the node has properties
		if(node->hasProperties())
		{
			//get the node properties
			NodePropertyMap properties = node->getProperties();

			//iterate through all the properties
			for (NodePropertyMap::iterator it = properties.begin(); it != properties.end(); it++)
			{
				//Check the property type id
				switch(it->second.property->getTypeId())
				{
				case BOOL_PROP:
					//Create the correct property widget for each property type
					propertyWidget = new PropertyBoolWidget(it->second.property);
					break;
				case INT_PROP:
					propertyWidget = new PropertyNumericWidget(it->second.property);
					break;
				case FLOAT_PROP:
					propertyWidget = new PropertyNumericWidget(it->second.property);
					break;
				case STRING_PROP:
					propertyWidget = new PropertyStringWidget(it->second.property);
					break;
				case VEC2_PROP:
					propertyWidget = new PropertyVec2Widget(it->second.property);
					break;
				case VEC3_PROP:
					propertyWidget = new PropertyVec3Widget(it->second.property);
					break;
				case COLOUR_PROP:
					propertyWidget = new PropertyColourWidget(it->second.property);
					break;
				}
				//Set the fixed hight for all widgets
				//REMOVE THIS IS THE WIDGET IS GETTING SQUASHED
				propertyWidget->setFixedHeight(65);
				//add the widget and align it to the top
				propertyHolder_->addWidget(propertyWidget, 0, Qt::AlignTop);
			}
		}
	}


	
}


/// <summary>
/// Clear the layout.
/// </summary>
/// <param name="layout">The layout.</param>
/// <param name="deleteWidgets">The delete widgets.</param>
void PropertyWindow::clearLayout(QLayout* layout, bool deleteWidgets/* = true*/)
{
	//get all the widgets from the layout
	while (QLayoutItem* item = layout->takeAt(0))
	{
		//if the widget should be deleted delete it
		if (deleteWidgets)
		{
			if (QWidget* widget = item->widget())
				delete widget;
		}
		//clear the child widgets
		if (QLayout* childLayout = item->layout())
			clearLayout(childLayout, deleteWidgets);
		delete item;
	}
}