#include "scenelist.h"

SceneList::~SceneList(void)
{

}

/// <summary>
/// Constructs the tree.
/// This function takes a scene and will build a
/// tree structure out of it current scene and will
/// also link each tree item to a node.
/// Since this rebuilds the whole tree open tree structures
/// will be closed (this is slightly annoying and should be
/// build to add any new nodes allowing open trees to be saved)
/// </summary>
/// <param name="scene">The scene.</param>
void SceneList::constructTree(Scene* scene)
{
	//Store the current scene
	scene_ = scene;

	//If the scene is valid then a tree can be created
	if(scene_)
	{
		//Get the base node from the scene
		Node* baseNode = scene_->getBaseNode();
		//create a base widget item for the base node
		QTreeWidgetItem* base = new QTreeWidgetItem();
		//Set the text to the node name
		base->setText(0, QString::fromStdString(baseNode->getName()));
		//Insert the node into the tree
		sceneList_.insert(base, baseNode);

		//if the base node has children they will need to be added
		//they also need to be added to the base node tree widget item
		if(baseNode->getChildCount() != 0)
			addChildren(base, baseNode);
		
		//Set a label to say name at the top of the tree
		QStringList labels;
		labels << tr("Name");
		this->insertTopLevelItems(0, sceneList_.keys());
		this->header()->setResizeMode(QHeaderView::Stretch);
		this->setHeaderLabels(labels);
	}
}

/// <summary>
/// Gets the tree structure.
/// </summary>
/// <returns>The scene list</returns>
NodeTree* SceneList::getTreeStructure()
{
	return &sceneList_;
}

/// <summary>
/// Gets the selected node.
/// </summary>
/// <returns>The current selected Node</returns>
Node* SceneList::getSelectedNode()
{
	return selectedNode_;
}

/// <summary>
/// Key press event for QWidget.
/// This should be used to delete a node but hand not
/// been implemented yet
/// !NOT IMPLEMENTED!
/// </summary>
/// <param name="event">The event.</param>
void SceneList::keyPressEvent(QKeyEvent* event)
{

}

/// <summary>
/// Item changed slot.
/// !NOT IMPLEMENTED!
/// </summary>
/// <param name="item">The item.</param>
/// <param name="column">The column.</param>
void SceneList::itemChanged(QTreeWidgetItem *item, int column)
{

}


/// <summary>
/// Clicked item slot.
/// Used for when an item in the tree is selected
/// </summary>
/// <param name="item">The item.</param>
/// <param name="column">The column.</param>
void SceneList::clickedItem(QTreeWidgetItem *item, int column)
{
	//Create an iterator for the scene list to find the widget item
	NodeTree::Iterator it = sceneList_.find(item);
	//If the iterator has not reached the end the item was found
	if(it != sceneList_.end())
	{
		//Set the selected node
		selectedNode_ = it.value();
		//emit the selected node signal so other widgets can know when
		//a new node has been selected in the tree
		emit sceneListSelectedNode(it.value());
	}
}


/// <summary>
/// Updates the scene structure.
/// This slot is used to tell the scene list that
/// a node has been added to the scene or any changes
/// have been made to the scene so the tree list can
/// be rebuild to accommodate for the changes
/// </summary>
/// <param name="scene">The scene.</param>
void SceneList::updateScene(Scene* scene)
{
	//clear the tree
	this->clear();
	//Clear the scene list (Widgets items may need to be deleted)
	//Qt may not clean them up when clear is called
	sceneList_.clear();
	//Construct the new scene tree
	constructTree(scene);
}

/// <summary>
/// Add children to the scene tree.
/// </summary>
/// <param name="ParentItem">The parent tree widget item.</param>
/// <param name="parent">The parent node.</param>
void SceneList::addChildren(QTreeWidgetItem* ParentItem, Node* parent)
{
	//loop through all the children
	for (int i = 0; i < parent->getChildCount(); i++)
	{
		//create a widget item for the child
		QTreeWidgetItem* Child = new QTreeWidgetItem(ParentItem);
		//Set the name of the widget item
		Child->setText(0, QString::fromStdString(parent->getChildren()[i]->getName()));
		//add the item to the list
		sceneList_.insert(Child, parent->getChildren()[i]);
		//check if that node has any children and loop through them
		if (parent->getChildren()[i]->getChildCount() != 0)
		{
			//add that child to the scene tree list
			addChildren(Child, parent->getChildren()[i]);
		}
	}
}

