#ifndef PROPERTY_H
#define PROPERTY_H

#include "node.h"

/// <summary>
/// enumeration for base property types
/// </summary>
enum propertyType
{
	INT_PROP = 1,
	FLOAT_PROP = 2,
	BOOL_PROP = 3,
	STRING_PROP = 4,
	VEC2_PROP = 5,
	VEC3_PROP = 6,
	VEC4_PROP = 7,
	MAT2_PROP = 8,
	MAT3_PROP = 9,
	MAT4_PROP = 10,
	COLOUR_PROP = 11
};

/// <summary>
/// The propertyID class is a base type to be derived by Property
/// its job is to provide base id information about the property so
/// it can be converted back to its correct derived class. It also
/// allows for a generic class type for all properties allowing for
/// properties storing floating point data to be stored in the same array
/// as properties storing string data
/// </summary>
class PropertyID
{
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="PropertyID"/> class.
	/// Requires a set property ID which can be used to convert the property back to the
	/// correct derived class. The unique identifier is also created at this time
	/// </summary>
	/// <param name="typeId">The type identifier.</param>
	PropertyID(propertyType typeId) : typeId_(typeId), name_("property") { uUid_ = QUuid::createUuid(); }
	PropertyID(int customTypeId)  : typeId_(customTypeId), name_("property") { uUid_ = QUuid::createUuid(); }
	virtual ~PropertyID(void) {}


	/// <summary>
	/// Sets the name of the property
	/// </summary>
	/// <param name="name">The name.</param>
	void setName(std::string name) { name_ = name; }

	/// <summary>
	/// by default this is a derived property and will return null.
	/// however if the derived class overrides this function
	/// propertyID will have access to the property value
	/// </summary>
	/// <returns>NULL</returns>
	virtual void* getProperty() { return NULL; }

	/// <summary>
	/// Gets the name.
	/// </summary>
	/// <returns></returns>
	std::string getName()	{ return name_; }
	/// <summary>
	/// Gets the type identifier.
	/// </summary>
	/// <returns></returns>
	int getTypeId()			{ return typeId_; }

	/// <summary>
	/// Gets the UUID.
	/// </summary>
	/// <returns></returns>
	QUuid getUuid()			{ return uUid_; }	

private:
	
	/// <summary>
	/// The name of the property
	/// </summary>
	std::string name_;
	
	/// <summary>
	/// The property Type
	/// </summary>
	int typeId_;
	
	/// <summary>
	/// The unique identifier for the property
	/// </summary>
	QUuid uUid_;
};

/// <summary>
/// The property callback function
/// </summary>
typedef void (*propertyCallBack)(Node*, PropertyID*);

/// <summary>
/// The property class is a template class which inherits from
/// propertyID to allow for any property type to be stored by to have
/// a generic base for properties.
/// To use this class simply create a property with the a linked node, the typeID
/// to the typename (specified by the user) and the value.
/// This property can now be called via the propertyID gerProperty function
/// </summary>
template <typename T>
class Property : public PropertyID
{
public:
	Property(Node* linkedNode, propertyType typeId, const T value, bool enableCallBack = false, propertyCallBack changeEvent = NULL) 
		: linkedNode_(linkedNode), PropertyID(typeId), propertyValue_(value), enableCallBack_(enableCallBack), changeCallBack_(changeEvent) 
		{ if(changeCallBack_ && linkedNode_) changeCallBack_(linkedNode_, this); }
	
	Property(Node* linkedNode, int customTypeId, const T value, bool enableCallBack = false, propertyCallBack changeEvent = NULL) 
		:  linkedNode_(linkedNode), PropertyID(customTypeId), propertyValue_(value), enableCallBack_(enableCallBack), changeCallBack_(changeEvent) 
		{ if(changeCallBack_ && linkedNode_ && enableCallBack_) changeCallBack_(linkedNode_, this); }
	
	virtual ~Property(void) {}

	/// <summary>
	/// Enables the call back.
	/// </summary>
	inline void enableCallBack()	{ enableCallBack_ = true; }
	/// <summary>
	/// Disables the call back.
	/// </summary>
	inline void disableCallBack()	{ enableCallBack_ = false; }

	/// <summary>
	/// Sets the call back.
	/// </summary>
	/// <param name="callback">The callback.</param>
	inline void setCallBack(propertyCallBack callback) { changeCallBack_ = callback; }
	/// <summary>
	/// Removes the call back.
	/// </summary>
	inline void removeCallBack() { changeCallBack_ = NULL; }

	/// <summary>
	/// Triggers the call back.
	/// </summary>
	inline void triggerCallBack() { if(changeCallBack_ && linkedNode_ && enableCallBack_) changeCallBack_(linkedNode_, this); }

	/// <summary>
	/// Gets the property as a void pointer.
	/// </summary>
	/// <returns>void pointer to property value</returns>
	inline void* getProperty() { return (void*)&propertyValue_; }
	/// <summary>
	/// Gets the property as its template type.
	/// </summary>
	/// <returns>property value</returns>
	inline T getPropertyasType() { return propertyValue_; }
	/// <summary>
	/// Sets the property.
	/// </summary>
	/// <param name="value">The value.</param>
	inline void setProperty(const T value) { propertyValue_ = value;  if(changeCallBack_ && linkedNode_ && enableCallBack_) changeCallBack_(linkedNode_, this); }
	/// <summary>
	/// Sets the property no call.
	/// </summary>
	/// <param name="value">The value.</param>
	inline void setPropertyNoCall(const T value) { propertyValue_ = value; }

private:

	/// <summary>
	/// The linked node to the property
	/// </summary>
	Node* linkedNode_;
	
	/// <summary>
	/// The property value
	/// </summary>
	T propertyValue_;

	/// <summary>
	/// The property callback function
	/// </summary>
	propertyCallBack changeCallBack_;

	/// <summary>
	/// if the callback is enabled
	/// </summary>
	bool enableCallBack_;
};



#endif PROPERTY_H