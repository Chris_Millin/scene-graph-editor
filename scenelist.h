#ifndef SCENELIST_H
#define SCENELIST_H

#include <QtGui>

#include "customnode.h"
#include "scene.h"

typedef QHash<QTreeWidgetItem*, Node*> NodeTree;

/// <summary>
/// This is a modified Qtree widget which
/// handles the management of the nodes
/// in the tree widget
/// </summary>
class SceneList : public QTreeWidget
{
	Q_OBJECT

public:
	SceneList(QWidget* Parent = 0) : QTreeWidget(Parent) { selectedNode_ = NULL; }
	virtual ~SceneList(void);
	
	void constructTree(Scene* scene);

	NodeTree* getTreeStructure();
	Node* getSelectedNode();

protected:

	virtual void keyPressEvent(QKeyEvent* event);

signals:
	void sceneListSelectedNode(Node* node);

public slots:

	void itemChanged(QTreeWidgetItem *item, int column);
	void clickedItem(QTreeWidgetItem *item, int column);
	void updateScene(Scene* scene);

private:

	void addChildren(QTreeWidgetItem* ParentItem, Node* parent);
	//QTreeWidgetItem* createTreeItem(Node* nodeItem, QTreeWidgetItem* parent = NULL);

private:

	
	/// <summary>
	/// The currnet scene
	/// </summary>
	Scene* scene_;
	
	/// <summary>
	/// The scene list tree widget array
	/// </summary>
	NodeTree sceneList_;
	
	/// <summary>
	/// The current selected node
	/// </summary>
	Node* selectedNode_;
};

#endif // SCENELIST_H
