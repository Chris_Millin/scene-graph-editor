#include "scene.h"

Scene::Scene()
{
}

Scene::~Scene(void)
{

}

/// <summary>
/// Initalises the scene which created the base node.
/// </summary>
void Scene::initaliseScene()
{
	//consturct a new base node which has a null parent
	baseNode_ = new Node("BaseNode", NULL);
	//add the node to the complete node list
	NodeList_.push_back(baseNode_);
	//set the selected node to null
	currentSelectedNode_ = NULL;
}

/// <summary>
/// Determines whether node is the base node.
/// </summary>
/// <param name="checkNode">The check node.</param>
/// <returns>if the node is the base node</returns>
bool Scene::isNodeBase(Node* checkNode)
{
	return (baseNode_ == checkNode);
}


/// <summary>
/// Adds a custom node.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="propertyList">The complete property list build from the nodebuilder.</param>
void Scene::addCustomNode(CustomNode* node, std::vector<CompleteProperty> propertyList)
{
	//Sort the completed properties into there irrlicht node counter partner
	std::unordered_map<std::string,std::vector<CompleteProperty>> sortedList;	
	for(int i = 0; i < propertyList.size(); i++)
	{
		sortedList[propertyList[i].irrName].push_back(propertyList[i]);
	}

	
	//Search through the map and find all the irrlicht nodes
	//and build the properties and irrlicht node for it
	std::unordered_map<std::string, std::vector<CompleteProperty>>::iterator it;
	for (it = sortedList.begin(); it != sortedList.end(); it++)
	{
		if(it->second[0].irrtype == IRR_MESH_NODE)
		{
			addMeshNode(node, it->second);
		}
		else if(it->second[0].irrtype == IRR_TERRAIN_NODE)
		{
			addTerrainNode(node, it->second);
		}
		else if(it->second[0].irrtype == IRR_ANI_MESH_NODE)
		{
			addAniMeshNode(node, it->second);
		}

		addProperties(node, it->second);
	}

	//add the node to the selected node as a child
	//if there is no selected node then the base node is used
	if(currentSelectedNode_ != NULL && node->getParent() == NULL)
	{
		node->setParent(currentSelectedNode_);
		currentSelectedNode_->addChild(node);
		NodeList_.push_back(node);
	}
	else if(currentSelectedNode_ == NULL && node->getParent() == NULL)
	{
		node->setParent(baseNode_);
		baseNode_->addChild(node);
		NodeList_.push_back(node);
	}

	//emit that the scene will need updating
	emit updateScene(this);
}

/// <summary>
/// Gets the base node.
/// </summary>
/// <returns>returns the base Node object</returns>
Node* Scene::getBaseNode()
{
	return baseNode_;
}

/// <summary>
/// Find a completed property via its name.
/// this is used to find key properties used to build the irrlicht node object
/// </summary>
/// <param name="properties">The properties.</param>
/// <param name="name">The name.</param>
/// <returns>The completed property</returns>
CompleteProperty Scene::findName(std::vector<CompleteProperty> properties, std::string name)
{
	//loop through all the properties
	for (int i = 0; i < properties.size(); i++)
	{
		//if the name is the same then return the completed property
		if (properties[i].prop->getName() == name)
		{
			return properties[i];
		}
	}
	//if not return a null completed property
	CompleteProperty c;
	c.prop = NULL;
	return c;
}


/// <summary>
/// Adds the properties from a completed property array to a node.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="properties">The properties.</param>
void Scene::addProperties(CustomNode* node, std::vector<CompleteProperty> properties)
{
	//loop through all the properties
	std::vector<CompleteProperty>::iterator it = properties.begin();
	for (it; it != properties.end(); it++)
	{
		//add the property to the node
		node->addProperty(it->prop, it->irrName);
		//attach the callback to the property
		NodeCallBacks::attachCallback(it->prop, it->propCallBackInfo);
	}
}


/// <summary>
/// Add a irrlicht mesh node.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="properties">The properties.</param>
void Scene::addMeshNode(CustomNode* node, std::vector<CompleteProperty> properties)
{
	//create a new mesh node
	IMeshSceneNode* irrNode;
	//find the mesh property which is used to build a mesh node
	CompleteProperty mesh = findName(properties, "Mesh");
	//get the scene manager to help build the irrlicht node
	ISceneManager* sceneManager_ = RenderContext::getRenderDevice()->getSceneManager();
	if(mesh.prop != NULL)
	{
		//create the mesh node with the set mesh property value
		irrNode = sceneManager_->addMeshSceneNode(sceneManager_->getMesh((*(std::string*)mesh.prop->getProperty()).c_str()));
		//add the irrlicht node to the node
		node->addNode(properties[0].irrName, irrNode, properties[0].irrtype);
	}
	else
	{
		//if no mesh could be found a cube mesh is used instead
		irrNode = sceneManager_->addCubeSceneNode();
		//The irrlicht node is added to the node
		node->addNode(properties[0].irrName, irrNode, properties[0].irrtype);
	}
}

/// <summary>
/// Add a irrlicht terrain node.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="properties">The properties.</param>
void Scene::addTerrainNode(CustomNode* node, std::vector<CompleteProperty> properties)
{
	//The irrlicht node
	ITerrainSceneNode* irrTerrainNode;
	//find the hight map used to build the terrain
	CompleteProperty terrain = findName(properties, "Height Map");
	//get the scene manager used to build the node
	ISceneManager* sceneManager_ = RenderContext::getRenderDevice()->getSceneManager();
	//if there is a terrain hight map load it in
	if(terrain.prop != NULL)
	{
		//by default the scale will be set to a default value but this can be overriden if the scale property is added
		irrTerrainNode = sceneManager_->addTerrainSceneNode((*(std::string*)terrain.prop->getProperty()).c_str(),
			0,                  // parent node
			-1,                 // node id
			core::vector3df(0.f, 0.f, 0.f),     // position
			core::vector3df(0.f, 0.f, 0.f),     // rotation
			core::vector3df(40.f, 4.4f, 40.f),  // scale
			video::SColor ( 255, 255, 255, 255 ),   // vertexColor
			5,                  // maxLOD
			scene::ETPS_17,     // patchSize
			4                   // smoothFactor
			);
		//add the irrlicht node to the node
		node->addNode(properties[0].irrName, irrTerrainNode, properties[0].irrtype);
	}
	else
	{
		//if the terrain hightmap could not be found then a default one will be applied
		irrTerrainNode = sceneManager_->addTerrainSceneNode("terrain/terrain-heightmap.bmp",
			0,                  // parent node
			-1,                 // node id
			core::vector3df(0.f, 0.f, 0.f),     // position
			core::vector3df(0.f, 0.f, 0.f),     // rotation
			core::vector3df(40.f, 4.4f, 40.f),  // scale
			video::SColor ( 255, 255, 255, 255 ),   // vertexColor
			5,                  // maxLOD
			scene::ETPS_17,             // patchSize
			4                   // smoothFactor
			);

		//a texture is also applied to the default one but can be overriden by the texture property
		IVideoDriver* driver = RenderContext::getRenderDevice()->getVideoDriver();

		irrTerrainNode->setMaterialFlag(video::EMF_LIGHTING, false);
		irrTerrainNode->setMaterialTexture(0, driver->getTexture("terrain/terrain-texture.jpg"));
		//add the irrlicht node to the node
		node->addNode(properties[0].irrName, irrTerrainNode, properties[0].irrtype);
	}
}

/// <summary>
/// Adds a irrlicht animation mesh node.
/// NO DEFUALT MESH IS AVALIABLE FOR THIS NODE AT THE MOMENT
/// </summary>
/// <param name="node">The node.</param>
/// <param name="properties">The properties.</param>
void Scene::addAniMeshNode(CustomNode* node, std::vector<CompleteProperty> properties)
{
	//the irrlicht node
	IAnimatedMeshSceneNode* irrNode;
	//find the mesh property
	CompleteProperty mesh = findName(properties, "Mesh");
	//obtain the scene manager
	ISceneManager* sceneManager_ = RenderContext::getRenderDevice()->getSceneManager();
	if(mesh.prop != NULL)
	{
		//build the animation mesh scene node from the mesh property
		irrNode = sceneManager_->addAnimatedMeshSceneNode(sceneManager_->getMesh((*(std::string*)mesh.prop->getProperty()).c_str()));
		//add the irrlicht node to the node
		node->addNode(properties[0].irrName, irrNode, properties[0].irrtype);
	}
}