#include "toolbar.h"

ToolBar::~ToolBar(void)
{

}

/// <summary>
/// Initalises the tool bar.
/// </summary>
void ToolBar::initaliseToolBar()
{
	//Adds the file menu to the tool bar
	initaliseFileMenu();
	//Adds the create menu to the tool bar
	initaliseCreateMenu();
	//Adds the help menu to the tool bar
	initaliseHelpMenu();
}

/// <summary>
/// Initalises the file menu.
/// </summary>
void ToolBar::initaliseFileMenu()
{
	//Create the menu object
	fileMenu_ = new QMenu(tr("&File"), this);

	//Create the new action which will create a new scene
	newSceneAction_ = new QAction(tr("New"), this);
	//This can also be linked up with QT default hot keys for new (ctrl+n)
	newSceneAction_->setShortcut(QKeySequence::New);
	//link the signal when it is triggered to the new scene slot
	connect(newSceneAction_, SIGNAL(triggered()), this, SLOT(newScene()));
	//Add the action to the file menu
	fileMenu_->addAction(newSceneAction_);

	//Create the open action which will open a scene file
	openSceneAction_ = new QAction(tr("Open"), this);
	//Set the QT shortcut
	openSceneAction_->setShortcut(QKeySequence::Open);
	//Link the trigger signal with the open scene function slot
	connect(openSceneAction_, SIGNAL(triggered()), this, SLOT(openScene()));
	//add the open scene action to the file menu
	fileMenu_->addAction(openSceneAction_);

	//Create the save action which should save the current scene it its current loaded
	//file location
	saveSceneAction_ = new QAction(tr("Save"), this);
	//Set the QT shortcut
	saveSceneAction_->setShortcut(QKeySequence::Save);
	//Liink the trigger signal
	connect(saveSceneAction_, SIGNAL(triggered()), this, SLOT(saveScene()));
	//Add the action to the file menu
	fileMenu_->addAction(saveSceneAction_);

	//Create Save As action which will save the scene through the file dialog if the
	//user intends to rename or change the file location of the scene save file
	saveAsSceneAction_ = new QAction(tr("Save As"), this);
	//Set the QT shortcut
	saveAsSceneAction_->setShortcut(QKeySequence::SaveAs);
	//Connect the trigger signal
	connect(saveAsSceneAction_, SIGNAL(triggered()), this, SLOT(saveAsScene()));
	//Add to the file menu
	fileMenu_->addAction(saveAsSceneAction_);

	//Quit action which will close the application
	exitAction_ = new QAction(tr("Quit"), this);
	//QT shortcut
	exitAction_->setShortcut(QKeySequence::Quit);
	//This signal does not work as intended and should be linked to the close function for the
	//main window
	connect(exitAction_, SIGNAL(triggered()), this, SLOT(close()));
	//add the action to the file menu
	fileMenu_->addAction(exitAction_);

	//add the file menu to the window main menu object
	windowMenu_->addMenu(fileMenu_);
}

/// <summary>
/// Initalises the create menu.
/// </summary>
void ToolBar::initaliseCreateMenu()
{
	//build the create menu object
	createMenu_ = new QMenu(tr("&Create"), this);

	//build the build node action which will launch the node builder UI window
	customNodeAction_ = new QAction(tr("Build Node"), this);
	//connect the trigger signal to the construct node function
	connect(customNodeAction_, SIGNAL(triggered()), this, SLOT(constructNode()));
	//add the action to the create menu item
	createMenu_->addAction(customNodeAction_);

	//Add the create menu to the main menu object
	windowMenu_->addMenu(createMenu_);
}

/// <summary>
/// Initalises the help menu.
/// </summary>
void ToolBar::initaliseHelpMenu()
{
	//Construct the help menu item
	helpMenu_ = new QMenu(tr("&Help"), this);

	//create the help menu action
	helpAction_ = new QAction(tr("Help"), this);
	helpAction_->setShortcut(QKeySequence::HelpContents);
	connect(helpAction_, SIGNAL(triggered()), this, SLOT(help()));
	helpMenu_->addAction(helpAction_);

	//create the about menu action
	aboutAction_ = new QAction(tr("About"), this);
	connect(aboutAction_, SIGNAL(triggered()), this, SLOT(about()));
	helpMenu_->addAction(aboutAction_);

	//add the help menu to the main menu
	windowMenu_->addMenu(helpMenu_);
}


/// <summary>
/// new scene (will emit a signal for a new scene).
/// </summary>
void ToolBar::newScene()
{
	printf("Gen New Scene\n");
	//emits the build scene call back (this should be linked to the main menu
	//create scene
	emit buildNewScene();
}

/// <summary>
/// Opens a scene.
/// </summary>
void ToolBar::openScene()
{
	printf("Open Scene\n");
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open Scene"), "", tr("Scene Structure (*.xml)"));
	newScene();
	//Creates a new scene IO object
	sceneIO_ = new SceneIO();
	connect(sceneIO_, SIGNAL(addCustomNode(CustomNode*, std::vector<CompleteProperty>)), scene_, SLOT(addCustomNode(CustomNode*, std::vector<CompleteProperty>)));
	//Save the scene to the file location
	sceneIO_->loadScene(fileName, scene_);
	//delete the sceneIO object
	delete sceneIO_;
	sceneIO_ = NULL;
}

/// <summary>
/// Saves as scene.
/// !NOT IMPLEMENTED!
/// </summary>
void ToolBar::saveScene()
{
	printf("Save Scene\n");
}

/// <summary>
/// Saves the scene using the file dialog.
/// </summary>
void ToolBar::saveAsScene()
{
	printf("Save As Scene\n");
	//Create a file dialog with the .xml parameter and gets the location of the selected file
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Scene"), "", tr("Scene Structure (*.xml)"));
	//Creates a new scene IO object
	sceneIO_ = new SceneIO();
	//Save the scene to the file location
	sceneIO_->saveScene(fileName, scene_);
	//delete the sceneIO object
	delete sceneIO_;
	sceneIO_ = NULL;
}

/// <summary>
/// Constructs the node which will open the node builder.
/// </summary>
void ToolBar::constructNode()
{
	printf("Construct Node Scene\n");

	//emit the open node builder signal
	emit openNodeBuilder();
}

/// <summary>
/// Helps this instance.
/// !NOT IMPLEMENTED!
/// </summary>
void ToolBar::help()
{
	printf("Help\n");
}

/// <summary>
/// Abouts this instance.
/// !NOT IMPLEMENTED!
/// </summary>
void ToolBar::about()
{
	printf("About\n");
}