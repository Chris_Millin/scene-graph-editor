#include "scenewindow.h"

SceneWindow::SceneWindow(QWidget *parent/* = 0*/,  Qt::WindowFlags flags /*= 0 */) : QDockWidget(parent, flags)
{
}

/// <summary>
/// Initalises the scene tree.
/// </summary>
/// <param name="scene">The scene.</param>
void SceneWindow::initaliseSceneTree(Scene* scene)
{
	printf("Initalising Scene tree");
	//set the title of the dock widget
	this->setWindowTitle(tr("Scene Window"));
	//create a new scene tree widget
	sceneList_ = new SceneList();
	//construct the tree
	sceneList_->constructTree(scene);
	//Link the scene's update scene signal to the update of the scene tree widget so it knows
	//when the scene is updated
	connect(scene, SIGNAL(updateScene(Scene*)), sceneList_, SLOT(updateScene(Scene*)));
	//link the clicked item of the QTreeWidget to the custom slot in the SceneList tree widget
	connect(sceneList_, SIGNAL(itemClicked(QTreeWidgetItem*,int)), sceneList_, SLOT(clickedItem(QTreeWidgetItem*, int)));
	//link the selectednode signal to the scene and this dock widget to they know when a node is selected
	connect(sceneList_, SIGNAL(sceneListSelectedNode(Node*)), scene, SLOT(selectedNode(Node*)));
	connect(sceneList_, SIGNAL(sceneListSelectedNode(Node*)), this, SLOT(listSelectedNode(Node*)));

	//add the tree widget
	this->setWidget(sceneList_);

	//show the dock
	this->show();
}

/// <summary>
/// Gets the scene list.
/// </summary>
/// <returns>The scene list widget</returns>
SceneList* SceneWindow::getSceneList()
{
	return sceneList_;
}

/// <summary>
/// causes the signal from the selected node in the tree to be
/// available from this widget through the selectedNode slot.
/// </summary>
/// <param name="node">The node.</param>
void SceneWindow::listSelectedNode(Node* node)
{
	emit selectedNode(node);
}