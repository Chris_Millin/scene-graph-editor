#ifndef NODE_H
#define NODE_H

#include <QUuid>
#include <map>
#include <vector>
#include <string>
#include <irrlicht.h>


/// <summary>
/// Node types which was going to be implemented
/// with fixed node types but the design changed
/// to custom node only and this has remaind in still
/// </summary>
enum NodeType
{
	BASE_NODE = 0,
	MESH_NODE = 1,
	ANI_MESH_NODE = 2,
	CAMERA_NODE = 3,
	SOUND_NODE = 4,
	TERRAIN_NODE = 5,
	PORTAL_NODE = 6,
	ROOM_NODE = 7,
	CUSTOM_NODE = 8
};

/// <summary>
/// The node contains the core features the a node should contain
/// This should not be used no its now and instead custom node should be use
/// which adds more functionality to the node with help of irrlicht
/// </summary>
class Node
{
public:
    Node(std::string name, Node* parentNode) : parentNode(parentNode), nodeName_(name) { uUid_ = QUuid::createUuid(); nodeType = BASE_NODE; }
	virtual ~Node(void);
	
	virtual void addChild(Node* child);
	
	virtual void removeChild(Node* child);
	

	/// <summary>
	/// Renames the specified name.
	/// </summary>
	/// <param name="name">The new name.</param>
	void rename(std::string name)		{ nodeName_ = name; }

	/// <summary>
	/// Sets the parent of the node.
	/// </summary>
	/// <param name="parnet">The parnet node.</param>
	void setParent(Node* parnet)		{ parentNode = parnet; }

	/// <summary>
	/// Gets the parent node.
	/// </summary>
	/// <returns>parent node</returns>
	Node* getParent()					{ return parentNode; }
	/// <summary>
	/// Gets the children nodes.
	/// </summary>
	/// <returns>vector list of children nodes</returns>
	std::vector<Node*> getChildren()	{ return childrenNodes; }
	/// <summary>
	/// Gets the child count.
	/// </summary>
	/// <returns>The count of children</returns>
	int getChildCount()					{ return childrenNodes.size(); }
	/// <summary>
	/// Gets the unique identifier.
	/// </summary>
	/// <returns>Qt unique identifier</returns>
	QUuid getUuID()						{ return uUid_; }
	/// <summary>
	/// Gets the child node.
	/// </summary>
	/// <param name="Id">The identifier of the child node.</param>
	/// <returns>The child node</returns>
	Node* getChild(QUuid Id);
	
	/// <summary>
	/// Gets the name of the node.
	/// </summary>
	/// <returns>Node name</returns>
	std::string getName()				{ return nodeName_; }

	/// <summary>
	/// == Operator which can be used on dereference nodes to check if they are the
	/// same with the Uuid. *node1 == *node2
	/// </summary>
	/// <param name="rhsNode">The RHS node.</param>
	/// <returns></returns>
	bool operator==(Node& rhsNode)		{ return this->getUuID() == rhsNode.getUuID(); }

protected:

	/// <summary>
	/// The parent node object for the current node
	/// </summary>
	Node* parentNode;

	/// <summary>
	/// The children nodes for the current node
	/// </summary>
	std::vector<Node*> childrenNodes;

	/// <summary>
	/// The node type
	/// This was going to be used in conjunction with fixed type nodes
	/// but the design has changed to be custom node only so this does not
	/// provide much functionality.
	/// </summary>
	NodeType nodeType;
private:
	/// <summary>
	/// The name of the node
	/// </summary>
	std::string nodeName_;

	/// <summary>
	/// The unique identifier for the node
	/// </summary>
	QUuid uUid_;
};

#endif // NODE_H
