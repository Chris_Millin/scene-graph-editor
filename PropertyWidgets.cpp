#include "PropertyWidgets.h"


PropertyBoolWidget::~PropertyBoolWidget(void)
{
	if (layout_)
	{
		delete layout_;
		layout_ = NULL;
	}
	if (textLabel_)
	{
		delete textLabel_;
		textLabel_ = NULL;
	}
	if (boolCheckBox_)
	{
		delete boolCheckBox_;
		boolCheckBox_ = NULL;
	}
}

/// <summary>
/// Initalises the widget with the property.
/// </summary>
void PropertyBoolWidget::initalise()
{
	//if a valid property was set then set up
	//the widget to display it
	if(property_)
	{
		//Create a layout to hold the widgets
		layout_ = new QVBoxLayout(this);
		//label to display the property name
		textLabel_ = new QLabel(QString::fromStdString(property_->getName()), this);
		//Create the check box
		boolCheckBox_ = new QCheckBox(this);
		//obtain the current property value (if it is a bool property)
		if (property_->getTypeId() == BOOL_PROP)
		{
			//This shows that propertyID can still obtain the derived class
			//property value as a void pointer
			boolCheckBox_->setChecked(*(bool*)property_->getProperty());
		}
		//connect the checkbox state change to the checked function
		connect(boolCheckBox_, SIGNAL(stateChanged(int)), this, SLOT(checked(int)));
		//add the widgets to the layout
		layout_->addWidget(textLabel_);
		layout_->addWidget(boolCheckBox_);
		//set the layout for the widget
		this->setLayout(layout_);
	}	
}

/// <summary>
/// Slot for the state change function for the check box.
/// </summary>
/// <param name="checkValue">The check value.</param>
void PropertyBoolWidget::checked(int checkValue)
{
	//check to ensure the property is a boolean value
	if (property_->getTypeId() == BOOL_PROP)
	{
		//The propertyID is downcasted using dynamic_cast this time as
		//the propertyID does not provide a set function only the derived
		//class does
		Property<bool>* propertyData = dynamic_cast<Property<bool>*>(property_);
		//set the new property value (this will trigger the callback if set)
		propertyData->setProperty(checkValue);
	}
}


PropertyNumericWidget::~PropertyNumericWidget(void)
{
	if (layout_)
	{
		delete layout_;
		layout_ = NULL;
	}
	if (textLabel_)
	{
		delete textLabel_;
		textLabel_ = NULL;
	}
	if (inputLine_)
	{
		delete inputLine_;
		inputLine_ = NULL;
	}
}

/// <summary>
/// Initalises this instance.
/// </summary>
void PropertyNumericWidget::initalise()
{
	//Check the property is valid
	if(property_)
	{
		//Create a new layout
		layout_ = new QVBoxLayout(this);
		//Create the name label
		textLabel_ = new QLabel(QString::fromStdString(property_->getName()), this);
		//Create the text box
		inputLine_ = new QLineEdit(this);
		//Since the numerical widget can work with int or floats
		//the will each need to be obtained in different ways
		if (property_->getTypeId() == INT_PROP)
		{
			//set the text to the value stored in the property
			inputLine_->setText(QString::number(*(int*)property_->getProperty()));
			//Apply a validator to the text box to only allow integer values
			inputLine_->setValidator(new QIntValidator(-99999, 99999, inputLine_));
		}
		if (property_->getTypeId() == FLOAT_PROP)
		{
			//same as the int but for floats
			inputLine_->setText(QString::number(*(float*)property_->getProperty()));
			//Uses a double validator which allows for point values to be input
			inputLine_->setValidator(new QDoubleValidator(-99999.0, 99999.0, 5, inputLine_));
		}
		
		//Connect the edit finished signal to the line complete function
		connect(inputLine_, SIGNAL(editingFinished()), this, SLOT(lineComplete()));
		//Add the widgets
		layout_->addWidget(textLabel_);
		layout_->addWidget(inputLine_);
		//Set the layout
		this->setLayout(layout_);
	}	
}

/// <summary>
/// Slot for when the LineEdit widget finished.
/// </summary>
void PropertyNumericWidget::lineComplete()
{
	//Check the property type
	if (property_->getTypeId() == INT_PROP)
	{
		//Get the property as its derived class
		Property<int>* propertyData = dynamic_cast<Property<int>*>(property_);
		//Set the property
		propertyData->setProperty(inputLine_->text().toInt());
	}
	if (property_->getTypeId() == FLOAT_PROP)
	{
		Property<float>* propertyData = dynamic_cast<Property<float>*>(property_);
		propertyData->setProperty(inputLine_->text().toFloat());
	}
}

PropertyStringWidget::~PropertyStringWidget(void)
{
	if (layout_)
	{
		delete layout_;
		layout_ = NULL;
	}
	if (textLabel_)
	{
		delete textLabel_;
		textLabel_ = NULL;
	}
	if (inputLine_)
	{
		delete inputLine_;
		inputLine_ = NULL;
	}
}


/// <summary>
/// Initalises the widget.
/// </summary>
void PropertyStringWidget::initalise()
{
	//This widget is build similar to the numeric widget but removes the
	//validators since they are not needed for string values
	if(property_)
	{
		layout_ = new QVBoxLayout(this);
		textLabel_ = new QLabel(QString::fromStdString(property_->getName()), this);
		inputLine_ = new QLineEdit(this);
		if (property_->getTypeId() == STRING_PROP)
		{
			inputLine_->setText(QString::fromStdString(*(std::string*)property_->getProperty()));
		}
		connect(inputLine_, SIGNAL(editingFinished()), this, SLOT(lineComplete()));
		layout_->addWidget(textLabel_);
		layout_->addWidget(inputLine_);
		this->setLayout(layout_);
	}
}

/// <summary>
/// Slot for when the LineEdit widget finished.
/// </summary>
void PropertyStringWidget::lineComplete()
{
	if (property_->getTypeId() == STRING_PROP)
	{
		Property<std::string>* propertyData = dynamic_cast<Property<std::string>*>(property_);
		propertyData->setProperty(inputLine_->text().toStdString());
	}
}

PropertyVec2Widget::~PropertyVec2Widget(void)
{
	if (layout_)
	{
		delete layout_;
		layout_ = NULL;
	}
	if (textLabel_)
	{
		delete textLabel_;
		textLabel_ = NULL;
	}
	if (xTextLabel_)
	{
		delete xTextLabel_;
		xTextLabel_ = NULL;
	}
	if (xInputLine_)
	{
		delete xInputLine_;
		xInputLine_ = NULL;
	}
	if (yTextLabel_)
	{
		delete yTextLabel_;
		yTextLabel_ = NULL;
	}
	if (yInputLine_)
	{
		delete yInputLine_;
		yInputLine_ = NULL;
	}
}


/// <summary>
/// Initalises the widget.
/// </summary>
void PropertyVec2Widget::initalise()
{
	//The follows similar design to the numerical widget
	//however it includes two lineedit widgets for the
	//X and Y input
	if(property_)
	{
		layout_ = new QGridLayout(this);
		textLabel_ = new QLabel(QString::fromStdString(property_->getName()), this);
		xTextLabel_ = new QLabel(tr("X: "));
		xInputLine_ = new QLineEdit(this);
		xInputLine_->setValidator(new QDoubleValidator(-99999.0, 99999.0, 5, xInputLine_));
		yTextLabel_ = new QLabel(tr("Y: "));
		yInputLine_ = new QLineEdit(this);
		yInputLine_->setValidator(new QDoubleValidator(-99999.0, 99999.0, 5, yInputLine_));
		if (property_->getTypeId() == VEC2_PROP)
		{
			//Obtain the X and Y value 
			xInputLine_->setText(QString::number((*(vector2df*)property_->getProperty()).X));
			yInputLine_->setText(QString::number((*(vector2df*)property_->getProperty()).Y));
		}
		connect(xInputLine_, SIGNAL(editingFinished()), this, SLOT(xLineComplete()));
		connect(yInputLine_, SIGNAL(editingFinished()), this, SLOT(yLineComplete()));
		//Unlike numerical widget the Vec2 contains a grid layout
		//to allow for the x and y line editor to be inline with each other
		//in the horizontal but also allow for the name to be placed vertically
		//about the x and y line editors
		layout_->addWidget(textLabel_, 0, 0);
		layout_->addWidget(xTextLabel_, 1, 0);
		layout_->addWidget(xInputLine_, 1, 1);
		layout_->addWidget(yTextLabel_, 1, 2);
		layout_->addWidget(yInputLine_, 1, 3);
		this->setLayout(layout_);
	}
}

/// <summary>
/// line editor complete for x component
/// </summary>
void PropertyVec2Widget::xLineComplete()
{
	if (property_->getTypeId() == VEC2_PROP)
	{
		Property<vector2df>* propertyData = dynamic_cast<Property<vector2df>*>(property_);
		propertyData->setProperty(vector2df(xInputLine_->text().toFloat(), (*(vector2df*)propertyData->getProperty()).Y));
	}
}

/// <summary>
/// line editor complete for y component
/// </summary>
void PropertyVec2Widget::yLineComplete()
{
	if (property_->getTypeId() == VEC2_PROP)
	{
		Property<vector2df>* propertyData = dynamic_cast<Property<vector2df>*>(property_);
		propertyData->setProperty(vector2df((*(vector2df*)propertyData->getProperty()).X, yInputLine_->text().toFloat()));
	}
}

PropertyVec3Widget::~PropertyVec3Widget(void)
{
	if (layout_)
	{
		delete layout_;
		layout_ = NULL;
	}
	if (textLabel_)
	{
		delete textLabel_;
		textLabel_ = NULL;
	}
	if (xTextLabel_)
	{
		delete xTextLabel_;
		xTextLabel_ = NULL;
	}
	if (xInputLine_)
	{
		delete xInputLine_;
		xInputLine_ = NULL;
	}
	if (yTextLabel_)
	{
		delete yTextLabel_;
		yTextLabel_ = NULL;
	}
	if (yInputLine_)
	{
		delete yInputLine_;
		yInputLine_ = NULL;
	}
	if (zTextLabel_)
	{
		delete zTextLabel_;
		zTextLabel_ = NULL;
	}
	if (zInputLine_)
	{
		delete zInputLine_;
		zInputLine_ = NULL;
	}
}

/// <summary>
/// Initalises the widget.
/// </summary>
void PropertyVec3Widget::initalise()
{
	//follows the same design as vec2 but has an
	//extra component Z
	if(property_)
	{
		layout_ = new QGridLayout(this);
		layout_->setSpacing(0);
		layout_->setMargin(0);
		layout_->setContentsMargins(0,0,0,0);
		textLabel_ = new QLabel(QString::fromStdString(property_->getName()), this);
		xTextLabel_ = new QLabel(tr(" X: "));
		xInputLine_ = new QLineEdit(this);
		xInputLine_->setValidator(new QDoubleValidator(-99999.0, 99999.0, 5, xInputLine_));
		yTextLabel_ = new QLabel(tr(" Y: "));
		yInputLine_ = new QLineEdit(this);
		yInputLine_->setValidator(new QDoubleValidator(-99999.0, 99999.0, 5, yInputLine_));
		zTextLabel_ = new QLabel(tr(" Z: "));
		zInputLine_ = new QLineEdit(this);
		zInputLine_->setValidator(new QDoubleValidator(-99999.0, 99999.0, 5, yInputLine_));
		if (property_->getTypeId() == VEC3_PROP)
		{
			xInputLine_->setText(QString::number((*(vector3df*)property_->getProperty()).X));
			yInputLine_->setText(QString::number((*(vector3df*)property_->getProperty()).Y));
			zInputLine_->setText(QString::number((*(vector3df*)property_->getProperty()).Z));
		}
		connect(xInputLine_, SIGNAL(editingFinished()), this, SLOT(xLineComplete()));
		connect(yInputLine_, SIGNAL(editingFinished()), this, SLOT(yLineComplete()));
		connect(zInputLine_, SIGNAL(editingFinished()), this, SLOT(zLineComplete()));
		layout_->addWidget(textLabel_, 0, 0, Qt::AlignLeft);
		layout_->addWidget(xTextLabel_, 1, 0, Qt::AlignLeft);
		layout_->addWidget(xInputLine_, 1, 1, Qt::AlignLeft);
		layout_->addWidget(yTextLabel_, 1, 2, Qt::AlignLeft);
		layout_->addWidget(yInputLine_, 1, 3, Qt::AlignLeft);
		layout_->addWidget(zTextLabel_, 1, 4, Qt::AlignLeft);
		layout_->addWidget(zInputLine_, 1, 5, Qt::AlignLeft);
		this->setLayout(layout_);
	}
}

/// <summary>
/// line editor complete for x component
/// </summary>
void PropertyVec3Widget::xLineComplete()
{
	if (property_->getTypeId() == VEC3_PROP)
	{
		Property<vector3df>* propertyData = dynamic_cast<Property<vector3df>*>(property_);
		propertyData->setProperty(vector3df(xInputLine_->text().toFloat(), (*(vector3df*)propertyData->getProperty()).Y, (*(vector3df*)propertyData->getProperty()).Z));
	}
}

/// <summary>
/// line editor complete for y component
/// </summary>
void PropertyVec3Widget::yLineComplete()
{
	if (property_->getTypeId() == VEC3_PROP)
	{
		Property<vector3df>* propertyData = dynamic_cast<Property<vector3df>*>(property_);
		propertyData->setProperty(vector3df((*(vector3df*)propertyData->getProperty()).X, yInputLine_->text().toFloat(), (*(vector3df*)propertyData->getProperty()).Z));
	}
}

/// <summary>
/// line editor complete for z component
/// </summary>
void PropertyVec3Widget::zLineComplete()
{
	if (property_->getTypeId() == VEC3_PROP)
	{
		Property<vector3df>* propertyData = dynamic_cast<Property<vector3df>*>(property_);
		propertyData->setProperty(vector3df((*(vector3df*)propertyData->getProperty()).X, (*(vector3df*)propertyData->getProperty()).Y, zInputLine_->text().toFloat()));
	}
}

PropertyVec4Widget::~PropertyVec4Widget(void)
{

}

void PropertyVec4Widget::initalise(QString text)
{

}

void PropertyVec4Widget::xLineComplete()
{

}

void PropertyVec4Widget::yLineComplete()
{

}

void PropertyVec4Widget::zLineComplete()
{

}

void PropertyVec4Widget::wLineComplete()
{

}

PropertyColourWidget::~PropertyColourWidget(void)
{
	if (layout_)
	{
		delete layout_;
		layout_ = NULL;
	}
	if (textLabel_)
	{
		delete textLabel_;
		textLabel_ = NULL;
	}
	if (colourImageLabel_)
	{
		delete colourImageLabel_;
		colourImageLabel_ = NULL;
	}
	if (changeColour_)
	{
		delete changeColour_;
		changeColour_ = NULL;
	}
	if (colourWindow_)
	{
		delete colourWindow_;
		colourWindow_ = NULL;
	}
}

/// <summary>
/// Initalises the specified text.
/// </summary>
void PropertyColourWidget::initalise()
{
	if(property_)
	{
		//Create the layout
		layout_ = new QGridLayout(this);
		//Set the property name
		textLabel_ = new QLabel(QString::fromStdString(property_->getName()), this);
		//Create the colour dialog
		colourWindow_ = new QColorDialog(this);
		//Create the colour change button
		changeColour_ = new QPushButton(tr("Change Colour"), this);
		//Create the label which will store the colour image
		colourImageLabel_ = new QLabel(this);
		//Create a colour pixel map size 25px by 25px
		colour_ = QPixmap(25,25);
		//If the property is a valid colour property load the value
		if (property_->getTypeId() == COLOUR_PROP)
		{
			//get the irrlicht colour
			SColor value = *(SColor*)property_->getProperty();
			//Send the colour too the current colour dialog
			colourWindow_->setCurrentColor(QColor(value.getRed(), value.getGreen(), value.getBlue(), value.getAlpha()));
		}
		//fill the pixel map with the current colour
		colour_.fill(colourWindow_->currentColor());
		//Set the label image
		colourImageLabel_->setPixmap(colour_);
		//Link both the select colour from the colour dialog window and the click event of the button
		//to the slot functions
		connect(colourWindow_, SIGNAL(colorSelected(QColor)), this, SLOT(selectedColour(QColor)));
		connect(changeColour_, SIGNAL(clicked()), this, SLOT(changeColour()));
		//Add the widgets to the layout
		layout_->addWidget(textLabel_, 0, 0);
		layout_->addWidget(colourImageLabel_, 1, 0);
		layout_->addWidget(changeColour_, 1, 1);
		//set the layout
		this->setLayout(layout_);
	}
}

/// <summary>
/// The colour from the colour dialog window.
/// </summary>
/// <param name="color">The color.</param>
void PropertyColourWidget::selectedColour(const QColor& color)
{
	if (property_->getTypeId() == COLOUR_PROP)
	{
		Property<SColor>* propertyData = dynamic_cast<Property<SColor>*>(property_);
		//set the colour to the irrlicht colour property
		propertyData->setProperty(SColor(color.alpha(), color.red(), color.green(), color.blue()));
		//refill the pixel map with the new colour
		colour_.fill(color);
		//set the image label to contain the new pixel map
		colourImageLabel_->setPixmap(colour_);
	}
}

/// <summary>
/// Changes the colour.
/// </summary>
void PropertyColourWidget::changeColour()
{
	//show the colour dialog window
	colourWindow_->show();
}


PropertyImageWidget::~PropertyImageWidget()
{

}

void PropertyImageWidget::initalise(QString text)
{

}

void PropertyImageWidget::selectedFile(const QString& file)
{

}

void PropertyImageWidget::changeImage()
{

}

PropertyMat2Widget::~PropertyMat2Widget(void)
{

}

void PropertyMat2Widget::initalise(QString text)
{

}

void PropertyMat2Widget::lineComplete(int col, int row)
{

}

PropertyMat3Widget::~PropertyMat3Widget(void)
{

}

void PropertyMat3Widget::initalise(QString text)
{

}

void PropertyMat3Widget::lineComplete(int col, int row)
{

}

PropertyMat4Widget::~PropertyMat4Widget(void)
{

}

void PropertyMat4Widget::initalise(QString text)
{

}

void PropertyMat4Widget::lineComplete(int col, int row)
{

}





