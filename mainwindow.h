#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "toolbar.h"
#include "editorwindow.h"
#include "propertywindow.h"
#include "scenewindow.h"



/// <summary>
/// The main window object which will store all the
/// core widgets for the UI
/// </summary>
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    virtual ~MainWindow();

	void initaliseWindow();

private:

	void initaliseToolBar();
	void initaliseWidgetWindows();

public slots:
	
	void newScene();
	void openNodeBuilder();
	void closeNodeBuilder();

private:

	//Tool bar which displays the menu at the top of the window
	ToolBar* windowToolBar_;

	//The editor window which will contain the irrlicht rendering target
	EditorWindow*	editorWidgetWindow_;
	//The property window which allows for selected nodes properties to be displayed
	PropertyWindow* propertyWidgetWindow_;
	//The scene window which will display the current objects in the scene within a hierarchy
	SceneWindow*	sceneWidgetWindow_;	

	//The current active scene 
	Scene* activeScene_;

	//The node builder UI dialog which will be triggered from the 
	//tool bar widget
	NodeBuilder* builder_;
};

#endif // MAINWINDOW_H
