#include "erroroutput.h"

/// <summary>
/// The log file
/// </summary>
FILE* ErrorOutput::logFile_;

/// <summary>
/// Initalises the error log.
/// !NOT IMPLEMENTED!
/// </summary>
/// <param name="newLog">If a new log should be created.</param>
void ErrorOutput::initaliseErrorLog(bool newLog /* = false */)
{

}

/// <summary>
/// Clears the log.
/// !NOT IMPLEMENTED!
/// </summary>
void ErrorOutput::clearLog()
{

}

/// <summary>
/// Writes the error.
/// !NOT IMPLEMENTED!
/// </summary>
/// <param name="message">The error message.</param>
void ErrorOutput::WriteError(std::string message)
{

}

/// <summary>
/// Checks the file exists.
/// !NOT IMPLEMENTED!
/// </summary>
/// <returns>if the file exist in the file system</returns>
bool ErrorOutput::checkFileExists()
{
	return false;
}

/// <summary>
/// Creates the log.
/// !NOT IMPLEMENTED!
/// </summary>
void ErrorOutput::createLog()
{

}

/// <summary>
/// Loads the log.
/// !NOT IMPLEMENTED!
/// </summary>
void ErrorOutput::loadLog()
{

}