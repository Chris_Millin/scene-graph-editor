#include "PropertyCallBacks.h"


//Static definitions for all the maps of callback information stored in each callback class
std::unordered_map<std::string, CallBackInfo> MeshNodeCallBacks::callbackArray_;
std::unordered_map<std::string, CallBackInfo> AniMeshNodeCallBacks::callbackArray_;
std::unordered_map<std::string, CallBackInfo> CameraNodeCallBacks::callbackArray_;
std::unordered_map<std::string, CallBackInfo> TerrainNodeCallBacks::callbackArray_;
std::unordered_map<std::string, CallBackInfo> SoundNodeCallBacks::callbackArray_;
std::unordered_map<std::string, CallBackInfo> BillBoardNodeCallBacks::callbackArray_;
std::unordered_map<std::string, CallBackInfo> BillBoardTextNodeCallBacks::callbackArray_;
std::unordered_map<std::string, CallBackInfo> LightNodeCallBacks::callbackArray_;
std::unordered_map<std::string, CallBackInfo> ParticleEmitterCallBack::callbackArray_;
std::unordered_map<std::string, CallBackInfo> ParticleAniMeshNodeCallBacks::callbackArray_;
std::unordered_map<std::string, CallBackInfo> TextNodeCallBacks::callbackArray_;
std::unordered_map<std::string, CallBackInfo> VolumeLightNodeCallBacks::callbackArray_;


#pragma region Node Call Backs
/// <summary>
/// Gets the callback list for a specific irrlicht node.
/// </summary>
/// <param name="type">Irrlicht node type.</param>
/// <returns>The callback map for the node</returns>
std::unordered_map<std::string, CallBackInfo> NodeCallBacks::getCallbackList(IrrNodeType type)
{
	//Check which irrlicht node has been sent to the function and return the appropriate
	//function callback list
	switch(type)
	{
	case IRR_MESH_NODE:
		return MeshNodeCallBacks::getCallBackArray();
		break;
	case IRR_ANI_MESH_NODE:
		return AniMeshNodeCallBacks::getCallBackArray();
		break;
	case IRR_CAMERA_NODE:
		return CameraNodeCallBacks::getCallBackArray();
		break;
	case IRR_TERRAIN_NODE:
		return TerrainNodeCallBacks::getCallBackArray();
		break;
	case IRR_SOUND_NODE:
		return SoundNodeCallBacks::getCallBackArray();
		break;
	case IRR_BILLBOARD_NODE:
		return BillBoardNodeCallBacks::getCallBackArray();
		break;
	case IRR_BILLBOARD_TEXT_NODE:
		return BillBoardTextNodeCallBacks::getCallBackArray();
		break;
	case IRR_LIGHT_NODE:
		return LightNodeCallBacks::getCallBackArray();
		break;
	case IRR_PARTICLE_EMITTER:
		return ParticleEmitterCallBack::getCallBackArray();
		break;
	case IRR_PARTICLE_ANI_MESH_NODE:
		return ParticleAniMeshNodeCallBacks::getCallBackArray();
		break;
	case IRR_TEXT_NODE:
		return TextNodeCallBacks::getCallBackArray();
		break;
	case IRR_VOLUME_LIGHT_NODE:
		return VolumeLightNodeCallBacks::getCallBackArray();
		break;
	}
}

/// <summary>
/// Helper function for building a callback structure.
/// </summary>
/// <param name="node">The node type for the callback.</param>
/// <param name="callback">The callback function location.</param>
/// <param name="propertytype">The propertytype that is allowed with the function.</param>
/// <returns>The callback structure</returns>
CallBackInfo NodeCallBacks::buildCallBack(IrrNodeType node, propertyCallBack callback, int propertytype)
{
	//Create a new callback structure
	CallBackInfo info;
	//set the node type
	info.LinkedNode = node;
	//Set the  property type
	info.PropertyType = propertytype;
	//Set the callback
	info.Callback = callback;
	//return the callback structure
	return info;
}

/// <summary>
/// Attaches the callback to a property.
/// </summary>
/// <param name="prop">The property to attack the callback.</param>
/// <param name="callback">The callback information.</param>
void NodeCallBacks::attachCallback(PropertyID* prop, CallBackInfo callback)
{

	if (callback.PropertyType == BOOL_PROP)
	{
		//convert the property back to its derived class
		Property<bool>* propertyDatab = dynamic_cast<Property<bool>*>(prop);
		//enable callback to run
		propertyDatab->enableCallBack();
		//link the callback function
		propertyDatab->setCallBack(callback.Callback);
		//trigger the callback to ensure the data is set
		propertyDatab->triggerCallBack();
	}
	else if (callback.PropertyType == INT_PROP)
	{
		Property<int>* propertyDatai = dynamic_cast<Property<int>*>(prop);
		propertyDatai->enableCallBack();
		propertyDatai->setCallBack(callback.Callback);
		propertyDatai->triggerCallBack();
	}
	else if (callback.PropertyType == FLOAT_PROP)
	{
		Property<float>* propertyDataf = dynamic_cast<Property<float>*>(prop);
		propertyDataf->enableCallBack();
		propertyDataf->setCallBack(callback.Callback);
		propertyDataf->triggerCallBack();
	}
	else if (callback.PropertyType == STRING_PROP)
	{
		Property<std::string>* propertyDatas = dynamic_cast<Property<std::string>*>(prop);
		propertyDatas->enableCallBack();
		propertyDatas->setCallBack(callback.Callback);
		propertyDatas->triggerCallBack();
	}
	else if (callback.PropertyType == VEC2_PROP)
	{
		Property<vector2df>* propertyDatav2 = dynamic_cast<Property<vector2df>*>(prop);
		propertyDatav2->enableCallBack();
		propertyDatav2->setCallBack(callback.Callback);
		propertyDatav2->triggerCallBack();
	}
	else if (callback.PropertyType == VEC3_PROP)
	{
		Property<vector3df>* propertyDatav3 = dynamic_cast<Property<vector3df>*>(prop);
		propertyDatav3->enableCallBack();
		propertyDatav3->setCallBack(callback.Callback);
		propertyDatav3->triggerCallBack();
	}
	else if (callback.PropertyType == COLOUR_PROP)
	{
		Property<SColor>* propertyDatac = dynamic_cast<Property<SColor>*>(prop);
		propertyDatac->enableCallBack();
		propertyDatac->setCallBack(callback.Callback);
		propertyDatac->triggerCallBack();
	}
	
}
#pragma endregion


#pragma region Scene Node Callbacks
/// <summary>
/// Names the call back.
/// This function would have set the name for the irrlicht node. However this is not
/// needed since the names are given at a higher level any way. This function could
/// be implemented if required
/// </summary>
/// <param name="node">The node object the property is linked to.</param>
/// <param name="property">The property to be updated.</param>
void SceneNodeCallbacks::nameCallBack(Node* node,PropertyID* property)
{

}

/// <summary>
/// Positions the call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void SceneNodeCallbacks::positionCallBack(Node* node, PropertyID* property)
{
	//Check the type to ensure that is is valid for the callback
	if (property->getTypeId() == VEC3_PROP)
	{
		//get the irrlicht node from the node object (since custom node is the only type
		//with base node and only the base node can be of node type it can be assumed it
		//will be of custom node type.)
		ISceneNode* irrNode = (ISceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		//If the irrlicht node could not be found then dont try to update it
		if(irrNode != NULL)
		{
			//get the property data of the derived class
			Property<vector3df>* propertyData = dynamic_cast<Property<vector3df>*>(property);
			//since this is position check the change amount to update all the children with
			vector3df change = propertyData->getPropertyasType() - irrNode->getPosition();
			//update all the children positions
			((CustomNode*)node)->setChildrenPosition(change);
			//set the position of the irrlicht node
			irrNode->setPosition(propertyData->getPropertyasType());
		}
	}
}

/// <summary>
/// Rotations the call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void SceneNodeCallbacks::rotationCallBack(Node* node,PropertyID* property)
{
	//follows similar functionality to the position call back but with the rotation value
	if (property->getTypeId() == VEC3_PROP)
	{
		ISceneNode* irrNode = (ISceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<vector3df>* propertyData = dynamic_cast<Property<vector3df>*>(property);
			vector3df change = propertyData->getPropertyasType() - irrNode->getRotation();
			((CustomNode*)node)->setChildrenRotation(change);
			irrNode->setRotation(propertyData->getPropertyasType());
		}
	}
}

/// <summary>
/// Scales the call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void SceneNodeCallbacks::scaleCallBack(Node* node,PropertyID* property)
{
	//follows similar functionality to the position callback but with the scale value
	if (property->getTypeId() == VEC3_PROP)
	{
		ISceneNode* irrNode = (ISceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<vector3df>* propertyData = dynamic_cast<Property<vector3df>*>(property);
			vector3df change = propertyData->getPropertyasType() - irrNode->getScale();
			((CustomNode*)node)->setChildrenScale(change);
			irrNode->setScale(propertyData->getPropertyasType());
		}

	}
}

/// <summary>
/// Visibles the call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void SceneNodeCallbacks::visibleCallBack(Node* node,PropertyID* property)
{
	//check to see if the value is the right type 
	if (property->getTypeId() == BOOL_PROP)
	{
		//get the irrlicht node
		ISceneNode* irrNode = (ISceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			//convert the propertyID to its derived class
			Property<bool>* propertyData = dynamic_cast<Property<bool>*>(property);
			//set the visiable element of the node
			irrNode->setVisible(propertyData->getPropertyasType());
		}
	}
}

/// <summary>
/// Materials the texture call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void MeshNodeCallBacks::materialTextureCallBack(Node* node, PropertyID* property)
{
	//same as most callbacks
	if (property->getTypeId() == STRING_PROP)
	{
		ISceneNode* irrNode = (ISceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<std::string>* propertyData = dynamic_cast<Property<std::string>*>(property);
			//get the irrlicht video driver since the texture will need to be changed which requires
			//loading a new texture
			IVideoDriver* driver = RenderContext::getRenderDevice()->getVideoDriver();
			//set the material type flag
			irrNode->setMaterialFlag(video::EMF_LIGHTING, false);
			//load and set the material
			irrNode->setMaterialTexture(0 , driver->getTexture(propertyData->getPropertyasType().c_str()));
		}
	}
}

/// <summary>
/// Meshes the call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void MeshNodeCallBacks::meshCallBack(Node* node, PropertyID* property)
{
	//same as most callbacks
	if (property->getTypeId() == STRING_PROP)
	{
		IMeshSceneNode* irrNode = (IMeshSceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<std::string>* propertyData = dynamic_cast<Property<std::string>*>(property);
			//requires the scene manager to load the mesh object
			ISceneManager* driver = RenderContext::getRenderDevice()->getSceneManager();
			//load and set the mesh object
			irrNode->setMesh(driver->getMesh(propertyData->getPropertyasType().c_str()));
		}
	}
}

/// <summary>
/// Gets the call back array.
/// </summary>
/// <returns>The callback map</returns>
std::unordered_map<std::string, CallBackInfo>MeshNodeCallBacks::getCallBackArray()
{
	//build the callback info array
	if(callbackArray_.size() == 0)
	{
		callbackArray_["Name"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, nameCallBack, STRING_PROP);
		callbackArray_["Position"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, positionCallBack, VEC3_PROP);
		callbackArray_["Rotation"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, rotationCallBack, VEC3_PROP);
		callbackArray_["Scale"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, scaleCallBack, VEC3_PROP);
		callbackArray_["Visible"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, visibleCallBack, BOOL_PROP);

		callbackArray_["Texture"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, materialTextureCallBack, STRING_PROP);
		callbackArray_["Mesh"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, meshCallBack, STRING_PROP);

		return callbackArray_;
	}

	return callbackArray_;
}
#pragma endregion


#pragma region Ani Mesh Node Callbacks
/// <summary>
/// Set Material texture call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void AniMeshNodeCallBacks::materialTextureCallBack(Node* node, PropertyID* property)
{
	//same as mesh node meterial callback
	if (property->getTypeId() == STRING_PROP)
	{
		IAnimatedMeshSceneNode* irrNode = (IAnimatedMeshSceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<std::string>* propertyData = dynamic_cast<Property<std::string>*>(property);
			IVideoDriver* driver = RenderContext::getRenderDevice()->getVideoDriver();
			irrNode->setMaterialFlag(video::EMF_LIGHTING, false);
			irrNode->setMaterialTexture(0, driver->getTexture(propertyData->getPropertyasType().c_str()));
		}
	}
}

/// <summary>
/// Set Mesh call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void AniMeshNodeCallBacks::meshCallBack(Node* node, PropertyID* property)
{
	if (property->getTypeId() == STRING_PROP)
	{
		IAnimatedMeshSceneNode* irrNode = (IAnimatedMeshSceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<std::string>* propertyData = dynamic_cast<Property<std::string>*>(property);
			ISceneManager* driver = RenderContext::getRenderDevice()->getSceneManager();
			irrNode->setMesh(driver->getMesh(propertyData->getPropertyasType().c_str()));
		}
	}
}

/// <summary>
/// Set Animation speed call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void AniMeshNodeCallBacks::animationSpeedCallBack(Node* node, PropertyID* property)
{
	if (property->getTypeId() == INT_PROP)
	{
		IAnimatedMeshSceneNode* irrNode = (IAnimatedMeshSceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<int>* propertyData = dynamic_cast<Property<int>*>(property);
			irrNode->setAnimationSpeed(propertyData->getPropertyasType());
		}
	}
}

/// <summary>
/// Set Current frame call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void AniMeshNodeCallBacks::currentFrameCallBack(Node* node, PropertyID* property)
{
	if (property->getTypeId() == INT_PROP)
	{
		IAnimatedMeshSceneNode* irrNode = (IAnimatedMeshSceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<int>* propertyData = dynamic_cast<Property<int>*>(property);
			irrNode->setCurrentFrame(propertyData->getPropertyasType());
		}
	}
}

/// <summary>
/// Set loop animation call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void AniMeshNodeCallBacks::loopAnimationCallBack(Node* node, PropertyID* property)
{
	if (property->getTypeId() == BOOL_PROP)
	{
		IAnimatedMeshSceneNode* irrNode = (IAnimatedMeshSceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<int>* propertyData = dynamic_cast<Property<int>*>(property);
			irrNode->setLoopMode(propertyData->getPropertyasType());
		}
	}
}

/// <summary>
/// Set MD2s animation name call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void AniMeshNodeCallBacks::md2AnimationCallBack(Node* node, PropertyID* property)
{
	if (property->getTypeId() == STRING_PROP)
	{
		IAnimatedMeshSceneNode* irrNode = (IAnimatedMeshSceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<std::string>* propertyData = dynamic_cast<Property<std::string>*>(property);
			irrNode->setMD2Animation(propertyData->getPropertyasType().c_str());
		}
	}
}

/// <summary>
/// Get Start frame call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void AniMeshNodeCallBacks::startFrameCallBack(Node* node, PropertyID* property)
{
	if (property->getTypeId() == INT_PROP)
	{
		IAnimatedMeshSceneNode* irrNode = (IAnimatedMeshSceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<int>* propertyData = dynamic_cast<Property<int>*>(property);
			//Slightly different to other callbacks this one does not trigger the callback when the value is
			//set as this is a getter stype property only
			propertyData->setPropertyNoCall(irrNode->getStartFrame());
		}
	}
}

/// <summary>
/// Get End frame call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void AniMeshNodeCallBacks::endFrameCallBack(Node* node, PropertyID* property)
{
	if (property->getTypeId() == INT_PROP)
	{
		IAnimatedMeshSceneNode* irrNode = (IAnimatedMeshSceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<int>* propertyData = dynamic_cast<Property<int>*>(property);
			propertyData->setPropertyNoCall(irrNode->getEndFrame());
		}
	}
}

/// <summary>
/// Get Frames call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void AniMeshNodeCallBacks::framesCallBack(Node* node, PropertyID* property)
{
	if (property->getTypeId() == INT_PROP)
	{
		IAnimatedMeshSceneNode* irrNode = (IAnimatedMeshSceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<int>* propertyData = dynamic_cast<Property<int>*>(property);
			propertyData->setPropertyNoCall(irrNode->getFrameNr());
		}
	}
}

/// <summary>
/// Gets the call back array.
/// </summary>
/// <returns>The callback map</returns>
std::unordered_map<std::string, CallBackInfo>AniMeshNodeCallBacks::getCallBackArray()
{
	if(callbackArray_.size() == 0)
	{
		callbackArray_["Name"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, nameCallBack, STRING_PROP);
		callbackArray_["Position"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, positionCallBack, VEC3_PROP);
		callbackArray_["Rotation"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, rotationCallBack, VEC3_PROP);
		callbackArray_["Scale"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, scaleCallBack, VEC3_PROP);
		callbackArray_["Visible"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, visibleCallBack, BOOL_PROP);

		callbackArray_["Texture"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, materialTextureCallBack, STRING_PROP);
		callbackArray_["Mesh"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, meshCallBack, STRING_PROP);
		callbackArray_["Speed"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, animationSpeedCallBack, INT_PROP);
		callbackArray_["Frame"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, currentFrameCallBack, INT_PROP);
		callbackArray_["Speed"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, animationSpeedCallBack, INT_PROP);
		callbackArray_["Frame"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, currentFrameCallBack, INT_PROP);
		callbackArray_["Loop"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, loopAnimationCallBack, BOOL_PROP);
		callbackArray_["Animation"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, md2AnimationCallBack, STRING_PROP);
		
		callbackArray_["Start"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, startFrameCallBack, INT_PROP);
		callbackArray_["End"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, endFrameCallBack, INT_PROP);
		callbackArray_["Frames"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, framesCallBack, INT_PROP);

		return callbackArray_;
	}

	return callbackArray_;
}
#pragma endregion


#pragma region Terrain Node Callbacks
/// <summary>
/// Set Hight map call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void TerrainNodeCallBacks::hightMapCallBack(Node* node, PropertyID* property)
{
	if (property->getTypeId() == STRING_PROP)
	{
		ITerrainSceneNode* irrNode = (ITerrainSceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<std::string>* propertyData = dynamic_cast<Property<std::string>*>(property);
			IVideoDriver* driver = RenderContext::getRenderDevice()->getVideoDriver();
			irrNode->setMaterialTexture(0 , driver->getTexture(propertyData->getPropertyasType().c_str()));
		}
	}
}

/// <summary>
/// Set Texture scale call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void TerrainNodeCallBacks::textureScaleCallBack(Node* node, PropertyID* property)
{
	if (property->getTypeId() == VEC2_PROP)
	{
		ITerrainSceneNode* irrNode = (ITerrainSceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<vector2df>* propertyData = dynamic_cast<Property<vector2df>*>(property);
			IVideoDriver* driver = RenderContext::getRenderDevice()->getVideoDriver();
			irrNode->scaleTexture(propertyData->getPropertyasType().X, propertyData->getPropertyasType().Y);
		}
	}
}

/// <summary>
/// Set Material texture call back.
/// </summary>
/// <param name="node">The node.</param>
/// <param name="property">The property.</param>
void TerrainNodeCallBacks::materialTextureCallBack(Node* node, PropertyID* property)
{
	if (property->getTypeId() == STRING_PROP)
	{
		ITerrainSceneNode* irrNode = (ITerrainSceneNode*)((CustomNode*)node)->getNodeFromProperty(property);
		if(irrNode != NULL)
		{
			Property<std::string>* propertyData = dynamic_cast<Property<std::string>*>(property);
			IVideoDriver* driver = RenderContext::getRenderDevice()->getVideoDriver();
			irrNode->setMaterialTexture(0 , driver->getTexture(propertyData->getPropertyasType().c_str()));
		}
	}
}

/// <summary>
/// Gets the call back array.
/// </summary>
/// <returns>The callback map</returns>
std::unordered_map<std::string, CallBackInfo>TerrainNodeCallBacks::getCallBackArray()
{
	if(callbackArray_.size() == 0)
	{
		callbackArray_["Name"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, nameCallBack, STRING_PROP);
		callbackArray_["Position"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, positionCallBack, VEC3_PROP);
		callbackArray_["Rotation"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, rotationCallBack, VEC3_PROP);
		callbackArray_["Scale"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, scaleCallBack, VEC3_PROP);
		callbackArray_["Visible"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, visibleCallBack, BOOL_PROP);

		callbackArray_["Height Map"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, hightMapCallBack, STRING_PROP);
		callbackArray_["Texture Scale"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, textureScaleCallBack, VEC2_PROP);
		callbackArray_["Material"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, materialTextureCallBack, STRING_PROP);

		return callbackArray_;
	}

	return callbackArray_;
}
#pragma endregion


#pragma region Camera Node Callbacks
void CameraNodeCallBacks::aspectRatioCallBack(Node* node, PropertyID* property)
{

}

void CameraNodeCallBacks::farViewCallBack(Node* node, PropertyID* property)
{

}

void CameraNodeCallBacks::fovCallBack(Node* node, PropertyID* property)
{

}

void CameraNodeCallBacks::nearViewCallBack(Node* node, PropertyID* property)
{

}

void CameraNodeCallBacks::projectionCallBack(Node* node, PropertyID* property)
{

}

void CameraNodeCallBacks::targetCallBack(Node* node, PropertyID* property)
{

}

void CameraNodeCallBacks::upVecCallBack(Node* node, PropertyID* property)
{
	
}

std::unordered_map<std::string, CallBackInfo>CameraNodeCallBacks::getCallBackArray()
{
	if(callbackArray_.size() == 0)
	{
		callbackArray_["Name"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, nameCallBack, STRING_PROP);
		callbackArray_["Position"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, positionCallBack, VEC3_PROP);
		callbackArray_["Rotation"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, rotationCallBack, VEC3_PROP);
		callbackArray_["Scale"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, scaleCallBack, VEC3_PROP);
		callbackArray_["Visible"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, visibleCallBack, BOOL_PROP);

		callbackArray_["Aspect Ratio"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, aspectRatioCallBack, FLOAT_PROP);
		callbackArray_["Far View"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, farViewCallBack, FLOAT_PROP);
		callbackArray_["FOV"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, fovCallBack, FLOAT_PROP);
		callbackArray_["Near View"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, nearViewCallBack, FLOAT_PROP);
		callbackArray_["Projection"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, projectionCallBack, MAT4_PROP);
		callbackArray_["Look At"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, targetCallBack, VEC3_PROP);
		callbackArray_["Up"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, upVecCallBack, VEC3_PROP);

		return callbackArray_;
	}

	return callbackArray_;
}
#pragma endregion


#pragma region Sound Node Callbacks
void SoundNodeCallBacks::volumeCallBack(Node* node, PropertyID* property)
{

}

void SoundNodeCallBacks::upVecCallBack(Node* node, PropertyID* property)
{

}

void SoundNodeCallBacks::loopCallBack(Node* node, PropertyID* property)
{

}

void SoundNodeCallBacks::minDistanceCallBack(Node* node, PropertyID* property)
{

}

void SoundNodeCallBacks::maxDistanceCallBack(Node* node, PropertyID* property)
{

}

void SoundNodeCallBacks::speedCallBack(Node* node, PropertyID* property)
{

}

void SoundNodeCallBacks::lengthCallBack(Node* node, PropertyID* property)
{

}

std::unordered_map<std::string, CallBackInfo>SoundNodeCallBacks::getCallBackArray()
{
	if(callbackArray_.size() == 0)
	{
		callbackArray_["Name"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, nameCallBack, STRING_PROP);
		callbackArray_["Position"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, positionCallBack, VEC3_PROP);
		callbackArray_["Rotation"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, rotationCallBack, VEC3_PROP);
		callbackArray_["Scale"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, scaleCallBack, VEC3_PROP);
		callbackArray_["Visible"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, visibleCallBack, BOOL_PROP);

		callbackArray_["Volume"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, volumeCallBack, FLOAT_PROP);
		callbackArray_["Up Vector"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, upVecCallBack, VEC3_PROP);
		callbackArray_["Loop"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, loopCallBack, BOOL_PROP);
		callbackArray_["Min Distance"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, loopCallBack, VEC3_PROP);
		callbackArray_["Max Distance"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, loopCallBack, VEC3_PROP);
		callbackArray_["Speed"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, loopCallBack, FLOAT_PROP);

		callbackArray_["Length"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, loopCallBack, FLOAT_PROP);

		return callbackArray_;
	}

	return callbackArray_;
}
#pragma endregion


#pragma region Billboard Node Callbacks
void BillBoardNodeCallBacks::colourCallBack(Node* node, PropertyID* property)
{

}

void BillBoardNodeCallBacks::sizeCallBack(Node* node, PropertyID* property)
{

}

void BillBoardNodeCallBacks::materialTextureCallBack(Node* node, PropertyID* property)
{

}

std::unordered_map<std::string, CallBackInfo>BillBoardNodeCallBacks::getCallBackArray()
{
	if(callbackArray_.size() == 0)
	{
		callbackArray_["Name"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, nameCallBack, STRING_PROP);
		callbackArray_["Position"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, positionCallBack, VEC3_PROP);
		callbackArray_["Rotation"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, rotationCallBack, VEC3_PROP);
		callbackArray_["Scale"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, scaleCallBack, VEC3_PROP);
		callbackArray_["Visible"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, visibleCallBack, BOOL_PROP);

		callbackArray_["Texture"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, materialTextureCallBack, STRING_PROP);
		callbackArray_["Colour"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, colourCallBack, COLOUR_PROP);
		callbackArray_["Size"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, sizeCallBack, VEC2_PROP);

		return callbackArray_;
	}

	return callbackArray_;
}

void BillBoardTextNodeCallBacks::textCallBack(Node* node, PropertyID* property)
{

}

void BillBoardTextNodeCallBacks::textColourCallBack(Node* node, PropertyID* property)
{

}

std::unordered_map<std::string, CallBackInfo>BillBoardTextNodeCallBacks::getCallBackArray()
{
	if(callbackArray_.size() == 0)
	{
		callbackArray_["Name"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, nameCallBack, STRING_PROP);
		callbackArray_["Position"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, positionCallBack, VEC3_PROP);
		callbackArray_["Rotation"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, rotationCallBack, VEC3_PROP);
		callbackArray_["Scale"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, scaleCallBack, VEC3_PROP);
		callbackArray_["Visible"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, visibleCallBack, BOOL_PROP);

		callbackArray_["Texture"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, materialTextureCallBack, STRING_PROP);
		callbackArray_["Colour"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, colourCallBack, COLOUR_PROP);
		callbackArray_["Size"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, sizeCallBack, VEC2_PROP);

		callbackArray_["Text Colour"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, textColourCallBack, COLOUR_PROP);
		callbackArray_["Text"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, textCallBack, STRING_PROP);
		return callbackArray_;
	}

	return callbackArray_;
}
#pragma endregion


#pragma region Light Node Callbacks
void LightNodeCallBacks::castShadowCallBack(Node* node, PropertyID* property)
{

}

void LightNodeCallBacks::lightTypeCallBack(Node* node, PropertyID* property)
{

}

void LightNodeCallBacks::radiusCallBack(Node* node, PropertyID* property)
{

}

std::unordered_map<std::string, CallBackInfo>LightNodeCallBacks::getCallBackArray()
{
	if(callbackArray_.size() == 0)
	{
		callbackArray_["Name"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, nameCallBack, STRING_PROP);
		callbackArray_["Position"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, positionCallBack, VEC3_PROP);
		callbackArray_["Rotation"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, rotationCallBack, VEC3_PROP);
		callbackArray_["Scale"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, scaleCallBack, VEC3_PROP);
		callbackArray_["Visible"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, visibleCallBack, BOOL_PROP);

		callbackArray_["Shadow"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, castShadowCallBack, BOOL_PROP);
		callbackArray_["Type"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, lightTypeCallBack, INT_PROP);
		callbackArray_["Radius"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, radiusCallBack, FLOAT_PROP);

		return callbackArray_;
	}

	return callbackArray_;
}
#pragma endregion


#pragma region Particle Emitter Callbacks
void ParticleEmitterCallBack::directionCallBack(Node* node, PropertyID* property)
{

}

void ParticleEmitterCallBack::maxAngleCallBack(Node* node, PropertyID* property)
{

}

void ParticleEmitterCallBack::maxLifeCallBack(Node* node, PropertyID* property)
{

}

void ParticleEmitterCallBack::maxPerSecCallBack(Node* node, PropertyID* property)
{

}

void ParticleEmitterCallBack::maxColourCallBack(Node* node, PropertyID* property)
{

}

void ParticleEmitterCallBack::maxStartSizeCallBack(Node* node, PropertyID* property)
{

}

void ParticleEmitterCallBack::minLifeCallBack(Node* node, PropertyID* property)
{

}

void ParticleEmitterCallBack::minPerSecCallBack(Node* node, PropertyID* property)
{

}

void ParticleEmitterCallBack::minColourCallBack(Node* node, PropertyID* property)
{

}

void ParticleEmitterCallBack::minStartSizeCallBack(Node* node, PropertyID* property)
{

}

std::unordered_map<std::string, CallBackInfo>ParticleEmitterCallBack::getCallBackArray()
{
	if(callbackArray_.size() == 0)
	{
		callbackArray_["Direction"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, directionCallBack, VEC3_PROP);
		callbackArray_["Angle"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, maxAngleCallBack, INT_PROP);
		callbackArray_["Max Life"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, maxLifeCallBack, INT_PROP);
		callbackArray_["Max Per Sec"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, maxPerSecCallBack, INT_PROP);
		callbackArray_["Max Colour"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, maxColourCallBack, COLOUR_PROP);
		callbackArray_["Max Size"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, maxStartSizeCallBack, VEC2_PROP);
		callbackArray_["Min Life"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, minLifeCallBack, INT_PROP);
		callbackArray_["Min Per Sec"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, minPerSecCallBack, INT_PROP);
		callbackArray_["Min Colour"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, minColourCallBack, COLOUR_PROP);
		callbackArray_["Min Size"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, minStartSizeCallBack, VEC2_PROP);

		return callbackArray_;
	}

	return callbackArray_;
}
#pragma endregion


#pragma region Particle Ani Mesh Node Callbacks
void ParticleAniMeshNodeCallBacks::meshCallBack(Node* node, PropertyID* property)
{

}

void ParticleAniMeshNodeCallBacks::normalDirectionCallBack(Node* node, PropertyID* property)
{

}

void ParticleAniMeshNodeCallBacks::useNormalDirectionCallBack(Node* node, PropertyID* property)
{

}

std::unordered_map<std::string, CallBackInfo>ParticleAniMeshNodeCallBacks::getCallBackArray()
{
	if(callbackArray_.size() == 0)
	{
		callbackArray_["Name"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, nameCallBack, STRING_PROP);
		callbackArray_["Position"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, positionCallBack, VEC3_PROP);
		callbackArray_["Rotation"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, rotationCallBack, VEC3_PROP);
		callbackArray_["Scale"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, scaleCallBack, VEC3_PROP);
		callbackArray_["Visible"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, visibleCallBack, BOOL_PROP);

		callbackArray_["Normal Direction"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, normalDirectionCallBack, VEC3_PROP);
		callbackArray_["Mesh"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, meshCallBack, STRING_PROP);
		callbackArray_["Use Normal"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, useNormalDirectionCallBack, BOOL_PROP);

		return callbackArray_;
	}

	return callbackArray_;
}
#pragma endregion


#pragma region Text Node Callbacks
void TextNodeCallBacks::textCallBack(Node* node, PropertyID* property)
{

}

void TextNodeCallBacks::textColourCallBack(Node* node, PropertyID* property)
{

}

std::unordered_map<std::string, CallBackInfo>TextNodeCallBacks::getCallBackArray()
{
	if(callbackArray_.size() == 0)
	{
		callbackArray_["Name"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, nameCallBack, STRING_PROP);
		callbackArray_["Position"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, positionCallBack, VEC3_PROP);
		callbackArray_["Rotation"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, rotationCallBack, VEC3_PROP);
		callbackArray_["Scale"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, scaleCallBack, VEC3_PROP);
		callbackArray_["Visible"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, visibleCallBack, BOOL_PROP);

		callbackArray_["Text"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, textCallBack, STRING_PROP);
		callbackArray_["Text Colour"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, textColourCallBack, COLOUR_PROP);

		return callbackArray_;
	}

	return callbackArray_;
}
#pragma endregion


#pragma region Volume Light Node Callbacks
void VolumeLightNodeCallBacks::footColourCallBack(Node* node, PropertyID* property)
{

}

void VolumeLightNodeCallBacks::subDivUCallBack(Node* node, PropertyID* property)
{

}

void VolumeLightNodeCallBacks::subDivVCallBack(Node* node, PropertyID* property)
{

}

void VolumeLightNodeCallBacks::tailColourCallBack(Node* node, PropertyID* property)
{

}


std::unordered_map<std::string, CallBackInfo>VolumeLightNodeCallBacks::getCallBackArray()
{
	if(callbackArray_.size() == 0)
	{
		callbackArray_["Name"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, nameCallBack, STRING_PROP);
		callbackArray_["Position"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, positionCallBack, VEC3_PROP);
		callbackArray_["Rotation"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, rotationCallBack, VEC3_PROP);
		callbackArray_["Scale"]			= NodeCallBacks::buildCallBack(IRR_MESH_NODE, scaleCallBack, VEC3_PROP);
		callbackArray_["Visible"]		= NodeCallBacks::buildCallBack(IRR_MESH_NODE, visibleCallBack, BOOL_PROP);

		callbackArray_["Foot Colour"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, footColourCallBack, COLOUR_PROP);
		callbackArray_["Sub Divide U"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, subDivUCallBack, FLOAT_PROP);
		callbackArray_["Sub Divide V"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, subDivVCallBack, FLOAT_PROP);
		callbackArray_["Tail Colour"]	= NodeCallBacks::buildCallBack(IRR_MESH_NODE, tailColourCallBack, COLOUR_PROP);

		return callbackArray_;
	}

	return callbackArray_;
}
#pragma endregion