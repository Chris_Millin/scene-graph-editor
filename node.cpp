#include "node.h"

Node::~Node(void)
{
}

/// <summary>
/// Adds the child.
/// </summary>
/// <param name="child">The child.</param>
void Node::addChild(Node* child)
{
	//add the child to the list
	childrenNodes.push_back(child);
}

/// <summary>
/// Removes the child.
/// !NOT IMPLEMENTED!
/// </summary>
/// <param name="child">The child.</param>
void Node::removeChild(Node* child)
{

}