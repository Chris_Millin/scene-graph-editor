#include <QtGui>
#include <QWidget>
#include "Property.h"
#pragma once


using namespace irr;
using namespace core;
using namespace video;

/// <summary>
/// Qt custom widget designed to display a bool property
/// </summary>
class PropertyBoolWidget : public QWidget
{
	Q_OBJECT

public:
	PropertyBoolWidget(PropertyID* property, QWidget* parent = 0) : QWidget(parent), property_(property) { initalise(); }
	virtual ~PropertyBoolWidget(void);

	void initalise();

public slots:
	void checked(int checkValue);

private:
	/// <summary>
	/// layout for the widget
	/// </summary>
	QVBoxLayout* layout_;
	
	/// <summary>
	/// the property value used for the widget
	/// </summary>
	PropertyID* property_;
	
	/// <summary>
	/// The name label
	/// </summary>
	QLabel* textLabel_;
	
	/// <summary>
	/// The boolean checkbox
	/// </summary>
	QCheckBox* boolCheckBox_;
};

/// <summary>
/// Qt custom widget designed to display a bool propertyQt custom widget designed to display numerical property values
/// </summary>
class PropertyNumericWidget : public QWidget
{
	Q_OBJECT

public:
	PropertyNumericWidget(PropertyID* property, QWidget* parent = 0) : QWidget(parent), property_(property) { initalise(); }
	virtual ~PropertyNumericWidget(void);

	void initalise();
public slots:
	
	void lineComplete(); 

private:
	
	/// <summary>
	/// The layout
	/// </summary>
	QVBoxLayout* layout_;
	
	/// <summary>
	/// The property for the widget
	/// </summary>
	PropertyID* property_;

	/// <summary>
	/// The name label
	/// </summary>
	QLabel* textLabel_;

	/// <summary>
	/// The text box for the numerical value
	/// </summary>
	QLineEdit* inputLine_;
};

/// <summary>
/// Qt custom widget designed to display a string property
/// </summary>
class PropertyStringWidget : public QWidget
{
	Q_OBJECT

public:
	PropertyStringWidget(PropertyID* property, QWidget* parent = 0) : QWidget(parent), property_(property) { initalise(); }
	virtual ~PropertyStringWidget(void);

	void initalise();
public slots:

	void lineComplete();

private:

	/// <summary>
	/// The layout
	/// </summary>
	QVBoxLayout* layout_;

	/// <summary>
	/// The property for the widget
	/// </summary>
	PropertyID* property_;
	
	/// <summary>
	/// The name label
	/// </summary>
	QLabel* textLabel_;
	
	/// <summary>
	/// The textbox input
	/// </summary>
	QLineEdit* inputLine_;
};

/// <summary>
/// Qt custom widget designed to display a vector2df value property
/// </summary>
class PropertyVec2Widget : public QWidget
{
	Q_OBJECT

public:
	PropertyVec2Widget(PropertyID* property, QWidget* parent = 0) : QWidget(parent), property_(property) { initalise(); }
	virtual ~PropertyVec2Widget(void);

	void initalise();
public slots:

	void xLineComplete();
	void yLineComplete();

private:

	/// <summary>
	/// Gird layout for the widget
	/// </summary>
	QGridLayout* layout_;

	/// <summary>
	/// The property_
	/// </summary>
	PropertyID* property_;
	
	/// <summary>
	/// Label for the property name
	/// </summary>
	QLabel* textLabel_;
	
	/// <summary>
	/// X: label
	/// </summary>
	QLabel* xTextLabel_;
	
	/// <summary>
	/// The X input text box
	/// </summary>
	QLineEdit* xInputLine_;
	
	/// <summary>
	/// Y: label
	/// </summary>
	QLabel* yTextLabel_;
	
	/// <summary>
	/// The Y input text box
	/// </summary>
	QLineEdit* yInputLine_;
};

/// <summary>
/// Qt custom widget designed to display a vector3df value property
/// </summary>
class PropertyVec3Widget : public QWidget
{
	Q_OBJECT
public:
	PropertyVec3Widget(PropertyID* property, QWidget* parent = 0) : QWidget(parent), property_(property) { initalise(); }
	virtual ~PropertyVec3Widget(void);

	void initalise();

	//This can be used to set the widget size but doesn't seem to want to
	//work as intended
	QSize sizeHint() { return QSize(200,65); }

public slots:

	void xLineComplete();
	void yLineComplete();
	void zLineComplete();

private:
	
	/// <summary>
	/// Grid layout for widget
	/// </summary>
	QGridLayout* layout_;
	
	/// <summary>
	/// The property
	/// </summary>
	PropertyID* property_;
	
	/// <summary>
	/// Label for the property name
	/// </summary>
	QLabel* textLabel_;
	
	/// <summary>
	/// X: label
	/// </summary>
	QLabel* xTextLabel_;
	
	/// <summary>
	/// The X input text box
	/// </summary>
	QLineEdit* xInputLine_;
	
	/// <summary>
	/// Y: label
	/// </summary>
	QLabel* yTextLabel_;
	
	/// <summary>
	/// The Y input text box
	/// </summary>
	QLineEdit* yInputLine_;
	
	/// <summary>
	/// Z: label
	/// </summary>
	QLabel* zTextLabel_;
	
	/// <summary>
	/// The z input text box
	/// </summary>
	QLineEdit* zInputLine_;	
};

/// <summary>
/// Qt custom widget designed to display a vector4df value property
/// !NOT IMPLEMENTED!
/// </summary>
class PropertyVec4Widget : public QWidget
{
	Q_OBJECT

public:
	PropertyVec4Widget(PropertyID* property, QWidget* parent = 0) : QWidget(parent), property_(property) {} 
	virtual ~PropertyVec4Widget(void);

	void initalise(QString text);


public slots:

	void xLineComplete();
	void yLineComplete();
	void zLineComplete();
	void wLineComplete();

private:
	PropertyID* property_;
	QLabel* textLabel_;
	QLabel* xTextLabel_;
	QLineEdit* xInputLine_;
	QLabel* yTextLabel_;
	QLineEdit* yInputLine_;
	QLabel* zTextLabel_;
	QLineEdit* zInputLine_;
	QLabel* wTextLabel_;
	QLineEdit* wInputLine_;
};

/// <summary>
/// Qt custom widget designed to display a Colour value property
/// </summary>
class PropertyColourWidget : public QWidget
{
	Q_OBJECT
public:
	PropertyColourWidget(PropertyID* property, QWidget* parent = 0) : QWidget(parent), property_(property) { initalise(); }
	virtual ~PropertyColourWidget(void);

	void initalise();
public slots:
	
	void selectedColour(const QColor& color);
	void changeColour();

private:
	
	/// <summary>
	/// Grid layout for widget
	/// </summary>
	QGridLayout* layout_;
	
	/// <summary>
	/// The property
	/// </summary>
	PropertyID* property_;
	
	/// <summary>
	/// The name label
	/// </summary>
	QLabel* textLabel_;
	
	/// <summary>
	/// label which the colour will be displayed in
	/// </summary>
	QLabel* colourImageLabel_;
	
	/// <summary>
	/// Button to open the colour dialog
	/// </summary>
	QPushButton* changeColour_;
	
	/// <summary>
	/// The colour window dialog
	/// </summary>
	QColorDialog* colourWindow_;
	
	/// <summary>
	/// Pixel map which will build the colour
	/// for the colourImageLabel_
	/// </summary>
	QPixmap colour_;
};

/// <summary>
/// Qt custom widget designed to display a Image value property
/// !NOT IMPLEMENTED!
/// </summary>
class PropertyImageWidget : public QWidget
{
	Q_OBJECT
public:
	PropertyImageWidget(PropertyID* property, QWidget* parent = 0) : QWidget(parent), property_(property) {}
	virtual ~PropertyImageWidget(void);

	void initalise(QString text);
public slots:

	void selectedFile(const QString& file);
	void changeImage();

private:
	PropertyID* property_;
	QLabel* textLabel_;
	QLabel* imageLabel_;
	QPushButton* changeImage_;
	QFileDialog* locationWindow_;
};

/// <summary>
/// Qt custom widget designed to display a matrix 2x2 value property
/// !NOT IMPLEMENTED!
/// </summary>
class PropertyMat2Widget : public QWidget
{
	Q_OBJECT
public:
	PropertyMat2Widget(PropertyID* property, QWidget* parent = 0) : QWidget(parent), property_(property) {}
	virtual ~PropertyMat2Widget(void);

	void initalise(QString text);
public slots:

	void lineComplete(int col, int row);

private:
	PropertyID* property_;
	QLabel* textLabel_;
	QLabel* x1TextLabel_;
	QLineEdit* x1InputLine_;
	QLabel* y1TextLabel_;
	QLineEdit* y1InputLine_;
	QLabel* x2TextLabel_;
	QLineEdit* x2InputLine_;
	QLabel* y2TextLabel_;
	QLineEdit* y2InputLine_;
};

/// <summary>
/// Qt custom widget designed to display a matrix 3x3 value property
/// !NOT IMPLEMENTED!
/// </summary>
class PropertyMat3Widget : public QWidget
{
	Q_OBJECT
public:
	PropertyMat3Widget(PropertyID* property, QWidget* parent = 0) : QWidget(parent), property_(property) {}
	virtual ~PropertyMat3Widget(void);

	void initalise(QString text);

public slots:

	void lineComplete(int col, int row);

private:
	PropertyID* property_;
	QLabel* textLabel_;
	QLabel* x1TextLabel_;
	QLineEdit* x1InputLine_;
	QLabel* y1TextLabel_;
	QLineEdit* y1InputLine_;
	QLabel* z1TextLabel_;
	QLineEdit* z1InputLine_;
	QLabel* x2TextLabel_;
	QLineEdit* x2InputLine_;
	QLabel* y2TextLabel_;
	QLineEdit* y2InputLine_;
	QLabel* z2TextLabel_;
	QLineEdit* z2InputLine_;
	QLabel* x3TextLabel_;
	QLineEdit* x3InputLine_;
	QLabel* y3TextLabel_;
	QLineEdit* y3InputLine_;
	QLabel* z3TextLabel_;
	QLineEdit* z3InputLine_;
};

/// <summary>
/// Qt custom widget designed to display a matrix 4x4 value property
/// !NOT IMPLEMENTED!
/// </summary>
class PropertyMat4Widget : public QWidget
{
	Q_OBJECT
public:
public:
	PropertyMat4Widget(PropertyID* property, QWidget* parent = 0) : QWidget(parent), property_(property) {}
	virtual ~PropertyMat4Widget(void);

	void initalise(QString text);
public slots:

	void lineComplete(int col, int row);

private:
	PropertyID* property_;
	QLabel* textLabel_;
	QLabel* x1TextLabel_;
	QLineEdit* x1InputLine_;
	QLabel* y1TextLabel_;
	QLineEdit* y1InputLine_;
	QLabel* w1TextLabel_;
	QLineEdit* w1InputLine_;
	QLabel* x2TextLabel_;
	QLineEdit* x2InputLine_;
	QLabel* y2TextLabel_;
	QLineEdit* y2InputLine_;
	QLabel* w2TextLabel_;
	QLineEdit* w2InputLine_;
	QLabel* x3TextLabel_;
	QLineEdit* x3InputLine_;
	QLabel* y3TextLabel_;
	QLineEdit* y3InputLine_;
	QLabel* w3TextLabel_;
	QLineEdit* w3InputLine_;
};
