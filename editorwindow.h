#ifndef EDITORWINDOW_H
#define EDITORWINDOW_H

#include <QWidget>
#include "rendercontext.h"
#include "scene.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

/// <summary>
/// The editor window is a mix of the render context and
/// a Qt widget. It allows for irrlicht to render into a
/// Qt widget and information from Qt can be sent back to
/// irrlicht
/// </summary>
class EditorWindow : public QWidget, public RenderContext
{
    Q_OBJECT
public:
    explicit EditorWindow(QWidget *parent = 0);
	virtual ~EditorWindow(void);

	virtual void initaliseContext();

	bool eventFilter(QObject *watched, QEvent *event);

public slots:
	void autoUpdateScene();
	void SceneListSelectedNode(Node* node);

signals:
	void updateScene();

protected:

	SIrrlichtKey convertKey(int key);
	virtual void keyPressEvent(QKeyEvent* event);
	virtual void keyReleaseEvent(QKeyEvent* event);
	virtual void mousePressEvent(QMouseEvent* event);
	virtual void mouseReleaseEvent(QMouseEvent* event);
	virtual void wheelEvent(QWheelEvent* event);
	virtual void mouseMoveEvent( QMouseEvent* event );

	virtual void resizeEvent(QResizeEvent* event);

	virtual void paintEvent(QPaintEvent* event);
	virtual void timerEvent(QTimerEvent* event);

	virtual void castRay();
	virtual Node* findNodeFromIrrlichtNode(ISceneNode* node);

	virtual void sendKeyEventToIrrlicht(QKeyEvent* event, bool pressedDown);
	virtual void sendMouseEventToIrrlicht(QMouseEvent* event, bool pressedDown);

private:

	
	/// <summary>
	/// The current scene object
	/// </summary>
	Scene* scene_;
};

#endif // EDITORWINDOW_H

