#ifndef SCENEWINDOW_H
#define SCENEWINDOW_H

#include <QDockWidget>
#include "scenelist.h"

/// <summary>
/// This is a dock widget which contains
/// a custom tree widget which store a
/// relation to the node and the tree widget item
/// </summary>
class SceneWindow : public QDockWidget
{
    Q_OBJECT
public:
    explicit SceneWindow(QWidget *parent = 0,  Qt::WindowFlags flags = 0 );

	void initaliseSceneTree(Scene* scene);

	SceneList* getSceneList();

signals:

	void selectedNode(Node* node);

public slots:
	void listSelectedNode(Node* node);


private:
	
	/// <summary>
	/// The scene tree widget
	/// </summary>
	SceneList* sceneList_;
};

#endif // SCENEWINDOW_H
