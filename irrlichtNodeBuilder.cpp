#include "irrlichtNodeBuilder.h"


irrlichtNodeBuilder::irrlichtNodeBuilder(QWidget* parent /*= 0*/) : QWidget(parent)
{
}


irrlichtNodeBuilder::~irrlichtNodeBuilder(void)
{
}

/// <summary>
/// Initalises the widget
/// </summary>
void irrlichtNodeBuilder::initalise()
{
	//Create the horizontal layout box
	HLayout_ = new QHBoxLayout(this);
	//create the vertical layout box for the right and left side
	VLayoutRight_ = new QVBoxLayout(this);
	VLayoutLeft_ = new QVBoxLayout(this);

	//build the table element to display irrlicht node types
	buildNodeTableElement();
	//build the right hand side widget elements to add a irrlicht node type
	buildNodeCreatorElement();

	//create the add node button and link its click signal
	//to the addnode slot
	addNode_ = new QPushButton(tr("Add Node"));
	connect(addNode_, SIGNAL(clicked()), this, SLOT(addNodeToTable()));
	//add the button to the right hand side layout
	VLayoutRight_->addWidget(addNode_);
	
	//Create the remove node button and link its click signal
	//to the removenode slot
	removeNode_ = new QPushButton(tr("Remove Node"));
	connect(removeNode_, SIGNAL(clicked()), this, SLOT(removeNodeFromTable()));
	//add the button to the right hand side layout
	VLayoutRight_->addWidget(removeNode_);

	//Add the left and right layouts to the main horizontal layout
	HLayout_->addLayout(VLayoutLeft_);
	HLayout_->addLayout(VLayoutRight_);
}

/// <summary>
/// Builds the node table element.
/// </summary>
void irrlichtNodeBuilder::buildNodeTableElement()
{
	//Create the table widget with two columns for the  two labels
	irrNodeTable_ = new QTableWidget(0,2);

	//Create the labels for the table
	QStringList labels;
	labels << tr("Name") << tr("IrrlichtNode Name");
	//Add the labels to the table and set the parameters for them
	irrNodeTable_->setHorizontalHeaderLabels(labels);
	irrNodeTable_->resizeColumnToContents(0);
	irrNodeTable_->horizontalHeader()->setStretchLastSection(true);

	//add the table to the left layout
	VLayoutLeft_->addWidget(irrNodeTable_);
}

void irrlichtNodeBuilder::buildNodeCreatorElement()
{
	//Set the node name label and input text box
	nameLabel_ = new QLabel(tr("Node Name"));
	nameTextBox_ = new QLineEdit(tr("New Node"));

	//Set the type label and create the combo box
	typeLabel_ = new QLabel(tr("Node Type"));
	typeCombo_ = new QComboBox();

	//build the node type map
	nodeTypes_[IRR_MESH_NODE]				= tr("Mesh Node");
	nodeTypes_[IRR_ANI_MESH_NODE]			= tr("Ani Mesh Node");
	nodeTypes_[IRR_CAMERA_NODE]				= tr("Camera Node");
	nodeTypes_[IRR_TERRAIN_NODE]			= tr("Terrain Node");
	//nodeTypes_[IRR_SOUND_NODE]				= tr("Sound Node");
	//nodeTypes_[IRR_BILLBOARD_NODE]			= tr("Billboard Node");
	//nodeTypes_[IRR_BILLBOARD_TEXT_NODE]		= tr("Billboard Text Node");
	nodeTypes_[IRR_LIGHT_NODE]				= tr("Light Node");
	//nodeTypes_[IRR_PARTICLE_ANI_MESH_NODE]	= tr("Ani Mesh Particle Emitter Node");
	//nodeTypes_[IRR_PARTICLE_EMITTER]		= tr("Particle Emitter Node");
	//nodeTypes_[IRR_TEXT_NODE]				= tr("Text Node");
	//nodeTypes_[IRR_VOLUME_LIGHT_NODE]		= tr("Volume Light Node");
	nodeTypes_[IRR_NO_NODE]					= tr("No Node");

	//iterate through the node type map and add them to the combo box
	std::unordered_map<int, QString>::iterator i;
	for (i = nodeTypes_.begin(); i != nodeTypes_.end(); i++)
	{
		typeCombo_->addItem(i->second, i->first);
	}
	
	//add all the widgets created in this function to the right hand layout
	VLayoutRight_->addWidget(nameLabel_);
	VLayoutRight_->addWidget(nameTextBox_);
	VLayoutRight_->addWidget(typeLabel_);
	VLayoutRight_->addWidget(typeCombo_);
}

/// <summary>
/// Updates the table widget.
/// </summary>
void irrlichtNodeBuilder::updateTable()
{
	//clear the table first
	irrNodeTable_->clearContents();

	int row = 0;
	//iterate through the added node types
	std::unordered_map<std::string, IrrNodeType>::iterator it;
	for (it = nodeList_.begin(); it != nodeList_.end(); it++)
	{
		//create a table item with the name of the node 
		QTableWidgetItem* item = new QTableWidgetItem(QString::fromStdString(it->first));
		//set the item to the first column
		irrNodeTable_->setItem(row, 0, item);
		//create another item but with the name of the node type
		item = new QTableWidgetItem(nodeTypes_[it->second]);
		//add it to the second column
		irrNodeTable_->setItem(row, 1, item);
		//increase the row count
		row++;
	}
}

/// <summary>
/// Adds the node to the table.
/// </summary>
void irrlichtNodeBuilder::addNodeToTable()
{
	//check to see if the node name has not already been added
	std::unordered_map<std::string, IrrNodeType>::iterator it =  nodeList_.find(nameTextBox_->text().toStdString());
	if(it == nodeList_.end())
	{
		//if not inset a new row in the table for it
		irrNodeTable_->insertRow(irrNodeTable_->rowCount());
		//add the node to the node list
		nodeList_[nameTextBox_->text().toStdString()] = (IrrNodeType)typeCombo_->itemData(typeCombo_->currentIndex()).toInt();
		//update the table with the new node
		updateTable();

		//emit the addnode signal so other object can know to get the list of node types again
		emit addedNode();
	}
}

/// <summary>
/// Removes the node from table.
/// </summary>
void irrlichtNodeBuilder::removeNodeFromTable()
{
	//get all the selected items in the table
	QList<QTableWidgetItem*> selected_itms = irrNodeTable_->selectedItems();
	//while the selected items is not empty
	while( !selected_itms.isEmpty() )
	{
		//get the first item
		QTableWidgetItem* item = selected_itms.at(0);
		//check its text value to see if it matches any in the stored node list
		std::unordered_map<std::string, IrrNodeType>::iterator it = nodeList_.find(item->text().toStdString());
		if(it != nodeList_.end())
		{
			//if it does remove it from the list and remove the row
			nodeList_.erase(it->first);
			irrNodeTable_->removeRow(item->row());
		}
		else
			break;
		//obtain the selected items again which will contain less as one selected row has already been removed
		selected_itms = irrNodeTable_->selectedItems();
	}
	//update the table to show the changes
	updateTable();

	//emit the removenode signal so other object can know to get the list of node types again
	emit removedNode();
}